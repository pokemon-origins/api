START TRANSACTION;

-- Attack stats
CREATE TABLE IF NOT EXISTS `stats` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Stat ID (Primary key)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `stats` (`id`) VALUES (1), (2), (3), (4), (5), (6);

CREATE TABLE IF NOT EXISTS `stats_translation` (
  `languages_id` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `stats_id` int(1) unsigned NOT NULL COMMENT 'Stat ID (Foreign)',
  `name` varchar(255) NOT NULL COMMENT 'Stat name',
  CONSTRAINT FK_STAT_ID FOREIGN KEY (stats_id) REFERENCES stats(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_STAT_LANGUAGE FOREIGN KEY (languages_id) REFERENCES languages(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (languages_id, stats_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `stats_translation` (`languages_id`, `stats_id`, `name`) VALUES
 ('FR', 1, 'Attaque'), ('EN', 1, 'Attack'),
 ('FR', 2, 'Défense'), ('EN', 2, 'Defense'),
 ('FR', 3, 'Vitesse'), ('EN', 3, 'Speed'),
 ('FR', 4, 'Attaque spéciale'), ('EN', 4, 'Special Attack'),
 ('FR', 5, 'Défense spéciale'), ('EN', 5, 'Special Defense'),
 ('FR', 6, 'PV'), ('EN', 6, 'HP');

 -- Attack classes
CREATE TABLE IF NOT EXISTS `attack_classes` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Attack class ID (Primary key)',
  `color` char(6) NOT NULL COMMENT 'Display color (in hexadecimal) of the attack class'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `attack_classes` (`id`, `color`) VALUES (1, '8C888C'), (2, 'C92112'), (3, '4F5870');

CREATE TABLE IF NOT EXISTS `attack_classes_translation` (
  `attack_classes_id` int(2) unsigned NOT NULL COMMENT 'Attack class ID (Foreign)',
  `languages_id` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `name` varchar(255) NOT NULL COMMENT 'Name of the attack class',
  `description` text NOT NULL COMMENT 'Description of the attack class',
  CONSTRAINT FK_ATTACK_CLASSES FOREIGN KEY (attack_classes_id) REFERENCES attack_classes(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ATTACK_CLASSES_LANGUAGE FOREIGN KEY (languages_id) REFERENCES languages(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (languages_id, attack_classes_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `attack_classes_translation` (`attack_classes_id`, `languages_id`, `name`, `description`) VALUES (1, 'EN', 'Status', 'This is the only category that does not inflict damage. Status moves include moves that change the weather, inflict status conditions, or raise or lower the stats of a Pokémon, among other effects.'), (1, 'FR', 'Statut', 'C\'est la seule catégorie qui n\'inflige pas de dégâts. Les mouvements de statut comprennent les coups qui modifient la météo, infligent des conditions de statut, ou augmentent ou diminuent les statistiques d\'un Pokémon, entre autres effets.'), (2, 'EN', 'Physical', 'Physical moves deal damage depending on both the Attack stat of the attacking Pokémon and the Defense stat of the defending Pokémon.'), (2, 'FR', 'Physique', 'Les coups physiques infligent des dégâts dépendant à la fois de la valeur des stats d\'attaque du Pokémon attaquant et de la valeur de défense du Pokémon défenseur.'), (3, 'EN', 'Special', 'Special moves cause damage depending on the special stats of both the attacking Pokémon and the defending Pokémon.'), (3, 'FR', 'Spéciale', 'Les coups spéciaux provoquent des dégâts en fonction des statistiques spéciales du Pokémon attaquant et du Pokémon défenseur.');

-- Attack/Pokemon types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Attack/Pokemon type ID (Primary key)',
  `color` char(6) NOT NULL COMMENT 'Display color (in hexadecimal) of the attack/pokemon type'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `types` (`id`, `color`) VALUES (1, 'A8A878'), (2, 'C03028'), (3, 'A890F0'), (4, 'A040A0'), (5, 'E0C068'), (6, 'B8A038'), (7, 'A8B820'), (8, '705898'), (9, 'B8B8D0'), (10, 'F08030'), (11, '6890F0'), (12, '78C850'), (13, 'F8D030'), (14, 'F85888'), (15, '98D8D8'), (16, '7038F8'), (17, '705848'), (18, 'EE99AC');

CREATE TABLE IF NOT EXISTS `types_translation` (
  `types_id` int(2) unsigned NOT NULL COMMENT 'Attack/Pokemon type ID (Foreign)',
  `languages_id` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `name` varchar(255) NOT NULL COMMENT 'Name of the attack/pokemon type',
  `description` text NOT NULL COMMENT 'Description of the attack/pokemon type',
  CONSTRAINT FK_ATTACK_TYPES FOREIGN KEY (types_id) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ATTACK_TYPES_LANGUAGE FOREIGN KEY (languages_id) REFERENCES languages(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (languages_id, types_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `types_translation` (`types_id`, `languages_id`, `name`, `description`) VALUES
  (1, 'EN', 'Normal', 'More moves are Normal than any other type; however, many of these moves are status moves that don\'t inflict any damage. Among species, Normal-type Pokémon tend to be based on a variety of different real-world animals.'),
  (1, 'FR', 'Normal', 'La plupart des attaques sont de types normaux; Cependant, beaucoup de ces attaques sont des attaques de statut qui n\'infligent aucun dégât. Parmi les espèces, les Pokémon de type Normal ont tendance à être inspirés d\'animaux du monde réel.'),
  (2, 'EN', 'Fighting', 'Pokémon of this type are specialists in melee attacks, such as punches and kicks. It could be said that they\'re the opposite to Psychic-type Pokémon because while Pokémon of that type depend primarily on special attacks, Pokémon of the Fighting type depend mostly on physical attacks. Most Fighting-type Pokémon have a human-like body shape because they represent practitioners of various martial arts, which tend to be real-world humans.'),
  (2, 'FR', 'Combat', 'Les Pokémon de ce type sont des spécialistes des attaques de mêlée, telles que les coups de poing et les coups de pied. On pourrait dire qu\'ils sont à l\'opposé des Pokémon de type Psy car si les Pokémon de ce type dépendent principalement d\'attaques spéciales, les Pokémon du Type de Combat dépendent principalement des attaques physiques. La plupart des Pokémon de type Combat ont une forme de corps semblable à l\'humain car ils représentent des pratiquants de divers arts martiaux, qui ont tendance à être des humains du monde réel.'),
  (3, 'EN', 'Flying', 'Pokémon of this type can fly, many of them live at high altitudes, even. Most of them are birds and insects. Their power is mostly related with aerial and wind-related attacks. Most of them have wings, but there are also some of them that just float without wings, like Rayquaza and Gyarados.'),
  (3, 'FR', 'Vol', 'Les Pokémon de ce type peuvent voler, beaucoup d\'entre eux vivent même à haute altitude. La plupart d\'entre eux sont des oiseaux et des insectes. Leur puissance est principalement liée à des attaques aériennes et liées au vent. La plupart d\'entre eux ont des ailes, mais il y en a aussi qui flottent sans ailes, comme Rayquaza et Gyarados.'),
  (4, 'EN', 'Poison', 'These Pokémon have a natural toxic quality; some directly represent real-world species known for their venom, such as snakes, and some even represent pollution itself. They normally live in caves, marshes or similar places.'),
  (4, 'FR', 'Poison', 'Ces Pokémon ont une qualité toxique naturelle; certains représentent directement des espèces du monde réel connues pour leur venin, comme les serpents, et certains représentent même la pollution elle-même. Ils vivent normalement dans des grottes, des marais ou des endroits similaires.'),
  (5, 'EN', 'Ground', 'Ground-type Pokémon have powers and abilities related to control of ground and earth. Ground-type Pokémon are afraid of water, like Rock-type Pokémon, unless they are Water type. Many Ground Pokémon are also partially Rock type. These Pokémon are normally found in caves or rocky terrains.'),
  (5, 'FR', 'Sol', 'Les Pokémons terrestres ont des pouvoirs et des capacités liés au contrôle du sol et de la terre. Les Pokémons terrestres ont peur de l\'eau, comme les Pokémons de type Roche, à moins qu\'ils ne soient de type Eau. De nombreux Pokémons terrestres sont également de type Roche. Ces Pokémon se trouvent normalement dans des grottes ou des terrains rocheux.'),
  (6, 'EN', 'Rock', 'Rock-type Pokémon stand out for their great defense to physical attacks, however, this type is tied with the Grass-type as having the most weaknesses, with five, and Pokémon of this type are not very fast. Also, some Rock-type moves don\'t have great accuracy.'),
  (6, 'FR', 'Roche', 'Les Pokémons de type roche se distinguent par leur grande défense contre les attaques physiques, cependant, ce type est lié au type plante comme ayant le plus de faiblesses, avec cinq. De plus, les Pokémons de ce type ne sont pas très rapides et certaines attaques n\'ont pas une grande précision.'),
  (7, 'EN', 'Bug', 'Bug-type Pokémon are characterized by their fast growing, they don\'t take long to evolve. Bug-type Pokémon live mostly (not all of them) in forests, some of them are a little harder to find because they live atop the trees.'),
  (7, 'FR', 'Insecte', 'Les Pokémons insectes sont caractérisés par leur croissance rapide, ils ne tardent pas à évoluer. Ils vivent pour la plupart (pas tous) dans les forêts, certains d\'entre eux sont un peu plus difficiles à trouver parce qu\'ils vivent au sommet des arbres.'),
  (8, 'EN', 'Ghost', 'The Ghost type is notable for having few Pokémon, many of whom have low HP, and moves. In battles, the Ghost-type is useful because it\'s the only type that has two immunities: Normal and Fighting, both of which are common move types. Pokémon of this type are usually connected to fear, the dark and the afterlife. They usually live in abandoned houses, cemeteries, funeral places and uninhabited dark places such as caves.'),
  (8, 'FR', 'Spectre', 'Le type spectre est remarquable pour avoir peu de Pokémons, dont beaucoup ont un faible HP, et peu d\'attaques. Dans les batailles, ce type est utile parce que c\'est le seul qui a deux immunités: Normal et Combat, les deux étant des types de coups communs. Les Pokémons de ce type sont généralement liés à la peur, l\'obscurité et l\'au-delà. Habituellement, ils vivent dans des maisons abandonnées, des cimetières, des lieux de funérailles et des endroits obscurs inhabités tels que des grottes.'),
  (9, 'EN', 'Steel', 'Steel-type Pokémon stand out for having great defense against both physical and special attacks, and a large number of resistances. They tend to be heavy and thus have low Speed. Steel-types are mainly inorganic in nature, some of them representing robots and machines. Along with the Rock and Ground types, Steel represents part of the Earth\'s minerals as types.'),
  (9, 'FR', 'Acier', 'Les Pokémons de type acier se distinguent par leur grande défense contre les attaques physiques et spéciales. Ils ont tendance à être lourds et ont donc une faible vitesse. Les types d\'acier sont principalement de nature inorganique, certains représentant des robots et des machines. Avec les types roche et sol, l\'acier représente une partie des minéraux de la Terre en tant que types.'),
  (10, 'EN', 'Fire', 'It is notable for being one of the three Starter types, forming a perfectly triangular relationship with Grass and Water. ire-type moves are based on attacks of fire itself, and most of them can leave the status Burn. Fire types are also immune to being Burned, regardless of the type of move used that would have inflicted a Burn.'),
  (10, 'FR', 'Feu', 'Ce type forme une relation parfaitement triangulaire avec l\'eau et la plante. Les attaques de type feu peuvent pour la plupart d\'entre elle infliger le statut brulé. Les types feu sont également immunisés contre le fait d\'être brûlé, peu importe le type de coup utilisé qui aurait infligé une brûlure.'),
  (11, 'EN', 'Water', 'The Water-type is one of the three Starter types, forming a perfectly triangular relationship with Fire and Grass. There are more Pokémon of this type than any other type due to the large number of marine creatures to base species from. Most Pokémon of this type also have another type, representing the biodiversity of marine creatures.'),
  (11, 'FR', 'Eau', 'Le type eau est l\'un des trois types de base, formant une relation parfaitement triangulaire avec le feu et la plante. Il y a plus de Pokémons de ce type que n\'importe quel autre type en raison du grand nombre de créatures marines à partir desquelles les espèces sont basées. La plupart des Pokémons de ce type ont aussi un autre type, représentant la biodiversité des créatures marines.'),
  (12, 'EN', 'Grass', 'Grass is one of the three Starter types, forming a perfectly triangular relationship with Fire and Water. Many Grass types are based on real-world plants and fungi; many such Pokémon also belong to the Plant egg group. Several Grass types are paired with the Poison type, reflecting the toxicity of several plants towards mainly humans. It is worth noting that the first five Grass-type starter lines are based on prehistoric or current species of reptile.'),
  (12, 'FR', 'Plante', 'Le type plante est l\'un des trois types de base, formant une relation parfaitement triangulaire avec le Feu et l\'Eau. De nombreux pokemons de type plante sont basés sur des plantes et des champignons du monde réel; beaucoup de ces Pokémons appartiennent également au groupe des œufs de plantes. Plusieurs types plante sont appariés avec le type de poison, reflétant la toxicité de plusieurs usines à cause des humains. Il est à noter que les cinq premières lignes de départ de type plante sont basées sur des espèces préhistoriques ou actuelles de reptile.'),
  (13, 'EN', 'Electric', 'The Electric type is a type of Pokémon that have electricity-oriented powers. They possess electro kinetic abilities, being able to control, store, or even produce electricity. Electric-type Pokémon have varied habitats, from forests, praries, cities and power plants. Electric-type Pokémon are usually fast, and many of their attacks may paralyze the target.'),
  (13, 'FR', 'Électrique', 'Le type électrique est un type de Pokémon qui a des pouvoirs orientés vers l\'électricité. Ils possèdent des capacités électro-cinétiques, pouvant contrôler, stocker ou même produire de l\'électricité. Les Pokémons de type électrique ont des habitats variés, issus des forêts, des prairies, des villes et des centrales électriques. Les Pokémons de type électrique sont généralement rapides, et beaucoup de leurs attaques peuvent paralyser la cible.'),
  (14, 'EN', 'Psychic', 'Many Pokémon are of this type, and, being the type with most Legendary Pokémon, for many, the Psychic type is the most powerful. Psychic-type Pokémon tend to be very intelligent. It\'s also interesting to note that, many Psychic-type Pokémon are based/related to real scientific or mythological discoveries such as DNA and Psychokinesis.'),
  (14, 'FR', 'Psy', 'Beaucoup de Pokémons sont de ce type, et, étant le type avec le plus de Pokémons Légendaire, pour beaucoup, le type Psychique est le plus puissant. Les Pokémons de type psychique ont tendance à être très intelligents. Il est également intéressant de noter que de nombreux Pokémons de type psychique sont basés / liés à de véritables découvertes scientifiques ou mythologiques telles que l\'ADN et la psychokinésie.'),
  (15, 'EN', 'Ice', 'Ice-type Pokémon stand out for being able to endure very low temperatures, as well as adapting to freezing weathers. They control ice at will. Their habitats go from the top of mountains, frozen caves and caverns or even the poles.'),
  (15, 'FR', 'Glace', 'Les Pokémons de type glace se distinguent par leur capacité à supporter des températures très basses et à s\'adapter aux temps de gel. Ils contrôlent la glace à volonté. Leurs habitats peuvent être le sommet des montagnes, des grottes et des cavernes gelées ou même des pôles.'),
  (16, 'EN', 'Dragon', 'The Dragon type is often considered an ancestral type as many Legendary Dragon-type Pokémon are revered as deities. Other Dragon-type Pokémon are frequently hard to catch and train. They also evolve late and are relatively rare. An interesting fact is that the stats of many Dragon-type Pokémon surpass the stats of other types of Pokémon.'),
  (16, 'FR', 'Dragon', 'Le type dragon est souvent considéré comme un type ancestral car de nombreux Pokémons de type Dragon Légendaire sont vénérés comme des divinités. D\'autres Pokémons de type Dragon sont souvent difficiles à attraper et à entraîner. Ils évoluent également tard et sont relativement rares. Un fait intéressant est que les statistiques de nombreux Pokémons de type Dragon surpassent les statistiques des autres types de Pokémons.'),
  (17, 'EN', 'Dark', 'Many Dark types are rather just born with features that define them as a type, as they understand of the powers that come with it, such as Absol and Sableye, and/or are defined so by the perspective of humans with necessary purpose in the environment and their species but whose actions and traits are at most taboo and are discouraged in human society.'),
  (17, 'FR', 'Ténèbres', 'De nombreux types ténèbres sont souvent nés avec des caractéristiques qui les définissent comme un type et comprennent les pouvoirs qui les accompagnent. Ils sont souvent définis par la perspective environnementale des humains, mais dont les actions et les traits sont tout au plus tabous et découragés dans la société actuelle.'),
  (18, 'EN', 'Fairy', 'Fairy-type Pokémon are generally considered cute and tend to be at least partially pink in color and feminine in appearance overall. However, Fairy types can be incredibly powerful. Fairy types also have some magical appeal to them. As opposed to Psychic-type Pokémon, Fairy-type Pokémon have powers related to magic and the supernatural as opposed to mental power.'),
  (18, 'FR', 'Fée', 'Les Pokémon féeriques sont généralement considérés comme mignons et ont tendance à être au moins partiellement de couleur rose et d\'aspect féminin. Il est à noter que les types fées peuvent être incroyablement puissants. Ils ont aussi un attrait magique pour eux. Contrairement aux Pokémons de type Psy, les Pokémons de type Fée ont des pouvoirs liés à la magie et au surnaturel par opposition au pouvoir mental.'); 

-- Attack/Pokemon type strength
CREATE TABLE IF NOT EXISTS `type_strengths` (
  `attacker_type` int(2) unsigned NOT NULL COMMENT 'Attacker type (Foreign)',
  `victim_type` int(2) unsigned NOT NULL COMMENT 'Victim type (Foreign)',
  `strength_multiplicator` float NOT NULL COMMENT 'Strength (Multiply the attack damage by this multiplicator)',
  CONSTRAINT FK_ATTACKER_TYPE_STRENGTH FOREIGN KEY (attacker_type) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_VICTIM_TYPE_STRENGTH FOREIGN KEY (victim_type) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `type_strengths` (`attacker_type`, `victim_type`, `strength_multiplicator`) VALUES (1,1,1), (1,2,1), (1,3,1), (1,4,1), (1,5,1), (1,6,0.5), (1,7,1), (1,8,0), (1,9,0.5), (1,10,1), (1,11,1), (1,12,1), (1,13,1), (1,14,1), (1,15,1), (1,16,1), (1,17,1), (1,18,1), (2,1,2), (2,2,1), (2,3,0.5), (2,4,0.5), (2,5,1), (2,6,2), (2,7,0.5), (2,8,0), (2,9,2), (2,10,1), (2,11,1), (2,12,1), (2,13,1), (2,14,0.5), (2,15,2), (2,16,1), (2,17,2), (2,18,0.5), (3,1,1), (3,2,2), (3,3,1), (3,4,1), (3,5,1), (3,6,0.5), (3,7,2), (3,8,1), (3,9,0.5), (3,10,1), (3,11,1), (3,12,2), (3,13,0.5), (3,14,1), (3,15,1), (3,16,1), (3,17,1), (3,18,1), (4,1,1), (4,2,1), (4,3,1), (4,4,0.5), (4,5,0.5), (4,6,0.5), (4,7,1), (4,8,0.5), (4,9,0), (4,10,1), (4,11,1), (4,12,2), (4,13,1), (4,14,1), (4,15,1), (4,16,1), (4,17,1), (4,18,2), (5,1,1), (5,2,1), (5,3,0), (5,4,2), (5,5,1), (5,6,2), (5,7,0.5), (5,8,1), (5,9,2), (5,10,2), (5,11,1), (5,12,0.5), (5,13,2), (5,14,1), (5,15,1), (5,16,1), (5,17,1), (5,18,1), (6,1,1), (6,2,0.5), (6,3,2), (6,4,1), (6,5,0.5), (6,6,1), (6,7,2), (6,8,1), (6,9,0.5), (6,10,2), (6,11,1), (6,12,1), (6,13,1), (6,14,1), (6,15,2), (6,16,1), (6,17,1), (6,18,1), (7,1,1), (7,2,0.5), (7,3,0.5), (7,4,0.5), (7,5,1), (7,6,1), (7,7,1), (7,8,0.5), (7,9,0.5), (7,10,0.5), (7,11,1), (7,12,2), (7,13,1), (7,14,2), (7,15,1), (7,16,1), (7,17,2), (7,18,0.5), (8,1,0), (8,2,1), (8,3,1), (8,4,1), (8,5,1), (8,6,1), (8,7,1), (8,8,2), (8,9,1), (8,10,1), (8,11,1), (8,12,1), (8,13,1), (8,14,2), (8,15,1), (8,16,1), (8,17,0.5), (8,18,1), (9,1,1), (9,2,1), (9,3,1), (9,4,1), (9,5,1), (9,6,2), (9,7,1), (9,8,1), (9,9,0.5), (9,10,0.5), (9,11,0.5), (9,12,1), (9,13,0.5), (9,14,1), (9,15,2), (9,16,1), (9,17,1), (9,18,2), (10,1,1), (10,2,1), (10,3,1), (10,4,1), (10,5,1), (10,6,0.5), (10,7,2), (10,8,1), (10,9,2), (10,10,0.5), (10,11,0.5), (10,12,2), (10,13,1), (10,14,1), (10,15,2), (10,16,0.5), (10,17,1), (10,18,1), (11,1,1), (11,2,1), (11,3,1), (11,4,1), (11,5,2), (11,6,2), (11,7,1), (11,8,1), (11,9,1), (11,10,2), (11,11,0.5), (11,12,0.5), (11,13,1), (11,14,1), (11,15,1), (11,16,0.5), (11,17,1), (11,18,1), (12,1,1), (12,2,1), (12,3,0.5), (12,4,0.5), (12,5,2), (12,6,2), (12,7,0.5), (12,8,1), (12,9,0.5), (12,10,0.5), (12,11,2), (12,12,0.5), (12,13,1), (12,14,1), (12,15,1), (12,16,0.5), (12,17,1), (12,18,1), (13,1,1), (13,2,1), (13,3,2), (13,4,1), (13,5,0), (13,6,1), (13,7,1), (13,8,1), (13,9,1), (13,10,1), (13,11,2), (13,12,0.5), (13,13,0.5), (13,14,1), (13,15,1), (13,16,0.5), (13,17,1), (13,18,1), (14,1,1), (14,2,2), (14,3,1), (14,4,2), (14,5,1), (14,6,1), (14,7,1), (14,8,1), (14,9,0.5), (14,10,1), (14,11,1), (14,12,1), (14,13,1), (14,14,0.5), (14,15,1), (14,16,1), (14,17,0), (14,18,1), (15,1,1), (15,2,1), (15,3,2), (15,4,1), (15,5,2), (15,6,1), (15,7,1), (15,8,1), (15,9,0.5), (15,10,0.5), (15,11,0.5), (15,12,2), (15,13,1), (15,14,1), (15,15,0.5), (15,16,2), (15,17,1), (15,18,1), (16,1,1), (16,2,1), (16,3,1), (16,4,1), (16,5,1), (16,6,1), (16,7,1), (16,8,1), (16,9,0.5), (16,10,1), (16,11,1), (16,12,1), (16,13,1), (16,14,1), (16,15,1), (16,16,2), (16,17,1), (16,18,0), (17,1,1), (17,2,0.5), (17,3,1), (17,4,1), (17,5,1), (17,6,1), (17,7,1), (17,8,2), (17,9,1), (17,10,1), (17,11,1), (17,12,1), (17,13,1), (17,14,2), (17,15,1), (17,16,1), (17,17,0.5), (17,18,0.5), (18,1,1), (18,2,2), (18,3,1), (18,4,0.5), (18,5,1), (18,6,1), (18,7,1), (18,8,1), (18,9,0.5), (18,10,0.5), (18,11,1), (18,12,1), (18,13,1), (18,14,1), (18,15,1), (18,16,2), (18,17,2), (18,18,1);

-- Attack effects
CREATE TABLE IF NOT EXISTS `attack_effects` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Attack effect ID (Primary key)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `attack_effects` (`id`) VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12), (14), (17), (18), (19), (20), (21), (24), (25), (26), (27), (28), (29), (30), (31), (32), (33), (34), (35), (36), (37), (38), (39), (40), (41), (42), (43), (44), (45), (46), (47), (48), (49), (50), (51), (52), (53), (54), (55), (58), (59), (60), (61), (63), (66), (67), (68), (69), (70), (71), (72), (73), (74), (76), (77), (78), (79), (80), (81), (82), (83), (84), (85), (86), (87), (88), (89), (90), (91), (92), (93), (94), (95), (96), (98), (99), (100), (101), (102), (103), (104), (105), (106), (107), (108), (109), (110), (112), (113), (114), (115), (116), (117), (118), (119), (120), (121), (122), (123), (124), (125), (126), (127), (128), (129), (130), (131), (133), (136), (137), (138), (139), (140), (141), (143), (144), (145), (146), (147), (148), (149), (150), (151), (152), (153), (154), (155), (156), (157), (159), (160), (161), (162), (163), (165), (166), (167), (168), (169), (170), (171), (172), (173), (174), (175), (176), (177), (178), (179), (180), (181), (182), (183), (184), (185), (186), (187), (188), (189), (190), (191), (192), (193), (194), (195), (196), (197), (198), (199), (200), (201), (202), (203), (204), (205), (206), (207), (208), (209), (210), (211), (212), (213), (214), (215), (216), (217), (218), (219), (220), (221), (222), (223), (224), (225), (226), (227), (228), (229), (230), (231), (232), (233), (234), (235), (236), (237), (238), (239), (240), (241), (242), (243), (244), (245), (246), (247), (248), (249), (250), (251), (252), (253), (254), (255), (256), (257), (258), (259), (260), (261), (262), (263), (264), (266), (267), (268), (269), (270), (271), (272), (273), (274), (275), (276), (277), (278), (279), (280), (281), (282), (283), (284), (285), (286), (287), (288), (289), (290), (291), (292), (293), (294), (295), (296), (297), (298), (299), (300), (301), (302), (303), (304), (305), (306), (307), (308), (309), (310), (311), (312), (313), (314), (315), (316), (317), (318), (319), (320), (321), (322), (323), (324), (325), (326), (327), (328), (329), (330), (331), (332), (333), (335), (336), (337), (338), (339), (340), (341), (342), (343), (344), (345), (346), (347), (348), (349), (350), (351), (352), (353), (354), (355), (356), (357), (358), (359), (360), (361), (362), (363), (364), (365), (366), (367), (368), (369), (370);

CREATE TABLE IF NOT EXISTS `attack_effects_translation` (
  `attack_effects_id` int(3) unsigned NOT NULL COMMENT 'Attack effect ID (Foreign)',
  `languages_id` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `short_description` text NOT NULL COMMENT 'Short description of the attack effect',
  `description` text NOT NULL COMMENT 'Description of the attack effect',
  CONSTRAINT FK_ATTACK_EFFECTS FOREIGN KEY (attack_effects_id) REFERENCES attack_effects(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ATTACK_EFFECTS_LANGUAGE FOREIGN KEY (languages_id) REFERENCES languages(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (languages_id, attack_effects_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `attack_effects_translation` (`attack_effects_id`, `languages_id`, `short_description`, `description`) VALUES
 (1, 'EN', 'Inflicts regular damage with no additional effect.', 'Inflicts $damage damage.'),
 (2, 'EN', 'Puts the target to sleep.', 'Puts the target to sleep.'),
 (3, 'EN', 'Has a $effect_chance% chance to poison the target.', 'Inflicts $damage damage and has a $effect_chance% chance to poison the target.'),
 (4, 'EN', 'Drains half the damage inflicted to heal the user.', 'Inflicts $damage damage and drains half the damage inflicted to heal the user.'),
 (5, 'EN', 'Has a $effect_chance% chance to burn the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to burn the target.'),
 (6, 'EN', 'Has a $effect_chance% chance to freeze the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to freeze the target.'),
 (7, 'EN', 'Has a $effect_chance% chance to paralyze the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to paralyze the target.'),
 (8, 'EN', 'User faints.', 'User faints, even if the attack fails or misses. Inflicts $damage damage.'),
 (9, 'EN', 'Only works on sleeping Pokémon. Drains half the damage inflicted to heal the user.', 'Fails if not used on a sleeping Pokémon. Inflicts $damage damage. Drains half the damage inflicted to heal the user.'),
 (10, 'EN', 'Uses the target''s last used move.', 'Uses the last move targeted at the user by a Pokémon still on the field. A move counts as targeting the user even if it hit multiple Pokémon, as long as the user was one of them; however, moves targeting the field itself do not count. If the user has not been targeted by an appropriate move since entering the field, or if no Pokémon that targeted the user remains on the field, this move will fail. Moves that failed, missed, had no effect, or were blocked are still copied. Assist moves, time-delayed moves, “meta” moves that operate on other moves/Pokémon/abilities, and some other special moves cannot be copied and are ignored; if the last move to hit the user was such a move, the previous move will be used instead. The full list of ignored moves is: acid-armor, acupressure, after-you, agility, ally-switch, amnesia, aqua-ring, aromatherapy, aromatic-mist, assist, autotomize, barrier, baton-pass, belch, belly-drum, bide, bulk-up, calm-mind, camouflage, celebrate, charge, coil, conversion, conversion-2, copycat, cosmic-power, cotton-guard, counter, crafty-shield, curse, defend-order, defense-curl, destiny-bond, detect, doom-desire, double-team, dragon-dance, electric-terrain, endure, final-gambit, flower-shield, focus-energy, focus-punch, follow-me, future-sight, geomancy, grassy-terrain, gravity, growth, grudge, guard-split, hail, happy-hour, harden, haze, heal-bell, heal-order, heal-pulse, healing-wish, helping-hand, hold-hands, hone-claws, howl, imprison, ingrain, ion-deluge, iron-defense, kings-shield, light-screen, lucky-chant, lunar-dance, magic-coat, magnet-rise, magnetic-flux, mat-block, me-first, meditate, metronome, milk-drink, mimic, minimize, mirror-coat, mirror-move, mist, misty-terrain, moonlight, morning-sun, mud-sport, nasty-plot, nature-power, perish-song, power-split, power-trick, protect, psych-up, quick-guard, quiver-dance, rage-powder, rain-dance, recover, recycle, reflect, reflect-type, refresh, rest, rock-polish, role-play, roost, rototiller, safeguard, sandstorm, shadow-blast, shadow-bolt, shadow-half, shadow-rush, shadow-shed, shadow-sky, shadow-storm, shadow-wave, sharpen, shell-smash, shift-gear, sketch, slack-off, sleep-talk, snatch, soft-boiled, spikes, spiky-shield, spit-up, splash, stealth-rock, sticky-web, stockpile, struggle, substitute, sunny-day, swallow, swords-dance, move:synthesis, tail-glow, tailwind, teleport, toxic-spikes, transform, water-sport, wide-guard, wish, withdraw and work-up. This move cannot be selected by assist, metronome, or sleep-talk, nor forced by encore.'),
 (11, 'EN', 'Raises the user''s Attack by one stage.', 'Raises the user''s attack by one stage.'),
 (12, 'EN', 'Raises the user''s Defense by one stage.', 'Raises the user''s defense by one stage.'),
 (14, 'EN', 'Raises the user''s Special Attack by one stage.', 'Raises the user''s special attack by one stage.'),
 (17, 'EN', 'Raises the user''s evasion by one stage.', 'Raises the user''s evasion by one stage.'),
 (18, 'EN', 'Never misses.', 'Inflicts $damage damage. Ignores accuracy and evasion modifiers.'),
 (19, 'EN', 'Lowers the target''s Attack by one stage.', 'Lowers the target''s attack by one stage.'),
 (20, 'EN', 'Lowers the target''s Defense by one stage.', 'Lowers the target''s defense by one stage.'),
 (21, 'EN', 'Lowers the target''s Speed by one stage.', 'Lowers the target''s speed by one stage.'),
 (24, 'EN', 'Lowers the target''s accuracy by one stage.', 'Lowers the target''s accuracy by one stage.'),
 (25, 'EN', 'Lowers the target''s evasion by one stage.', 'Lowers the target''s evasion by one stage.'),
 (26, 'EN', 'Resets all Pokémon''s stats, accuracy, and evasion.', 'Removes stat, accuracy, and evasion modifiers from every Pokémon on the field. This does not count as a stat reduction for the purposes of clear-body or white-smoke.'),
 (27, 'EN', 'User waits for two turns, then hits back for twice the damage it took.', 'User waits for two turns. On the second turn, the user inflicts twice the damage it accumulated on the last Pokémon to hit it. Damage inflicted is typeless. This move cannot be selected by sleep-talk.'),
 (28, 'EN', 'Hits every turn for 2-3 turns, then confuses the user.', 'Inflicts $damage damage. User is forced to attack with this move for 2–3 turns,selected at random. After the last hit, the user becomes confused. Safeguard does not protect against the confusion from this move.'),
 (29, 'EN', 'Immediately ends wild battles. Forces trainers to switch Pokémon.', 'Switches the target out for another of its trainer''s Pokémon selected at random. Wild battles end immediately. Doesn''t affect Pokémon with suction-cups or under the effect of ingrain.'),
 (30, 'EN', 'Hits 2-5 times in one turn.', 'Inflicts $damage damage. Hits 2–5 times in one turn. Has a 3/8 chance each to hit 2 or 3 times, and a 1/8 chance each to hit 4 or 5 times. Averages to 3 hits per use.'),
 (31, 'EN', 'User''s type changes to the type of one of its moves at random.', 'User''s type changes to the type of one of its moves, selected at random. Hidden-power and weather-ball are treated as normal type. Only moves with a different type are eligible, curse is never eligible. If the user has no suitable moves, this move will fail.'),
 (32, 'EN', 'Has a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. Has a $effect_chance% chance to make the target flinch.'),
 (33, 'EN', 'Heals the user by half its max HP.', 'Heals the user for half its max HP.'),
 (34, 'EN', 'Badly poisons the target, inflicting more damage every turn.', 'Badly poisons the target. Never misses when used by a poison-type Pokémon.'),
 (35, 'EN', 'Scatters money on the ground worth five times the user''s level.', 'Inflicts $damage damage. After the battle ends, the winner receives five times the user''s level in extra money for each time this move was used.'),
 (36, 'EN', 'Reduces damage from special attacks by 50% for five turns.', 'Erects a barrier around the user''s side of the field that reduces damage from special attacks by half for five turns. In double battles, the reduction is 1/3. Critical hits are not affected by the barrier. If the user is holding light-clay item, the barrier lasts for eight turns. Moves brick-break or defog used by an opponent will destroy the barrier.'),
 (37, 'EN', 'Has a $effect_chance% chance to burn, freeze, or paralyze the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to burn, freeze, or paralyze the target. One of these effects is selected at random; they do not each have independent chances to occur.'),
 (38, 'EN', 'User sleeps for two turns, completely healing itself.', 'User falls to sleep and immediately regains all its HP. If the user has another major status effect, sleep will replace it. The user will always wake up after two turns, or one turn with early-bird ability. This move fails if the Pokémon cannot fall asleep due to uproar move, insomnia, or vital-spirit ability. It also fails if the Pokémon is at full health or is already asleep.'),
 (39, 'EN', 'Causes a one-hit KO.', 'Inflicts damage equal to the target''s max HP. Ignores accuracy and evasion modifiers. This move''s accuracy is 30% plus 1% for each level the user is higher than the target. If the user is a lower level than the target, this move will fail. Because this move inflicts a specific and finite amount of damage, endure move still prevents the target from fainting. The effects of lock-on move, mind-reader move, and no-guard ability still apply, as long as the user is equal or higher level than the target. However, they will not give this move a chance to break through detect move or protect move.'),
 (40, 'EN', 'Requires a turn to charge before attacking.', 'Inflicts $damage damage. User''s Critical hit rate is one level higher when using this move. User charges for one turn before attacking. This move cannot be selected by sleep-talk move.'),
 (41, 'EN', 'Inflicts damage equal to half the target''s HP.', 'Inflicts typeless damage equal to half the target''s remaining HP.'),
 (42, 'EN', 'Inflicts 40 points of damage.', 'Inflicts exactly 40 damage.'),
 (43, 'EN', 'Prevents the target from fleeing and inflicts damage for 2-5 turns.', 'Inflicts $damage damage. For the next 2–5 turns, the target cannot leave the field and is damaged for 1/16 its max HP at the end of each turn. The user continues to use other moves during this time. If the user leaves the field, this effect ends. Has a 3/8 chance each to hit 2 or 3 times, and a 1/8 chance each to hit 4 or 5 times. Averages to 3 hits per use. Rapid-spin move cancels this effect.'),
 (44, 'EN', 'Has an increased chance for a critical hit.', 'Inflicts $damage damage. User''s Critical hit rate is one level higher when using this move.'),
 (45, 'EN', 'Hits twice in one turn.', 'Inflicts $damage damage. Hits twice in one turn.'),
 (46, 'EN', 'If the user misses, it takes half the damage it would have inflicted in recoil.', 'Inflicts $damage damage. If this move misses, is blocked by protect or detect move, or has no effect, the user takes half the damage it would have inflicted in recoil. This recoil damage will not exceed half the user''s max HP. This move cannot be used while gravity move is in effect.'),
 (47, 'EN', 'Protects the user''s stats from being changed by enemy moves.', 'Pokémon on the user''s side of the field are immune to stat-lowering effects for five turns. Guard-swap, heart-swap, and power-swap move may still be used. Defog move used by an opponent will end this effect.'),
 (48, 'EN', 'Increases the user''s chance to score a critical hit.', 'User''s Critical hit rate is two levels higher until it leaves the field. If the user has already used focus-energy move since entering the field, this move will fail. This effect is passed on by baton-pass move.'),
 (49, 'EN', 'User receives 1/4 the damage it inflicts in recoil.', 'Inflicts $damage damage. User takes 1/4 the damage it inflicts in recoil.'),
 (50, 'EN', 'Confuses the target.', 'Confuses the target.'),
 (51, 'EN', 'Raises the user''s Attack by two stages.', 'Raises the user''s attack by two stages.'),
 (52, 'EN', 'Raises the user''s Defense by two stages.', 'Raises the user''s defense by two stages.'),
 (53, 'EN', 'Raises the user''s Speed by two stages.', 'Raises the user''s speed by two stages.'),
 (54, 'EN', 'Raises the user''s Special Attack by two stages.', 'Raises the user''s special attack by two stages.'),
 (55, 'EN', 'Raises the user''s Special Defense by two stages.', 'Raises the user''s special defense by two stages.'),
 (58, 'EN', 'User becomes a copy of the target until it leaves battle.', 'User copies the target''s species, weight, type, ability, calculated stats (except HP), and moves. Copied moves will all have 5 PP remaining. IVs are copied for the purposes of hidden-power move, but stats are not recalculated. Items choice-band, choice-scarf, and choice-specs stay in effect, and the user must select a new move. This move cannot be copied by mirror move, nor forced by encore move.'),
 (59, 'EN', 'Lowers the target''s Attack by two stages.', 'Lowers the target''s attack by two stages.'),
 (60, 'EN', 'Lowers the target''s Defense by two stages.', 'Lowers the target''s defense by two stages.'),
 (61, 'EN', 'Lowers the target''s Speed by two stages.', 'Lowers the target''s speed by two stages.'),
 (63, 'EN', 'Lowers the target''s Special Defense by two stages.', 'Lowers the target''s special defense by two stages.'),
 (66, 'EN', 'Reduces damage from physical attacks by half.', 'Erects a barrier around the user''s side of the field that reduces damage from physical attacks by half for five turns. In double battles, the reduction is 1/3. Critical hits are not affected by the barrier. If the user is holding light-clay item, the barrier lasts for eight turns. Brick-break or defog move used by an opponent will destroy the barrier.'),
 (67, 'EN', 'Poisons the target.', 'Poisons the target.'),
 (68, 'EN', 'Paralyzes the target.', 'Paralyzes the target.'),
 (69, 'EN', 'Has a $effect_chance% chance to lower the target''s Attack by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s attack by one stage.'),
 (70, 'EN', 'Has a $effect_chance% chance to lower the target''s Defense by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s defense by one stage.'),
 (71, 'EN', 'Has a $effect_chance% chance to lower the target''s Speed by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s speed by one stage.'),
 (72, 'EN', 'Has a $effect_chance% chance to lower the target''s Special Attack by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s special attack by one stage.'),
 (73, 'EN', 'Has a $effect_chance% chance to lower the target''s Special Defense by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s special defense by one stage.'),
 (74, 'EN', 'Has a $effect_chance% chance to lower the target''s accuracy by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s accuracy by one stage.'),
 (76, 'EN', 'User charges for one turn before attacking. Has a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. User charges for one turn before attacking. Critical hit chance is one level higher than normal. Has a $effect_chance% chance to make the target flinch. This move cannot be selected by sleep-talk move.'),
 (77, 'EN', 'Has a $effect_chance% chance to confuse the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to [confuse]{mechanic:confuse} the target.'),
 (78, 'EN', 'Hits twice in the same turn. Has a $effect_chance% chance to poison the target.', 'Inflicts $damage damage. Hits twice in the same turn. Has a $effect_chance% chance to poison the target.'),
 (79, 'EN', 'Never misses.', 'Inflicts $damage damage. Ignores accuracy and evasion modifiers.'),
 (80, 'EN', 'Transfers 1/4 of the user''s max HP into a doll, protecting the user from further damage or status changes until it breaks.', 'Transfers 1/4 the user''s max HP into a doll that absorbs damage and causes most negative move effects to fail. If the user leaves the field, the doll will vanish. If the user cannot pay the HP cost, this move will fail. The doll takes damage as normal, using the user''s stats and types, and will break when its HP reaches zero. Self-inflicted damage from confusion or recoil is not absorbed. Healing effects from opponents ignore the doll and heal the user as normal. Moves that work based on the user''s HP still do so; the doll''s HP does not influence any move. The doll will block major status effects, confusion, and flinching. The effects of smelling-salts and wake-up-slap move do not trigger against a doll, even if the Pokémon behind the doll has the appropriate major status effect. Multi-turn trapping moves like wrap will hit the doll for their $damage damage, but the multi-turn trapping and damage effects will not activate. Moves blocked or damage absorbed by the doll do not count as hitting the user or inflicting damage for any effects that respond to such, e.g., avalanche, counter move, or a rowap berry. Magic-coat move still works as normal, even against moves the doll would block. Opposing Pokémon that damage the doll with a leech move like absorb move are healed as normal. It will also block acupressure, block, the curse effect of curse, dream-eater, embargo, flatter, gastro-acid, grudge, heal-block, leech-seed, lock-on, mean-look, mimic, mind-reader, nightmare, pain-split, psycho-shift, spider-web, sketch, swagger, switcheroo, trick, worry-seed, and yawn move. A Pokémon affected by yawn move before summoning the doll will still fall to sleep. The doll blocks intimidate ability, but all other abilities act as though the doll did not exist. If the user has an ability that absorbs moves of a certain type for HP (such as volt-absorb absorbing thunder-wave move), such moves will not be blocked. Life-orb item and berries that cause confusion still work as normal, but their respective HP loss and confusion are absorbed/blocked by the doll. The user is still vulnerable to damage inflicted when entering or leaving the field, such as by pursuit or spikes move; however, the doll will block the poison effect of toxic-spikes move. The doll is passed on by baton-pass move. It keeps its existing HP, but uses the replacement Pokémon''s stats and types for damage calculation. All other effects work as normal.'),
 (81, 'EN', 'User foregoes its next turn to recharge.', 'Inflicts $damage damage. User loses its next turn to "recharge", and cannot attack or switch out during that turn.'),
 (82, 'EN', 'If the user is hit after using this move, its Attack rises by one stage.', 'Inflicts $damage damage. Every time the user is hit after it uses this move but before its next action, its attack raises by one stage.'),
 (83, 'EN', 'Copies the target''s last used move.', 'This move is replaced by the target''s last successfully used move, and its PP changes to 5. If the target hasn''t used a move since entering the field, if it tried to use a move this turn and failed, or if the user already knows the targeted move, this move will fail. This effect vanishes when the user leaves the field. If chatter, metronome, mimic, sketch, or struggle move is selected, this move will fail. This move cannot be copied by mirror-move, nor selected by assist or metronome, nor forced by encore move.'),
 (84, 'EN', 'Randomly selects and uses any move in the game.', 'Selects any move at random and uses it. Moves the user already knows are not eligible. Assist, meta, protection, and reflection moves are also not eligible; specifically, assist, chatter, copycat, counter, covet, destiny-bond, detect, endure, feint, focus-punch, follow-me, helping-hand, me-first, metronome, mimic, mirror-coat, mirror-move, protect, quick-guard, sketch, sleep-talk, snatch, struggle, switcheroo, thief, trick, and wide-guard move will not be selected by this move. This move cannot be copied by mimic or mirror move, nor selected by assist, metronome or sleep-talk move.'),
 (85, 'EN', 'Seeds the target, stealing HP from it every turn.', 'Plants a seed on the target that drains 1/8 of its max HP at the end of every turn and heals the user for the amount taken. Has no effect on grass Pokémon. The seed remains until the target leaves the field. The user takes damage instead of being healed if the target has liquid-ooze ability. Rapid-spin move will remove this effect. This effect is passed on by baton-pass move.'),
 (86, 'EN', 'Does nothing.', 'Does nothing. This move cannot be used while gravity is in effect.'),
 (87, 'EN', 'Disables the target''s last used move for 1-8 turns.', 'Disables the target''s last used move, preventing its use for 4–7 turns, selected at random, or until the target leaves the field. If the target hasn''t used a move since entering the field, if it tried to use a move this turn and failed, if its last used move has 0 PP remaining, or if it already has a move disabled, this move will fail.'),
 (88, 'EN', 'Inflicts damage equal to the user''s level.', 'Inflicts damage equal to the user''s level. Type immunity applies, but other type effects are ignored.'),
 (89, 'EN', 'Inflicts damage between 50% and 150% of the user''s level.', 'Inflicts typeless damage between 50% and 150% of the user''s level, selected at random in increments of 10%.'),
 (90, 'EN', 'Inflicts twice the damage the user received from the last physical hit it took.', 'Targets the last opposing Pokémon to hit the user with a physical move this turn. Inflicts twice the damage that move did to the user. If there is no eligible target, this move will fail. Type immunity applies, but other type effects are ignored. This move cannot be copied by mirror move, nor selected by assist or metronome move.'),
 (91, 'EN', 'Forces the target to repeat its last used move every turn for 2 to 6 turns.', 'The next 4–8 times (selected at random) the target attempts to move, it is forced to repeat its last used move. If the selected move allows the trainer to select a target, an opponent will be selected at random each turn. If the target is prevented from using the selected move by some other effect, struggle move will be used instead. This effect ends if the selected move runs out of PP. If the target hasn''t used a move since entering the field, if it tried to use a move this turn and failed, if it does not know the selected move, or if the selected move has 0 PP remaining, this move will fail. If the target''s last used move was encore, mimic, mirror, sketch, struggle, or transform, this move will fail.'),
 (92, 'EN', 'Sets the user''s and targets''s HP to the average of their current HP.', 'Changes the user''s and target''s remaining HP to the average of their current remaining HP. Ignores accuracy and evasion modifiers. This effect does not count as inflicting damage for other moves and effects that respond to damage taken. This effect fails against a substitute move.'),
 (93, 'EN', 'Has a $effect_chance% chance to make the target flinch. Only works if the user is sleeping.', 'Only usable if the user is sleeping. Inflicts $damage damage. Has a $effect_chance% chance to make the target flinch.'),
 (94, 'EN', 'Changes the user''s type to a random type either resistant or immune to the last move used against it.', 'Changes the user''s type to a type either resistant or immune to the last damaging move that hit it. The new type is selected at random and cannot be a type the user already is. If there is no eligible new type, this move will fail.'),
 (95, 'EN', 'Ensures that the user''s next move will hit the target.', 'If the user targets the same target again before the end of the next turn, the move it uses is guaranteed to hit. This move itself also ignores accuracy and evasion modifiers. One-hit KO moves are also guaranteed to hit, as long as the user is equal or higher level than the target. This effect also allows the user to hit Pokémon that are off the field due to moves such as dig or fly move. If the target uses detect or protect move while under the effect of this move, the user is not guaranteed to hit, but has a (100 - accuracy)% chance to break through the protection. This effect is passed on by baton-pass move.'),
 (96, 'EN', 'Permanently becomes the target''s last used move.', 'Permanently replaces itself with the target''s last used move. If that move is chatter or struggle move, this move will fail. This move cannot be copied by mimic or mirror move, nor selected by assist or metronome move, nor forced by encore move.'),
 (98, 'EN', 'Randomly uses one of the user''s other three moves. Only works if the user is sleeping.', 'Only usable if the user is sleeping. Randomly selects and uses one of the user''s other three moves. Use of the selected move requires and costs 0 PP. This move will not select assist, bide, bounce, chatter, copycat, dig, dive, fly, focus-punch, me-first, metronome, mirror, shadow-force, skull-bash, sky-attack, sky-drop, sleep-talk, solar-beam, razor-wind, or uproar. If the selected move requires a recharge turn—i.e., one of blast-burn, frenzy-plant, giga-impact, hydro-cannon, hyper-beam, roar-of-time, or rock-wrecker—and the user is still sleeping next turn, then it''s forced to use this move again and pay another PP for the recharge turn. This move cannot be copied by mirror move, nor selected by assist, metronome, or sleep-talk.'),
 (99, 'EN', 'If the user faints this turn, the target automatically will, too.', 'If the user faints before its next move, the Pokémon that fainted it will automatically faint. End-of-turn damage is ignored. This move cannot be selected by assist or metronome move.'),
 (100, 'EN', 'Inflicts more damage when the user has less HP remaining, with a maximum of 200 power.', 'Inflicts $damage damage. Power varies inversely with the user''s proportional remaining HP.\n64 * current HP / max HP | Power\n-----------------------: | ----:\n 0– 1                    |  200\n 2– 5                    |  150\n 6–12                    |  100\n13–21                    |   80\n22–42                    |   40\n43–64                    |   20\n'),
 (101, 'EN', 'Lowers the PP of the target''s last used move by 4.', 'Lowers the PP of the target''s last used move by 4. If the target hasn''t used a move since entering the field, if it tried to use a move this turn and failed, or if its last used move has 0 PP remaining, this move will fail.'),
 (102, 'EN', 'Cannot lower the target''s HP below 1.', 'Inflicts $damage damage. Will not reduce the target''s HP below 1.'),
 (103, 'EN', 'Cures the entire party of major status effects.', 'Removes major status effects and confusion from every Pokémon in the user''s party.'),
 (104, 'EN', 'Inflicts regular damage with no additional effect.', 'Inflicts $damage damage.'),
 (105, 'EN', 'Hits three times, increasing power by 100% with each successful hit.', 'Inflicts $damage damage. Hits three times in the same turn. The second hit has double power, and the third hit has triple power. Each hit has a separate accuracy check, and this move stops if a hit misses. Skill-link ability does not apply.'),
 (106, 'EN', 'Takes the target''s item.', 'Inflicts $damage damage. If the target is holding an item and the user is not, the user will permanently take the item. Damage is still inflicted if an item cannot be taken. Pokémon with sticky-hold ability or multitype ability are immune to the item theft effect. The target cannot recover its item with recycle move. This move cannot be selected by assist or metronome move.'),
 (107, 'EN', 'Prevents the target from leaving battle.', 'The target cannot switch out normally. Ignores accuracy and evasion modifiers. This effect ends when the user leaves the field. The target may still escape by using baton-pass, u-turn move, or a shed-shell item. Both the user and the target pass on this effect with baton-pass move.'),
 (108, 'EN', 'Target loses 1/4 its max HP every turn as long as it''s asleep.', 'Only works on sleeping Pokémon. Gives the target a nightmare, damaging it for 1/4 its max HP every turn. If the target wakes up or leaves the field, this effect ends.'),
 (109, 'EN', 'Raises the user''s evasion by two stages.', 'Raises the user''s evasion by two stages. Stomp and steamroller move have double power against Pokémon that have used this move since entering the field.'),
 (110, 'EN', 'Ghosts pay half their max HP to hurt the target every turn. Others decrease Speed but raise Attack and Defense.', 'If the user is a ghost: user pays half its max HP to place a curse on the target, damaging it for 1/4 its max HP every turn. Otherwise: Lowers the user''s speed by one stage, and raises its attack and defense by one stage each. The curse effect is passed on by baton-pass move. This move cannot be copied by mirror move.'),
 (112, 'EN', 'Prevents any moves from hitting the user this turn.', 'No moves will hit the user for the remainder of this turn. If the user is last to act this turn, this move will fail. If the user successfully used detect, endure, protect, quick-guard, or wide-guard move on the last turn, this move has a 50% chance to fail. Lock-on, mind-reader move, and no-guard ability provide a (100 – accuracy)% chance for moves to break through this move. This does not apply to one-hit KO moves (fissure, guillotine, horn-drill, and sheer-cold move); those are always blocked by this move. Thunder during rain-dance and blizzard during hail have a 30% chance to break through this move. The following effects are not prevented by this move: acupressure from an ally, curse''s curse effect, Delayed damage from doom-desire and future-sight; however, these moves will be prevented if they are used this turn, feint which will also end this move''s protection after it hits, imprison, perish-song, shadow-force, Moves that merely copy the user, such as transform or psych-up. This move cannot be selected by assist or metronome.'),
 (113, 'EN', 'Scatters Spikes, hurting opposing Pokémon that switch in.', 'Scatters spikes around the opposing field, which damage opposing Pokémon that enter the field for 1/8 of their max HP. Pokémon immune to ground moves are immune to this damage, except during gravity. Up to three layers of spikes may be laid down, adding 1/16 to the damage done: two layers of spikes damage for 3/16 max HP, and three layers damage for 1/4 max HP. Wonder-guard ability does not block damage from this effect. Rapid-spin move removes this effect from its user''s side of the field. Defog move removes this effect from its target''s side of the field.'),
 (114, 'EN', 'Forces the target to have no Evade, and allows it to be hit by Normal and Fighting moves even if it''s a Ghost.', 'Resets the target''s evasion to normal and prevents any further boosting until the target leaves the field. A ghost under this effect takes normal damage from normal and fighting moves. This move itself ignores accuracy and evasion modifiers.'),
 (115, 'EN', 'User and target both faint after three turns.', 'Every Pokémon is given a counter that starts at 3 and decreases by 1 at the end of every turn, including this one. When a Pokémon''s counter reaches zero, that Pokémon faints. A Pokémon that leaves the field will lose its counter; its replacement does not inherit the effect, and other Pokémon''s counters remain. This effect is passed on by baton-pass move. This move cannot be copied by mirror move.'),
 (116, 'EN', 'Changes the weather to a sandstorm for five turns.', 'Changes the weather to a sandstorm for five turns. Pokémon that are not ground, rock, or steel take 1/16 their max HP at the end of every turn. Every rock Pokémon''s original special defense is raised by 50% for the duration of this effect. Solar-beam''s power is halved. Moonlight, morning-sun, and synthesis move only heal 1/4 the user''s max HP.'),
 (117, 'EN', 'Prevents the user''s HP from lowering below 1 this turn.', 'The user''s HP cannot be lowered below 1 by any means for the remainder of this turn. If the user successfully used detect, endure, protect, quick-guard, or wide-guard move on the last turn, this move has a 50% chance to fail. This move cannot be selected by assist or metronome move.'),
 (118, 'EN', 'Power doubles every turn this move is used in succession after the first, resetting after five turns.', 'Inflicts $damage damage. User is forced to use this move for five turns. Power doubles every time this move is used in succession to a maximum of 16x, and resets to normal after the lock-in ends. If this move misses or becomes unusable, the lock-in ends. If the user has used defense-curl move since entering the field, this move has double power.'),
 (119, 'EN', 'Raises the target''s Attack by two stages and confuses the target.', 'Raises the target''s attack by two stages, then confuses it. If the target''s attack cannot be raised by two stages, the confusion is not applied.'),
 (120, 'EN', 'Power doubles every turn this move is used in succession after the first, maxing out after five turns.', 'Inflicts $damage damage. Power doubles after every time this move is used, whether consecutively or not, maxing out at 16x. If this move misses or the user leaves the field, power resets.'),
 (121, 'EN', 'Target falls in love if it has the opposite gender, and has a 50% chance to refuse attacking the user.', 'Causes the target to fall in love with the user, giving it a 50% chance to do nothing each turn. If the user and target are the same gender, or either is genderless, this move will fail. If either Pokémon leaves the field, this effect ends.'),
 (122, 'EN', 'Power increases with happiness, up to a maximum of 102.', 'Inflicts $damage damage. Power increases with happiness, given by `happiness * 2 / 5`, to a maximum of 102. Power bottoms out at 1.'),
 (123, 'EN', 'Randomly inflicts damage with power from 40 to 120 or heals the target for 1/4 its max HP.', 'Randomly uses one of the following effects.\nEffect                                             | Chance\n-------------------------------------------------- | -----:\nInflicts $damage damage with 40 power  |    40%\nInflicts $damage damage with 80 power  |    30%\nInflicts $damage damage with 120 power |    10%\nHeals the target for 1/4 its max HP    |    20%\nOn average, this move inflicts $damage damage with 52 power and heals the target for 1/20 its max HP.'),
-- TODO: Continue to check text from here...
 (124, 'EN', 'Power increases as happiness decreases, up to a maximum of 102.', 'Inflicts $damage damage. Power increases inversely with happiness, given by `(255 - happiness) * 2 / 5`, to a maximum of 102. Power bottoms out at 1.'),
 (125, 'EN', 'Protects the user''s field from major status ailments and confusion for five turns.', 'Protects Pokémon on the user''s side of the field from [major status]{mechanic:major-status} effects and confusion for five turns. Does not cancel existing ailments. This effect remains even if the user leaves the field.

If []{move:yawn} is used while this move is in effect, it will immediately fail.

[]{move:defog} used by an opponent will end this effect.

This effect does not prevent the confusion caused by []{move:outrage}, []{move:petal-dance}, or []{move:thrash}.'),
 (126, 'EN', 'Has a $effect_chance% chance to burn the target. Lets frozen Pokémon thaw themselves.', 'Inflicts $damage damage. Has a $effect_chance% chance to burn the target. [Frozen]{mechanic:frozen} Pokémon may use this move, in which case they will thaw.'),
 (127, 'EN', 'Power varies randomly from 10 to 150.', 'Inflicts $damage damage. Power is selected at random between 10 and 150, with an average of 71:

Magnitude | Power | Chance
--------: | ----: | -----:
        4 |    10 |     5%
        5 |    30 |    10%
        6 |    50 |    20%
        7 |    70 |    30%
        8 |    90 |    20%
        9 |   110 |    10%
       10 |   150 |     5%

This move has double power against Pokémon currently underground due to []{move:dig}.'),
 (128, 'EN', 'Allows the trainer to switch out the user and pass effects along to its replacement.', 'User [switches out]{mechanic:switches-out}, and the trainer selects a replacement Pokémon from the party. stat changes, confusion, and persistent move effects are passed along to the replacement Pokémon.

The following move effects are passed:

* []{move:aqua-ring}
* both the user''s and target''s effect of []{move:block}, []{move:mean-look}, and []{move:spider-web}
* the curse effect of []{move:curse}
* []{move:embargo}
* []{move:focus-energy} or an activated []{item:lansat-berry}
* []{move:gastro-acid}
* []{move:ingrain}
* being sapped by []{move:leech-seed}
* being targeted by []{move:lock-on} or []{move:mind-reader}
* []{move:magnet-rise}
* []{move:perish-song}''s counter
* []{move:power-trick}
* []{move:substitute}; the doll''s HP is unchanged

The replacement Pokémon does not trigger effects that respond to Pokémon switching in.'),
 (129, 'EN', 'Has double power against, and can hit, Pokémon attempting to switch out.', 'Inflicts $damage damage. If the target attempts to switch out this turn before the user acts, this move hits the target before it leaves and has double power.

This effect can still hit a Pokémon that [switches out]{mechanic:switches-out} when it has a []{move:substitute} up or when an ally has used []{move:follow-me}.'),
 (130, 'EN', 'Frees the user from binding moves, removes Leech Seed, and blows away Spikes.', 'Inflicts $damage damage. Removes []{move:leech-seed} from the user, frees the user from []{move:bind}, []{move:clamp}, []{move:fire-spin}, []{move:magma-storm}, []{move:sand-tomb}, []{move:whirlpool}, and []{move:wrap}, and clears []{move:spikes}, []{move:stealth-rock}, and []{move:toxic-spikes} from the user''s side of the field. If this move misses or has no effect, its effect doesn''t activate.'),
 (131, 'EN', 'Inflicts 20 points of damage.', 'Inflicts exactly 20 damage.'),
 (133, 'EN', 'Heals the user by half its max HP. Affected by weather.', 'Heals the user for half its max HP.

During []{move:sunny-day}, the healing is increased to 2/3 max HP.

During []{move:hail}, []{move:rain-dance}, or []{move:sandstorm}, the healing is decreased to 1/4 max HP.'),
 (136, 'EN', 'Power and type depend upon user''s IVs. Power can range from 30 to 70.', 'Inflicts $damage damage. Power and type are determined by the user''s IVs.

Power is given by `x * 40 / 63 + 30`. `x` is obtained by arranging bit 1 from the IV for each of special defense, special attack, speed, defense, attack, and HP in that order. (Bit 1 is 1 if the IV is of the form `4n + 2` or `4n + 3`. `x` is then 64 * special defense IV bit 1, plus 32 * special attack IV bit 1, etc.)

Power is always between 30 and 70, inclusive. Average power is 49.5.

Type is given by `y * 15 / 63`, where `y` is similar to `x` above, except constructed from bit 0. (Bit 0 is 1 if the IV is odd.)  The result is looked up in the following table.

Value | Type
----: | --------
    0 | fighting
    1 | []{type:flying}
    2 | []{type:poison}
    3 | ground
    4 | rock
    5 | []{type:bug}
    6 | ghost
    7 | steel
    8 | []{type:fire}
    9 | []{type:water}
   10 | grass
   11 | []{type:electric}
   12 | []{type:psychic}
   13 | []{type:ice}
   14 | []{type:dragon}
   15 | []{type:dark}

This move thus cannot be normal. Most other types have an equal 1/16 chance to be selected, given random IVs. However, due to the flooring used here, []{type:bug}, fighting, and grass appear 5/64 of the time, and []{type:dark} only 1/64 of the time.'),
 (137, 'EN', 'Changes the weather to rain for five turns.', 'Changes the weather to rain for five turns, during which []{type:water} moves inflict 50% extra damage, and []{type:fire} moves inflict half damage.

If the user is holding []{item:damp-rock}, this effect lasts for eight turns.

[]{move:thunder} has 100% accuracy. If the target has used []{move:detect} or []{move:protect}, []{move:thunder} has a (100 - accuracy)% chance to break through the protection.

[]{move:solar-beam} has half power.

[]{move:moonlight}, []{move:morning-sun}, and []{move:synthesis} heal only 1/4 of the user''s max HP.

Pokémon with []{ability:swift-swim} have doubled original speed.

Pokémon with []{ability:forecast} become []{type:water}.

Pokémon with []{ability:dry-skin} heal 1/8 max HP, Pokémon with []{ability:hydration} are cured of major status effects, and Pokémon with []{ability:rain-dish} heal 1/16 max HP at the end of each turn.'),
 (138, 'EN', 'Changes the weather to sunny for five turns.', 'Changes the weather to sunshine for five turns, during which []{type:fire} moves inflict 50% extra damage, and []{type:water} moves inflict half damage.

If the user is holding []{item:heat-rock}, this effect lasts for eight turns.

Pokémon cannot become [frozen]{mechanic:frozen}.

[]{move:thunder} has 50% accuracy.

[]{move:solar-beam} skips its charge turn.

[]{move:moonlight}, []{move:morning-sun}, and []{move:synthesis} heal 2/3 of the user''s max HP.

Pokémon with []{ability:chlorophyll} have doubled original speed.

Pokémon with []{ability:forecast} become []{type:fire}.

Pokémon with []{ability:leaf-guard} are not affected by major status effects.

Pokémon with []{ability:flower-gift} change form; every Pokémon on their side of the field have their original attack and special attack increased by 50%.

Pokémon with []{ability:dry-skin} lose 1/8 max HP at the end of each turn.

Pokémon with []{ability:solar-power} have their original special attack raised by 50% but lose 1/8 their max HP at the end of each turn.'),
 (139, 'EN', 'Has a $effect_chance% chance to raise the user''s Defense by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to raise the user''s defense one stage.'),
 (140, 'EN', 'Has a $effect_chance% chance to raise the user''s Attack by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to raise the user''s attack one stage.'),
 (141, 'EN', 'Has a $effect_chance% chance to raise all of the user''s stats by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to raise all of the user''s stats one stage.'),
 (143, 'EN', 'User pays half its max HP to max out its Attack.', 'User pays half its max HP to raise its attack to +6 stages. If the user cannot pay the HP cost, this move will fail.'),
 (144, 'EN', 'Discards the user''s stat changes and copies the target''s.', 'Discards the user''s [stat changes]{mechanic:stat-changes} and copies the target''s.

This move cannot be copied by []{move:mirror-move}.'),
 (145, 'EN', 'Inflicts twice the damage the user received from the last special hit it took.', 'Targets the last opposing Pokémon to hit the user with a special move this turn. Inflicts twice the damage that move did to the user. If there is no eligible target, this move will fail. Type immunity applies, but other type effects are ignored.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist} or []{move:metronome}.'),
 (146, 'EN', 'Raises the user''s Defense by one stage. User charges for one turn before attacking.', 'Inflicts $damage damage. Raises the user''s defense by one stage. User then charges for one turn before attacking.

This move cannot be selected by []{move:sleep-talk}.'),
 (147, 'EN', 'Has a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. Has a $effect_chance% chance to make each target flinch.

If the target is under the effect of []{move:bounce}, []{move:fly}, or []{move:sky-drop}, this move will hit with double power.'),
 (148, 'EN', 'Inflicts regular damage and can hit Dig users.', 'Inflicts $damage damage.

If the target is in the first turn of []{move:dig}, this move will hit with double power.'),
 (149, 'EN', 'Hits the target two turns later.', 'Inflicts typeless $damage damage at the end of the third turn, starting with this one. This move cannot score a Critical hit. If the target [switches out]{mechanic:switches-out}, its replacement will be hit instead. Damage is calculated at the time this move is used; [stat changes]{mechanic:stat-changes} and [switching out]{mechanic:switching-out} during the delay won''t change the damage inflicted. No move with this effect can be used against the same target again until after the end of the third turn.

This effect breaks through []{ability:wonder-guard}.

If the target is protected by []{move:protect} or []{move:detect} on the turn this move is used, this move will fail. However, the damage on the third turn will break through protection.

The damage is applied at the end of the turn, so it ignores []{move:endure} and []{item:focus-sash}.

This move cannot be copied by []{move:mirror-move}.'),
 (150, 'EN', 'Inflicts regular damage and can hit Pokémon in the air.', 'Inflicts $damage damage.

If the target is under the effect of []{move:bounce}, []{move:fly}, or []{move:sky-drop}, this move will hit with double power.'),
 (151, 'EN', 'Has a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. Has a $effect_chance% chance to make the target flinch.

Power is doubled against Pokémon that have used []{move:minimize} since entering the field.'),
 (152, 'EN', 'Requires a turn to charge before attacking.', 'Inflicts $damage damage. User charges for one turn before attacking.

During []{move:sunny-day}, the charge turn is skipped.

During []{move:hail}, []{move:rain-dance}, or []{move:sandstorm}, power is halved.

This move cannot be selected by []{move:sleep-talk}.'),
 (153, 'EN', 'Has a $effect_chance% chance to paralyze the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to paralyze the target.

During []{move:rain-dance}, this move has 100% accuracy. It also has a (100 - accuracy)% chance to break through the protection of []{move:protect} and []{move:detect}.

During []{move:sunny-day}, this move has 50% accuracy.'),
 (154, 'EN', 'Immediately ends wild battles. No effect otherwise.', 'Does nothing. Wild battles end immediately.'),
 (155, 'EN', 'Hits once for every conscious Pokémon the trainer has.', 'Inflicts typeless $damage damage. Every Pokémon in the user''s party, excepting those that have fainted or have a major status effect, attacks the target. Calculated stats are ignored; the base stats for the target and assorted attackers are used instead. The random factor in the damage formula is not used. []{type:dark} Pokémon still get [STAB]{mechanic:stab}.

This effect breaks through []{ability:wonder-guard}.'),
 (156, 'EN', 'User flies high into the air, dodging all attacks, and hits next turn.', 'Inflicts $damage damage. User flies high into the air for one turn, becoming immune to attack, and hits on the second turn.

During the immune turn, []{move:gust}, []{move:hurricane}, []{move:sky-uppercut}, []{move:smack-down}, []{move:thunder}, []{move:twister}, and []{move:whirlwind} still hit the user normally. []{move:gust} and []{move:twister} also have double power against the user.

The damage from []{move:hail} and []{move:sandstorm} still applies during the immune turn.

The user may be hit during its immune turn if under the effect of []{move:lock-on}, []{move:mind-reader}, or []{ability:no-guard}.

This move cannot be used while []{move:gravity} is in effect.

This move cannot be selected by []{move:sleep-talk}.'),
 (157, 'EN', 'Raises user''s Defense by one stage.', 'Raises user''s defense by one stage.

After this move is used, the power of []{move:ice-ball} and []{move:rollout} are doubled until the user leaves the field.'),
 (159, 'EN', 'Can only be used as the first move after the user enters battle. Causes the target to flinch.', 'Inflicts $damage damage. Causes the target to flinch. Can only be used on the user''s first turn after entering the field.'),
 (160, 'EN', 'Forced to use this move for several turns. Pokémon cannot fall asleep in that time.', 'Inflicts $damage damage. User is forced to use this move for 2–5 turns, selected at random. All Pokémon on the field wake up, and none can fall to sleep until the lock-in ends.

Pokémon cannot use []{move:rest} during this effect.

This move cannot be selected by []{move:sleep-talk}.'),
 (161, 'EN', 'Stores energy up to three times for use with Spit Up and Swallow.', 'Raises the user''s defense and special defense by one stage each. Stores energy for use with []{move:spit-up} and []{move:swallow}. Up to three levels of energy can be stored, and all are lost if the user leaves the field. Energy is still stored even if the stat boosts cannot be applied.

If the user uses []{move:baton-pass}, the stat boosts are passed as normal, but the stored energy is not.'),
 (162, 'EN', 'Power is 100 times the amount of energy Stockpiled.', 'Inflicts $damage damage. Power is equal to 100 times the amount of energy stored by []{move:stockpile}. Ignores the random factor in the damage formula. Stored energy is consumed, and the user''s defense and special defense are reset to what they would be if []{move:stockpile} had not been used. If the user has no energy stored, this move will fail.

This move cannot be copied by []{move:mirror-move}.'),
 (163, 'EN', 'Recovers 1/4 HP after one Stockpile, 1/2 HP after two Stockpiles, or full HP after three Stockpiles.', 'Heals the user depending on the amount of energy stored by []{move:stockpile}: 1/4 its max HP after one use, 1/2 its max HP after two uses, or fully after three uses. Stored energy is consumed, and the user''s defense and special defense are reset to what they would be if []{move:stockpile} had not been used. If the user has no energy stored, this move will fail.'),
 (165, 'EN', 'Changes the weather to a hailstorm for five turns.', 'Changes the weather to hail for five turns, during which non-[]{type:ice} Pokémon are damaged for 1/16 their max HP at the end of every turn.

If the user is holding []{item:icy-rock}, this effect lasts for eight turns.

[]{move:blizzard} has 100% accuracy. If the target has used []{move:detect} or []{move:protect}, []{move:blizzard} has a (100 - accuracy)% chance to break through the protection.

[]{move:moonlight}, []{move:morning-sun}, and []{move:synthesis} heal only 1/4 of the user''s max HP.

Pokémon with []{ability:snow-cloak} are exempt from this effect''s damage.'),
 (166, 'EN', 'Prevents the target from using the same move twice in a row.', 'Prevents the target from attempting to use the same move twice in a row. When the target leaves the field, this effect ends.

If the target is forced to attempt a repeated move due to []{item:choice-band}, []{item:choice-scarf}, []{item:choice-specs}, []{move:disable}, []{move:encore}, []{move:taunt}, only having PP remaining for one move, or any other effect, the target will use []{move:struggle} instead. The target is then free to use the forced move next turn, as it didn''t use that move this turn.'),
 (167, 'EN', 'Raises the target''s Special Attack by one stage and confuses the target.', 'Raises the target''s special attack by one stage, then confuses it.'),
 (168, 'EN', 'Burns the target.', '[Burns]{mechanic:burns} the target.'),
 (169, 'EN', 'Lowers the target''s Attack and Special Attack by two stages. User faints.', 'Lowers the target''s attack and special attack by two stages. User faints.'),
 (170, 'EN', 'Power doubles if user is burned, paralyzed, or poisoned.', 'Inflicts $damage damage. If the user is [burned]{mechanic:burned}, [paralyzed]{mechanic:paralyzed}, or [poisoned]{mechanic:poisoned}, this move has double power.'),
 (171, 'EN', 'If the user takes damage before attacking, the attack is canceled.', 'Inflicts $damage damage. If the user takes damage this turn before hitting, this move will fail.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist}, []{move:metronome}, or []{move:sleep-talk}.'),
 (172, 'EN', 'If the target is paralyzed, inflicts double damage and cures the paralysis.', 'Inflicts $damage damage. If the target is [paralyzed]{mechanic:paralyzed}, this move has double power, and the target is cured of its [paralysis]{mechanic:paralysis}.'),
 (173, 'EN', 'Redirects the target''s single-target effects to the user for this turn.', 'Until the end of this turn, any moves that opposing Pokémon target solely at the user''s ally will instead target the user. If both Pokémon on the same side of the field use this move on the same turn, the Pokémon that uses it last will become the target.

This effect takes priority over []{ability:lightning-rod} and []{ability:storm-drain}.

If the user''s ally [switches out]{mechanic:switches-out}, opposing Pokémon may still hit it with []{move:pursuit}.

This move cannot be selected by []{move:assist} or []{move:metronome}.'),
 (174, 'EN', 'Uses a move which depends upon the terrain.', 'Uses another move chosen according to the terrain.

Terrain                   | Selected move
------------------------- | ------------------
Building                  | []{move:tri-attack}
Cave                      | []{move:rock-slide}
Deep water                | []{move:hydro-pump}
Desert                    | []{move:earthquake}
Grass                     | []{move:seed-bomb}
Mountain                  | []{move:rock-slide}
Road                      | []{move:earthquake}
Shallow water             | []{move:hydro-pump}
Snow                      | []{move:blizzard}
Tall grass                | []{move:seed-bomb}
[]{move:electric-terrain} | []{move:thunderbolt}
[]{move:grassy-terrain}   | []{move:energy-ball}
[]{move:misty-terrain}    | []{move:moonblast}

In Pokémon Battle Revolution:

Terrain        | Selected move
-------------- | ------------------
Courtyard      | []{move:tri-attack}
Crystal        | []{move:rock-slide}
Gateway        | []{move:hydro-pump}
Magma          | []{move:rock-slide}
Main Street    | []{move:tri-attack}
Neon           | []{move:tri-attack}
Stargazer      | []{move:rock-slide}
Sunny Park     | []{move:seed-bomb}
Sunset         | []{move:earthquake}
Waterfall      | []{move:seed-bomb}

This move cannot be copied by []{move:mirror-move}.'),
 (175, 'EN', 'Raises the user''s Special Defense by one stage. User''s Electric moves have doubled power next turn.', 'Raises the user''s special defense by one stage. If the user uses an []{type:electric} move next turn, its power will be doubled.'),
 (176, 'EN', 'For the next few turns, the target can only use damaging moves.', 'Target is forced to only use damaging moves for the next 3–5 turns, selected at random. Moves that select other moves not known in advance do not count as damaging.

[]{move:assist}, []{move:copycat}, []{move:me-first}, []{move:metronome}, []{move:mirror-move}, and []{move:sleep-talk} do not directly inflict damage and thus may not be used.

[]{move:bide}, []{move:counter}, []{move:endeavor}, []{move:metal-burst}, and []{move:mirror-coat} are allowed.'),
 (177, 'EN', 'Ally''s next move inflicts half more damage.', 'Boosts the power of the target''s moves by 50% until the end of this turn.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist} or []{move:metronome}.'),
 (178, 'EN', 'User and target swap items.', 'User and target permanently swap [held item]{mechanic:held-item}s. Works even if one of the Pokémon isn''t holding anything. If either Pokémon is holding mail, this move will fail.

If either Pokémon has []{ability:multitype} or []{ability:sticky-hold}, this move will fail.

If this move results in a Pokémon obtaining []{item:choice-band}, []{item:choice-scarf}, or []{item:choice-specs}, and that Pokémon was the latter of the pair to move this turn, then the move it used this turn becomes its chosen forced move. This applies even if both Pokémon had a choice item before this move was used. If the first of the two Pokémon gains a choice item, it may select whatever choice move it wishes next turn.

Neither the user nor the target can recover its item with []{move:recycle}.

This move cannot be selected by []{move:assist} or []{move:metronome}.'),
 (179, 'EN', 'Copies the target''s ability.', 'User''s ability is replaced with the target''s until the user leaves the field. Ignores accuracy and evasion modifiers.

If the target has []{ability:flower-gift}, []{ability:forecast}, []{ability:illusion}, []{ability:imposter}, []{ability:multitype}, []{ability:stance-change}, []{ability:trace}, []{ability:wonder-guard}, or []{ability:zen-mode}, this move will fail.

This move cannot be copied by []{move:mirror-move}.'),
 (180, 'EN', 'User will recover half its max HP at the end of the next turn.', 'At the end of the next turn, user will be healed for half its max HP. If the user is [switched out]{mechanic:switched-out}, its replacement will be healed instead for half of the user''s max HP. If the user faints or is forcefully switched by []{move:roar} or []{move:whirlwind}, this effect will not activate.'),
 (181, 'EN', 'Randomly selects and uses one of the trainer''s other Pokémon''s moves.', 'Uses a move from another Pokémon in the user''s party, both selected at random. Moves from fainted Pokémon can be used. If there are no eligible Pokémon or moves, this move will fail.

This move will not select []{move:assist}, []{move:chatter}, []{move:circle-throw}, []{move:copycat}, []{move:counter}, []{move:covet}, []{move:destiny-bond}, []{move:detect}, []{move:dig}, []{move:dive}, []{move:dragon-tail}, []{move:endure}, []{move:feint}, []{move:fly} []{move:focus-punch}, []{move:follow-me}, []{move:helping-hand}, []{move:me-first}, []{move:metronome}, []{move:mimic}, []{move:mirror-coat}, []{move:mirror-move}, []{move:phantom-force} []{move:protect}, []{move:quick-guard}, []{move:roar} []{move:shadow-force}, []{move:sketch}, []{move:sleep-talk}, []{move:snatch}, []{move:struggle}, []{move:switcheroo}, []{move:thief}, []{move:trick}, []{move:whirlwind}, or []{move:wide-guard}.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:metronome} or []{move:sleep-talk}.'),
 (182, 'EN', 'Prevents the user from leaving battle. User regains 1/16 of its max HP every turn.', 'Prevents the user from [switching out]{mechanic:switching-out}. User regains 1/16 of its max HP at the end of every turn. If the user was immune to ground attacks, it will now take normal damage from them.

[]{move:roar} and []{move:whirlwind} will not affect the user. The user cannot use []{move:magnet-rise}.

The user may still use []{move:u-turn} to leave the field.

This effect can be passed with []{move:baton-pass}.'),
 (183, 'EN', 'Lowers the user''s Attack and Defense by one stage after inflicting damage.', 'Inflicts $damage damage, then lowers the user''s attack and defense by one stage each.'),
 (184, 'EN', 'Reflects back the first effect move used on the user this turn.', 'The first non-damaging move targeting the user this turn that inflicts major status effects, [stat change]{mechanic:stat-change}s, or [trap]{mechanic:trap}ping effects will be reflected at its user.

[]{move:defog}, []{move:memento}, and []{move:teeter-dance} are not reflected.

[]{move:attract}, []{move:flatter}, []{move:gastro-acid}, []{move:leech-seed}, []{move:swagger}, []{move:worry-seed}, and []{move:yawn} are reflected.

This move cannot be copied by []{move:mirror-move}.'),
 (185, 'EN', 'User recovers the item it last used up.', 'User recovers the last item consumed by the user or a Pokémon in its position on the field. The item must be used again before it can be recovered by this move again. If the user is holding an item, this move fails.

Items taken or given away by []{move:covet}, []{move:knock-off}, []{move:switcheroo}, []{move:thief}, or []{move:trick} may not be recovered.'),
 (186, 'EN', 'Inflicts double damage if the user takes damage before attacking this turn.', 'Inflicts $damage damage. If the target damaged the user this turn and was the last to do so, this move has double power.

[]{move:pain-split} does not count as damaging the user.'),
 (187, 'EN', 'Destroys Reflect and Light Screen.', 'Destroys any []{move:light-screen} or []{move:reflect} on the target''s side of the field, then inflicts $damage damage.'),
 (188, 'EN', 'Target sleeps at the end of the next turn.', 'Puts the target to sleep at the end of the next turn. Ignores accuracy and evasion modifiers. If the target leaves the field, this effect is canceled. If the target has a status effect when this move is used, this move will fail.

If the target is protected by []{move:safeguard} when this move is used, this move will fail.

[]{ability:insomnia} and []{ability:vital-spirit} prevent the sleep if the target has either at the end of the next turn, but will not cause this move to fail on use.'),
 (189, 'EN', 'Target drops its held item.', 'Inflicts $damage damage. Target loses its [held item]{mechanic:held-item}.

Neither the user nor the target can recover its item with []{move:recycle}.

If the target has []{ability:multitype} or []{ability:sticky-hold}, it will take damage but not lose its item.'),
 (190, 'EN', 'Lowers the target''s HP to equal the user''s.', 'Inflicts exactly enough damage to lower the target''s HP to equal the user''s. If the target''s HP is not higher than the user''s, this move has no effect. Type immunity applies, but other type effects are ignored. This effect counts as damage for moves that respond to damage.'),
 (191, 'EN', 'Inflicts more damage when the user has more HP remaining, with a maximum of 150 power.', 'Inflicts $damage damage. Power increases with the user''s remaining HP and is given by `150 * HP / max HP`, to a maximum of 150 when the user has full HP.'),
 (192, 'EN', 'User and target swap abilities.', 'User and target switch abilities. Ignores accuracy and evasion modifiers.

If either Pokémon has []{ability:multitype} or []{ability:wonder-guard}, this move will fail.'),
 (193, 'EN', 'Prevents the target from using any moves that the user also knows.', 'Prevents any Pokémon on the opposing side of the field from using any move the user knows until the user leaves the field. This effect is live; if the user obtains new moves while on the field, these moves become restricted. If no opposing Pokémon knows any of the user''s moves when this move is used, this move will fail.'),
 (194, 'EN', 'Cleanses the user of a burn, paralysis, or poison.', 'Removes a burn, [paralysis]{mechanic:paralysis}, or poison from the user.'),
 (195, 'EN', 'If the user faints this turn, the PP of the move that fainted it drops to 0.', 'If the user faints before it next acts, the move that fainted it will have its PP dropped to 0. End-of-turn damage does not trigger this effect.'),
 (196, 'EN', 'Steals the target''s move, if it''s self-targeted.', 'The next time a Pokémon uses a beneficial move on itself or itself and its ally this turn, the user of this move will steal the move and use it itself. Moves which may be stolen by this move are identified by the "snatchable" flag.

If two Pokémon use this move on the same turn, the faster Pokémon will steal the first beneficial move, and the slower Pokémon will then steal it again—thus, only the slowest Pokémon using this move ultimately gains a stolen move''s effect.

If the user steals []{move:psych-up}, it will target the Pokémon that used []{move:psych-up}. If the user was the original target of []{move:psych-up}, and the Pokémon that originally used it''s affected by []{ability:pressure}, it will only lose 1 PP.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist} or []{move:metronome}.'),
 (197, 'EN', 'Inflicts more damage to heavier targets, with a maximum of 120 power.', 'Inflicts $damage damage. Power increases with the target''s weight in kilograms, to a maximum of 120.

Target''s weight | Power
--------------- | ----:
Up to 10kg      |    20
Up to 25kg      |    40
Up to 50kg      |    60
Up to 100kg     |    80
Up to 200kg     |   100
Above 200kg     |   120
'),
 (198, 'EN', 'Has a $effect_chance% chance to inflict a status effect which depends upon the terrain.', 'Inflicts $damage damage. Has a $effect_chance% chance to inflict an effect chosen according to the terrain.

Terrain        | Effect
-------------- | -------------------------------------------------------------
Building       | paralyzes target
Cave           | Makes target flinch
Deep water     | Lowers target''s attack by one stage
Desert         | Lowers target''s accuracy by one stage
Grass          | Puts target to sleep
Mountain       | Makes target flinch
Road           | Lowers target''s accuracy by one stage
Shallow water  | Lowers target''s attack by one stage
Snow           | [Freezes]{mechanic:freezes} target
Tall grass     | Puts target to sleep

In Pokémon Battle Revolution:

Terrain        | Effect
-------------- | -------------------------------------------------------------
Courtyard      | paralyzes target
Crystal        | Makes target flinch
Gateway        | Lowers target''s attack by one stage
Magma          | Makes target flinch
Main Street    | paralyzes target
Neon           | paralyzes target
Stargazer      | Makes target flinch
Sunny Park     | Puts target to sleep
Sunset         | Lowers target''s accuracy by one stage
Waterfall      | Puts target to sleep
'),
 (199, 'EN', 'User receives 1/3 the damage inflicted in recoil.', 'Inflicts $damage damage. User takes 1/3 the damage it inflicts in recoil.'),
 (200, 'EN', 'Confuses the target.', 'Confuses all targets.'),
 (201, 'EN', 'Has an increased chance for a critical hit and a $effect_chance% chance to burn the target.', 'Inflicts $damage damage. User''s Critical hit rate is one level higher when using this move. Has a $effect_chance% chance to burn the target.'),
 (202, 'EN', 'Halves all Electric-type damage.', '[]{type:electric} moves inflict half damage, regardless of target. If the user leaves the field, this effect ends.'),
 (203, 'EN', 'Has a $effect_chance% chance to badly poison the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to [badly poison]{mechanic:badly-poison} the target.'),
 (204, 'EN', 'If there be weather, this move has doubled power and the weather''s type.', 'Inflicts $damage damage. If a weather move is active, this move has double power, and its type becomes the type of the weather move. []{move:shadow-sky} is typeless for the purposes of this move.'),
 (205, 'EN', 'Lowers the user''s Special Attack by two stages after inflicting damage.', 'Inflicts $damage damage, then lowers the user''s special attack by two stages.'),
 (206, 'EN', 'Lowers the target''s Attack and Defense by one stage.', 'Lowers the target''s attack and defense by one stage.'),
 (207, 'EN', 'Raises the user''s Defense and Special Defense by one stage.', 'Raises the user''s defense and special defense by one stage.'),
 (208, 'EN', 'Inflicts regular damage and can hit Bounce and Fly users.', 'Inflicts $damage damage.

This move can hit Pokémon under the effect of []{move:bounce}, []{move:fly}, or []{move:sky-drop}.'),
 (209, 'EN', 'Raises the user''s Attack and Defense by one stage.', 'Raises the user''s attack and defense by one stage each.'),
 (210, 'EN', 'Has an increased chance for a critical hit and a $effect_chance% chance to poison the target.', 'Inflicts $damage damage. User''s Critical hit rate is one level higher when using this move. Has a $effect_chance% chance to poison the target.'),
 (211, 'EN', 'Halves all Fire-type damage.', '[]{type:fire} moves inflict half damage, regardless of target. If the user leaves the field, this effect ends.'),
 (212, 'EN', 'Raises the user''s Special Attack and Special Defense by one stage.', 'Raises the user''s special attack and special defense by one stage each.'),
 (213, 'EN', 'Raises the user''s Attack and Speed by one stage.', 'Raises the user''s attack and speed by one stage each.'),
 (214, 'EN', 'User''s type changes to match the terrain.', 'User''s type changes according to the terrain.

Terrain        | New type
-------------- | --------------
Building       | normal
Cave           | rock
Desert         | ground
Grass          | grass
Mountain       | rock
Ocean          | []{type:water}
Pond           | []{type:water}
Road           | ground
Snow           | []{type:ice}
Tall grass     | grass

In Pokémon Battle Revolution:

Terrain        | New type
-------------- | --------------
Courtyard      | normal
Crystal        | rock
Gateway        | []{type:water}
Magma          | rock
Main Street    | normal
Neon           | normal
Stargazer      | rock
Sunny Park     | grass
Sunset         | ground
Waterfall      | grass
'),
 (215, 'EN', 'Heals the user by half its max HP.', 'Heals the user for half its max HP. If the user is []{type:flying}, its []{type:flying} type is ignored until the end of this turn.'),
 (216, 'EN', 'Disables moves and immunities that involve flying or levitating for five turns.', 'For five turns (including this one), all immunities to ground moves are disabled. For the duration of this effect, the evasion of every Pokémon on the field is lowered by two stages. Cancels the effects of []{move:bounce}, []{move:fly}, and []{move:sky-drop}.

Specifically, []{type:flying} Pokémon and those with []{ability:levitate} or that have used []{move:magnet-rise} are no longer immune to ground attacks, []{ability:arena-trap}, []{move:spikes}, or []{move:toxic-spikes}.

[]{move:bounce}, []{move:fly}, []{move:sky-drop}, []{move:high-jump-kick}, []{move:jump-kick}, and []{move:splash} cannot be used while this move is in effect.

*Bug*: If this move is used during a double or triple battle while Pokémon are under the effect of []{move:sky-drop}, Sky Drop''s effect is not correctly canceled on its target, and it remains high in the air indefinitely. As Sky Drop prevents the target from acting, the only way to subsequently remove it from the field is to faint it.'),
 (217, 'EN', 'Forces the target to have no evasion, and allows it to be hit by Psychic moves even if it''s Dark.', 'Resets the target''s evasion to normal and prevents any further boosting until the target leaves the field. A []{type:dark} Pokémon under this effect takes normal damage from []{type:psychic} moves. This move itself ignores accuracy and evasion modifiers.'),
 (218, 'EN', 'If the target is asleep, has double power and wakes it up.', 'Inflicts $damage damage. If the target is sleeping, this move has double power, and the target wakes up.'),
 (219, 'EN', 'Lowers user''s Speed by one stage.', 'Inflicts $damage damage, then lowers the user''s speed by one stage.'),
 (220, 'EN', 'Power raises when the user has lower Speed, up to a maximum of 150.', 'Inflicts $damage damage. Power increases with the target''s current speed compared to the user, given by `1 + 25 * target Speed / user Speed`, capped at 150.'),
 (221, 'EN', 'User faints. Its replacement has its HP fully restored and any major status effect removed.', 'User faints. Its replacement''s HP is fully restored, and any major status effect is removed. If the replacement Pokémon is immediately fainted by a switch-in effect, the next replacement is healed by this move instead.'),
 (222, 'EN', 'Has double power against Pokémon that have less than half their max HP remaining.', 'Inflicts $damage damage. If the target has less than half its max HP remaining, this move has double power.'),
 (223, 'EN', 'Power and type depend on the held berry.', 'Inflicts $damage damage. Power and type are determined by the user''s held berry. The berry is consumed. If the user is not holding a berry, this move will fail.'),
 (224, 'EN', 'Hits through Protect and Detect.', 'Inflicts $damage damage. Removes the effects of []{move:detect} or []{move:protect} from the target before hitting.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist} or []{move:metronome}.'),
 (225, 'EN', 'If target has a berry, inflicts double damage and uses the berry.', 'Inflicts $damage damage. If the target is holding a berry, this move has double power, and the user takes the berry and uses it immediately.

If the target is holding a []{item:jaboca-berry} or []{item:rowap-berry}, the berry is still removed, but has no effect.

If this move is [super effective]{mechanic:super-effective} and the target is holding a berry that can reduce this move''s damage, it will do so, and will not be stolen.'),
 (226, 'EN', 'For three turns, friendly Pokémon have doubled Speed.', 'For the next three turns, all Pokémon on the user''s side of the field have their original speed doubled. This effect remains if the user leaves the field.'),
 (227, 'EN', 'Raises one of a friendly Pokémon''s stats at random by two stages.', 'Raises one of the target''s stats by two stages. The raised stat is chosen at random from any stats that can be raised by two stages. If no stat is eligible, this move will fail.

If the target has a []{move:substitute}, this move will have no effect, even if the user is the target.

This move cannot be copied by []{move:mirror-move}.'),
 (228, 'EN', 'Strikes back at the last Pokémon to hit the user this turn with 1.5× the damage.', 'Targets the last opposing Pokémon to hit the user with a damaging move this turn. Inflicts 1.5× the damage that move did to the user. If there is no eligible target, this move will fail. Type immunity applies, but other type effects are ignored.'),
 (229, 'EN', 'User must switch out after attacking.', 'Inflicts $damage damage, then the user immediately [switches out]{mechanic:switches-out}, and the trainer selects a replacement Pokémon from the party. If the target faints from this attack, the user''s trainer selects the new Pokémon to send out first. If the user is the last Pokémon in its party that can battle, it will not switch out.

The user may be hit by []{move:pursuit} when it [switches out]{mechanic:switches-out}, if it has been targeted and []{move:pursuit} has not yet been used.

This move may be used even if the user is under the effect of []{move:ingrain}. []{move:ingrain}''s effect will end.'),
 (230, 'EN', 'Lowers the user''s Defense and Special Defense by one stage after inflicting damage.', 'Inflicts $damage damage, then lowers the user''s defense and special defense by one stage each.'),
 (231, 'EN', 'Power is doubled if the target has already moved this turn.', 'Inflicts $damage damage. If the target uses a move or [switches out]{mechanic:switches-out} this turn before this move is used, this move has double power.'),
 (232, 'EN', 'Power is doubled if the target has already received damage this turn.', 'Inflicts $damage damage. If the target takes damage this turn for any reason before this move is used, this move has double power.'),
 (233, 'EN', 'Target cannot use held items.', 'Target cannot use its held item for five turns. If the target leaves the field, this effect ends.

If a Pokémon under this effect uses []{move:bug-bite} or []{move:pluck} on a Pokémon holding a berry, the berry is destroyed but not used. If a Pokémon under this effect uses []{move:fling}, it will fail.

This effect is passed by []{move:baton-pass}.'),
 (234, 'EN', 'Throws held item at the target; power depends on the item.', 'Inflicts $damage damage. Power and type are determined by the user''s [held item]{mechanic:held-item}. The item is consumed. If the user is not holding an item, or its item has no set type and power, this move will fail.

This move ignores []{ability:sticky-hold}.

If the user is under the effect of []{move:embargo}, this move will fail.'),
 (235, 'EN', 'Transfers the user''s major status effect to the target.', 'If the user has a major status effect and the target does not, the user''s status is transferred to the target.'),
 (236, 'EN', 'Power increases when this move has less PP, up to a maximum of 200.', 'Inflicts $damage damage. Power is determined by the PP remaining for this move, after its PP cost is deducted. Ignores accuracy and evasion modifiers.

PP remaining | Power
------------ | ----:
4 or more    |    40
3            |    50
2            |    60
1            |    80
0            |   200

If this move is activated by another move, the activating move''s PP is used to calculate power.'),
 (237, 'EN', 'Prevents target from restoring its HP for five turns.', 'For the next five turns, the target may not use any moves that only restore HP, and move effects that heal the target are disabled. Moves that steal HP may still be used, but will only inflict damage and not heal the target.'),
 (238, 'EN', 'Power increases against targets with more HP remaining, up to a maximum of 121 power.', 'Inflicts $damage damage. Power directly relates to the target''s relative remaining HP, given by `1 + 120 * current HP / max HP`, to a maximum of 121.'),
 (239, 'EN', 'User swaps Attack and Defense.', 'The user''s original attack and defense are swapped.

This effect is passed on by []{move:baton-pass}.'),
 (240, 'EN', 'Nullifies target''s ability until it leaves battle.', 'The target''s ability is disabled as long as it remains on the field.

This effect is passed on by []{move:baton-pass}.'),
 (241, 'EN', 'Prevents the target from scoring critical hits for five turns.', 'For five turns, opposing Pokémon cannot score [critical hits]{mechanic:critical-hit}.'),
 (242, 'EN', 'Uses the target''s move against it before it attacks, with power increased by half.', 'If the target has selected a damaging move this turn, the user will copy that move and use it against the target, with a 50% increase in power.

If the target moves before the user, this move will fail.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist}, []{move:metronome}, or []{move:sleep-talk}.'),
 (243, 'EN', 'Uses the target''s last used move.', 'Uses the last move that was used successfully by any Pokémon, including the user.

This move cannot copy itself, nor []{move:roar} nor []{move:whirlwind}.

This move cannot be copied by []{move:mirror-move}, nor selected by []{move:assist}, []{move:metronome}, or []{move:sleep-talk}.'),
 (244, 'EN', 'User swaps Attack and Special Attack changes with the target.', 'User swaps its attack and special attack [stat modifiers]{mechanic:stat-modifiers} modifiers with the target.'),
 (245, 'EN', 'User swaps Defense and Special Defense changes with the target.', 'User swaps its defense and special defense modifiers with the target.'),
 (246, 'EN', 'Power increases against targets with more raised stats, up to a maximum of 200.', 'Inflicts $damage damage. Power starts at 60 and is increased by 20 for every stage any of the target''s stats has been raised, capping at 200. accuracy and evasion modifiers do not increase this move''s power.'),
 (247, 'EN', 'Can only be used after all of the user''s other moves have been used.', 'Inflicts $damage damage. This move can only be used if each of the user''s other moves has been used at least once since the user entered the field. If this is the user''s only move, this move will fail.'),
 (248, 'EN', 'Changes the target''s ability to Insomnia.', 'Changes the target''s ability to []{ability:insomnia}.

If the target''s ability is []{ability:truant} or []{ability:multitype}, this move will fail.'),
 (249, 'EN', 'Only works if the target is about to use a damaging move.', 'Inflicts $damage damage. If the target has not selected a damaging move this turn, or if the target has already acted this turn, this move will fail.

This move is not affected by []{ability:iron-fist}.'),
 (250, 'EN', 'Scatters poisoned spikes, poisoning opposing Pokémon that switch in.', 'Scatters poisoned spikes around the opposing field, which poison opposing Pokémon that enter the field. A second layer of these spikes may be laid down, in which case Pokémon will be [badly poison]{mechanic:badly-poison}ed instead. Pokémon immune to either ground moves or being poisoned are immune to this effect. Pokémon otherwise immune to ground moves are affected during []{move:gravity}.

If a []{type:poison} Pokémon not immune to ground moves enters a field covered with poisoned spikes, the spikes are removed.

[]{move:rapid-spin} will remove this effect from its user''s side of the field. []{move:defog} will remove this effect from its target''s side of the field.

This move does not trigger []{ability:synchronize}, unless the Pokémon with []{ability:synchronize} was forced to enter the field by another effect such as []{move:roar}.

Pokémon entering the field due to []{move:baton-pass} are not affected by this effect.'),
 (251, 'EN', 'User and target swap stat changes.', 'User swaps its [stat modifiers]{mechanic:stat-modifiers} with the target.'),
 (252, 'EN', 'Restores 1/16 of the user''s max HP each turn.', 'Restores 1/16 of the user''s max HP at the end of each turn. If the user leaves the field, this effect ends.

This effect is passed on by []{move:baton-pass}.'),
 (253, 'EN', 'User is immune to Ground moves and effects for five turns.', 'For five turns, the user is immune to ground moves.

If the user is under the effect of []{move:ingrain} or has []{ability:levitate}, this move will fail.

This effect is temporarily disabled by and cannot be used during []{move:gravity}.

This effect is passed on by []{move:baton-pass}.'),
 (254, 'EN', 'User takes 1/3 the damage inflicted in recoil. Has a $effect_chance% chance to burn the target.', 'Inflicts $damage damage. User takes 1/3 the damage it inflicts in recoil. Has a $effect_chance% chance to burn the target. [Frozen]{mechanic:frozen} Pokémon may use this move, in which case they will thaw.'),
 (255, 'EN', 'User takes 1/4 its max HP in recoil.', 'Inflicts typeless $damage damage. User takes 1/4 its max HP in recoil. Ignores accuracy and evasion modifiers.

This move is used automatically when a Pokémon cannot use any other move legally, e.g., due to having no PP remaining or being under the effect of both []{move:encore} and []{move:torment} at the same time.

This move''s recoil is not treated as recoil for the purposes of anything that affects recoil, such as the ability []{ability:rock-head}. It also is not prevented by []{ability:magic-guard}.

This move cannot be copied by []{move:mimic}, []{move:mirror-move}, or []{move:sketch}, nor selected by []{move:assist} or []{move:metronome}, nor forced by []{move:encore}.'),
 (256, 'EN', 'User dives underwater, dodging all attacks, and hits next turn.', 'Inflicts $damage damage. User dives underwater for one turn, becoming immune to attack, and hits on the second turn.

During the immune turn, []{move:surf}, and []{move:whirlpool} still hit the user normally, and their power is doubled if appropriate.

The user may be hit during its immune turn if under the effect of []{move:lock-on}, []{move:mind-reader}, or []{ability:no-guard}.

This move cannot be selected by []{move:sleep-talk}.'),
 (257, 'EN', 'User digs underground, dodging all attacks, and hits next turn.', 'Inflicts $damage damage. User digs underground for one turn, becoming immune to attack, and hits on the second turn.

During the immune turn, []{move:earthquake}, []{move:fissure}, and []{move:magnitude} still hit the user normally, and their power is doubled if appropriate.

The user may be hit during its immune turn if under the effect of []{move:lock-on}, []{move:mind-reader}, or []{ability:no-guard}.

This move cannot be selected by []{move:sleep-talk}.'),
 (258, 'EN', 'Inflicts regular damage and can hit Dive users.', 'Inflicts $damage damage.

If the target is in the first turn of []{move:dive}, this move will hit with double power.'),
 (259, 'EN', 'Lowers the target''s evasion by one stage. Removes field effects from the enemy field.', 'Lowers the target''s evasion by one stage. Clears away fog. Removes the effects of []{move:mist}, []{move:light-screen}, []{move:reflect}, []{move:safeguard}, []{move:spikes}, []{move:stealth-rock}, and []{move:toxic-spikes} from the target''s side of the field.

If the target is protected by []{move:mist}, it will prevent the evasion change, then be removed by this move.'),
 (260, 'EN', 'For five turns, slower Pokémon will act before faster Pokémon.', 'For five turns (including this one), slower Pokémon will act before faster Pokémon. Move priority is not affected. Using this move when its effect is already active will end the effect.

Pokémon holding []{item:full-incense}, []{item:lagging-tail}, or []{item:quick-claw} and Pokémon with []{ability:stall} ignore this effect.'),
 (261, 'EN', 'Has a $effect_chance% chance to freeze the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to freeze the target.

During []{move:hail}, this move has 100% accuracy. It also has a (100 - accuracy)% chance to break through the protection of []{move:protect} and []{move:detect}.'),
 (262, 'EN', 'Prevents the target from leaving battle and inflicts 1/16 its max HP in damage for 2-5 turns.', 'Inflicts $damage damage. For the next 2–5 turns, the target cannot leave the field and is damaged for 1/16 its max HP at the end of each turn. The user continues to use other moves during this time. If the user leaves the field, this effect ends.

Has a 3/8 chance each to hit 2 or 3 times, and a 1/8 chance each to hit 4 or 5 times. Averages to 3 hits per use.

If the target is in the first turn of []{move:dive}, this move will hit with double power.'),
 (263, 'EN', 'User takes 1/3 the damage inflicted in recoil. Has a $effect_chance% chance to paralyze the target.', 'Inflicts $damage damage. User takes 1/3 the damage it inflicts in recoil. Has a $effect_chance% chance to paralyze the target.'),
 (264, 'EN', 'User bounces high into the air, dodging all attacks, and hits next turn.', 'Inflicts $damage damage. User bounces high into the air for one turn, becoming immune to attack, and hits on the second turn. Has a $effect_chance% chance to paralyze the target.

During the immune turn, []{move:gust}, []{move:hurricane}, []{move:sky-uppercut}, []{move:smack-down}, []{move:thunder}, and []{move:twister} still hit the user normally. []{move:gust} and []{move:twister} also have double power against the user.

The damage from []{move:hail} and []{move:sandstorm} still applies during the immune turn.

The user may be hit during its immune turn if under the effect of []{move:lock-on}, []{move:mind-reader}, or []{ability:no-guard}.

This move cannot be used while []{move:gravity} is in effect.

This move cannot be selected by []{move:sleep-talk}.'),
 (266, 'EN', 'Lowers the target''s Special Attack by two stages if it''s the opposite gender.', 'Lowers the target''s special attack by two stages. If the user and target are the same gender, or either is genderless, this move will fail.'),
 (267, 'EN', 'Causes damage when opposing Pokémon switch in.', 'Spreads sharp rocks around the opposing field, damaging any Pokémon that enters the field for 1/8 its max HP. This damage is affected by the entering Pokémon''s susceptibility to rock moves.

[]{move:rapid-spin} removes this effect from its user''s side of the field.'),
 (268, 'EN', 'Has a higher chance to confuse the target when the recorded sound is louder.', 'Inflicts $damage damage. Has either a 1%, 11%, or 31% chance to [confuse]{mechanic:confuse} the target, based on the volume of the recording made for this move; louder recordings increase the chance of confusion. If the user is not a []{pokemon:chatot}, this move will not cause confusion.

This move cannot be copied by []{move:mimic}, []{move:mirror-move}, or []{move:sketch}, nor selected by []{move:assist}, []{move:metronome}, or []{move:sleep-talk}.'),
 (269, 'EN', 'If the user is holding a appropriate plate or drive, the damage inflicted will match it.', 'Inflicts $damage damage. If the user is holding a plate or a drive, this move''s type is the type corresponding to that item.

Note: This effect is technically shared by both []{move:techno-blast} and []{move:judgment}; however, Techno Blast is only affected by drives, and Judgment is only affected by plates.'),
 (270, 'EN', 'User receives 1/2 the damage inflicted in recoil.', 'Inflicts $damage damage. User takes 1/2 the damage it inflicts in recoil.'),
 (271, 'EN', 'User faints, and its replacement is fully healed.', 'User faints. Its replacement''s HP and PP are fully restored, and any major status effect is removed.'),
 (272, 'EN', 'Has a $effect_chance% chance to lower the target''s Special Defense by two stages.', 'Inflicts $damage damage. Has a $effect_chance% chance to lower the target''s special defense by two stages.'),
 (273, 'EN', 'User vanishes, dodging all attacks, and hits next turn. Hits through Protect and Detect.', 'Inflicts $damage damage. User vanishes for one turn, becoming immune to attack, and hits on the second turn.

This move ignores the effects of []{move:detect} and []{move:protect}.

This move cannot be selected by []{move:sleep-talk}.'),
 (274, 'EN', 'Has a $effect_chance% chance to burn the target and a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. Has a $effect_chance% chance to burn the target and a separate $effect_chance% chance to make the target flinch.'),
 (275, 'EN', 'Has a $effect_chance% chance to freeze the target and a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. Has a $effect_chance% chance to freeze the target and a separate $effect_chance% chance to make the target flinch.'),
 (276, 'EN', 'Has a $effect_chance% chance to paralyze the target and a $effect_chance% chance to make the target flinch.', 'Inflicts $damage damage. Has a $effect_chance% chance to paralyze the target and a separate $effect_chance% chance to make the target flinch.'),
 (277, 'EN', 'Has a $effect_chance% chance to raise the user''s Special Attack by one stage.', 'Inflicts $damage damage. Has a $effect_chance% chance to raise the user''s special attack by one stage.'),
 (278, 'EN', 'Raises the user''s Attack and accuracy by one stage.', 'Raises the user''s attack and accuracy by one stage.'),
 (279, 'EN', 'Prevents any multi-target moves from hitting friendly Pokémon this turn.', 'Moves with multiple targets will not hit friendly Pokémon for the remainder of this turn. If the user is last to act this turn, this move will fail.

This move cannot be selected by []{move:assist} or []{move:metronome}.'),
 (280, 'EN', 'Averages Defense and Special Defense with the target.', 'Averages the user''s unmodified defense with the target''s unmodified Defense; the value becomes the unmodified Defense for both Pokémon. Unmodified special defense is averaged the same way.'),
 (281, 'EN', 'Averages Attack and Special Attack with the target.', 'Averages the user''s unmodified attack with the target''s unmodified Attack; the value becomes the unmodified Attack for both Pokémon. Unmodified special attack is averaged the same way.

This effect applies before any other persistent changes to unmodified Attack or Special Attack, such as []{ability:flower-gift} during []{move:sunny-day}.'),
 (282, 'EN', 'All Pokémon''s Defense and Special Defense are swapped for 5 turns.', 'For five turns (including this one), every Pokémon''s defense and special defense are swapped.'),
 (283, 'EN', 'Inflicts damage based on the target''s Defense, not Special Defense.', 'Inflicts $damage damage. Damage calculation always uses the target''s defense, regardless of this move''s damage class.'),
 (284, 'EN', 'Inflicts double damage if the target is Poisoned.', 'Inflicts $damage damage. If the target is [poisoned]{mechanic:poisoned}, this move has double power.'),
 (285, 'EN', 'Raises the user''s Speed by two stages and halves the user''s weight.', 'Raises the user''s speed by two stages. Halves the user''s weight; this effect does not stack.'),
 (286, 'EN', 'Moves have 100% accuracy against the target for three turns.', 'For three turns (including this one), moves used against the target have 100% accuracy, but the target is immune to ground damage. Accuracy of one-hit KO moves is exempt from this effect.

This effect is removed by []{move:gravity}. If Gravity is already in effect, this move will fail.'),
 (287, 'EN', 'Negates held items for five turns.', 'For five turns (including this one), passive effects of held items are ignored, and Pokémon will not use their held items.'),
 (288, 'EN', 'Removes any immunity to Ground damage.', 'Inflicts $damage damage. Removes the target''s [immunity]{mechanic:immune} to ground-type damage. This effect removes any existing Ground immunity due to []{ability:levitate}, []{move:magnet-rise}, or []{move:telekinesis}, and causes the target''s []{type:flying} type to be ignored when it takes Ground damage.

If the target isn''t immune to Ground damage, this move will fail.

This move can hit Pokémon under the effect of []{move:bounce}, []{move:fly}, or []{move:sky-drop}, and ends the effect of Bounce or Fly.'),
 (289, 'EN', 'Always scores a critical hit.', 'Inflicts $damage damage. Always scores a Critical hit.'),
 (290, 'EN', 'Deals splash damage to Pokémon next to the target.', 'Inflicts $damage damage. If this move successfully hits the target, any Pokémon adjacent to the target are damaged for 1/16 their max HP.'),
 (291, 'EN', 'Raises the user''s Special Attack, Special Defense, and Speed by one stage each.', 'Raises the user''s special attack, special defense, and speed by one stage each.'),
 (292, 'EN', 'Power is higher when the user weighs more than the target, up to a maximum of 120.', 'Inflicts $damage damage. The greater the user''s weight compared to the target''s, the higher power this move has, to a maximum of 120.

User''s weight                    | Power
-------------------------------- | ----:
Up to 2× the target''s weight     |    40
Up to 3× the target''s weight     |    60
Up to 4× the target''s weight     |    80
Up to 5× the target''s weight     |   100
More than 5× the target''s weight |   120
'),
 (293, 'EN', 'Hits any Pokémon that shares a type with the user.', 'Inflicts $damage damage. Only Pokémon that share a type with the user will take damage from this move.'),
 (294, 'EN', 'Power is higher when the user has greater Speed than the target, up to a maximum of 150.', 'Inflicts $damage damage. The greater the user''s speed compared to the target''s, the higher power this move has, to a maximum of 150.

User''s Speed                     | Power
-------------------------------- | ----:
Up to 2× the target''s Speed      |    60
Up to 3× the target''s Speed      |    80
Up to 4× the target''s Speed      |   120
More than 4× the target''s Speed  |   150
'),
 (295, 'EN', 'Changes the target''s type to Water.', 'Changes the target to pure []{type:water}-type until it leaves the field. If the target has []{ability:multitype}, this move will fail.'),
 (296, 'EN', 'Inflicts regular damage. Raises the user''s Speed by one stage.', 'Inflicts $damage damage. Raises the user''s speed by one stage.'),
 (297, 'EN', 'Lowers the target''s Special Defense by two stages.', 'Inflicts $damage damage. Lowers the target''s special defense by two stages.'),
 (298, 'EN', 'Calculates damage with the target''s attacking stat.', 'Inflicts $damage damage. Damage is calculated using the target''s attacking stat rather than the user''s.'),
 (299, 'EN', 'Changes the target''s ability to Simple.', 'Changes the target''s ability to []{ability:simple}.'),
 (300, 'EN', 'Copies the user''s ability onto the target.', 'Changes the target''s ability to match the user''s. This effect ends when the target leaves battle.'),
 (301, 'EN', 'Makes the target act next this turn.', 'The target will act next this turn, regardless of speed or move priority.
If the target has already acted this turn, this move will fail.'),
 (302, 'EN', 'Has double power if it''s used more than once per turn.', 'Inflicts $damage damage. If []{move:round} has already been used this turn, this move''s power is doubled. After this move is used, any other Pokémon using it this turn will immediately do so (in the order they would otherwise act), regardless of speed or priority. Pokémon using other moves will then continue to act as usual.'),
 (303, 'EN', 'Power increases by 100% for each consecutive use by any friendly Pokémon, to a maximum of 200.', 'Inflicts $damage damage. If any friendly Pokémon used this move earlier this turn or on the previous turn, that use''s power is added to this move''s power, to a maximum of 200.'),
 (304, 'EN', 'Ignores the target''s stat modifiers.', 'Inflicts $damage damage. Damage calculation ignores the target''s [stat modifiers]{mechanic:stat-modifiers}, including evasion.'),
 (305, 'EN', 'Removes all of the target''s stat modifiers.', 'Inflicts $damage damage. All of the target''s [stat modifiers]{mechanic:stat-modifiers} are reset to zero.'),
 (306, 'EN', 'Power is higher the more the user''s stats have been raised, to a maximum of 31×.', 'Inflicts $damage damage. Power is increased by 100% its original value for every stage any of the user''s stats have been raised. accuracy, evasion, and lowered stats do not affect this move''s power. For a Pokémon with all five stats modified to +6, this move''s power is 31×.'),
 (307, 'EN', 'Prevents any priority moves from hitting friendly Pokémon this turn.', 'Moves with priority greater than 0 will not hit friendly Pokémon for the remainder of this turn. If the user is last to act this turn, this move will fail.

This move cannot be selected by []{move:assist} or []{move:metronome}.'),
 (308, 'EN', 'User switches places with the friendly Pokémon opposite it.', 'User switches position on the field with the friendly Pokémon opposite it. If the user is in the middle position in a triple battle, or there are no other friendly Pokémon, this move will fail.'),
 (309, 'EN', 'Raises user''s Attack, Special Attack, and Speed by two stages. Lower user''s Defense and Special Defense by one stage.', 'Raises the user''s attack, special attack, and speed by two stages each. Lowers the user''s defense and special defense by one []{mechanic:stage} each.'),
 (310, 'EN', 'Heals the target for half its max HP.', 'Heals the target for half its max HP.'),
 (311, 'EN', 'Has double power if the target has a major status ailment.', 'Inflicts $damage damage. If the target has a [major status ailment]{mechanic:major-status-ailment}, this move has double power.'),
 (312, 'EN', 'Carries the target high into the air, dodging all attacks against either, and drops it next turn.', 'Inflicts $damage damage. User carries the target high into the air for one turn, during which no moves will hit either Pokémon and neither can act. On the following turn, the user drops the target, inflicting damage and ending the effect.

If the target is []{type:flying}-type, this move will function as normal but inflict no damage.

[]{move:gust}, []{move:hurricane}, []{move:sky-uppercut}, []{move:smack-down}, []{move:thunder}, []{move:twister}, and []{move:whirlwind} can hit both the user and the target during this effect. []{move:gust} and []{move:twister} will additionally have double power.

The damage from []{move:hail} and []{move:sandstorm} still applies during this effect.

Either Pokémon may be hit during this effect if also under the effect of []{move:lock-on}, []{move:mind-reader}, or []{ability:no-guard}.

This move cannot be used while []{move:gravity} is in effect.

This move cannot be selected by []{move:sleep-talk}.

*Bug*: If []{move:gravity} is used during a double or triple battle while this move is in effect, this move is not correctly canceled on the target, and it remains high in the air indefinitely. As this move prevents the target from acting, the only way to subsequently remove it from the field is to faint it.'),
 (313, 'EN', 'Raises the user''s Attack by one stage and its Speed by two stages.', 'Raises the user''s attack by one stage and its speed by two stages.'),
 (314, 'EN', 'Ends wild battles. Forces trainers to switch Pokémon.', 'Inflicts $damage damage, then [switches]{mechanic:switch-out} the target out for another of its trainer''s Pokémon, selected at random.

If the target is under the effect of []{move:ingrain} or []{ability:suction-cups}, or it has a []{move:substitute}, or its Trainer has no more usable Pokémon, it will not be switched out. If the target is a wild Pokémon, the battle ends instead.'),
 (315, 'EN', 'Destroys the target''s held berry.', 'Inflicts $damage damage. If the target is [holding]{mechanic:held-item} a [berry]{mechanic:berry}, it''s destroyed and cannot be used in response to this move.'),
 (316, 'EN', 'Makes the target act last this turn.', 'Forces the target to act last this turn, regardless of speed or move [priority]{mechanic:priority}. If the target has already acted this turn, this move will fail.'),
 (317, 'EN', 'Raises the user''s Attack and Special Attack by one stage.', 'Raises the user''s attack and special attack by one stage each. During []{move:sunny-day}, raises both stats by two stages.'),
 (318, 'EN', 'Has double power if the user has no held item.', 'Inflicts $damage damage. If the user has no [held item]{mechanic:held-item}, this move has double power.'),
 (319, 'EN', 'User becomes the target''s type.', 'User''s type changes to match the target''s.'),
 (320, 'EN', 'Has double power if a friendly Pokémon fainted last turn.', 'Inflicts $damage damage. If a friendly Pokémon fainted on the previous turn, this move has double power.'),
 (321, 'EN', 'Inflicts damage equal to the user''s remaining HP. User faints.', 'Inflicts damage equal to the user''s remaining HP. User faints.'),
 (322, 'EN', 'Raises the user''s Special Attack by three stages.', 'Raises the user''s special attack by three stages.'),
 (323, 'EN', 'Raises the user''s Attack, Defense, and accuracy by one stage each.', 'Raises the user''s attack, defense, and accuracy by one stage each.'),
 (324, 'EN', 'Gives the user''s held item to the target.', 'Transfers the user''s [held item]{mechanic:held-item} to the target. If the user has no held item, or the target already has a held item, this move will fail.'),
 (325, 'EN', 'With [Grass Pledge]{move:grass-pledge}, halves opposing Pokémon''s Speed for four turns.', 'Inflicts $damage damage. If a friendly Pokémon used []{move:grass-pledge} earlier this turn, all opposing Pokémon have halved speed for four turns (including this one).'),
 (326, 'EN', 'With [Water Pledge]{move:water-pledge}, doubles the effect chance of friendly Pokémon''s moves for four turns.', 'Inflicts $damage damage. If a friendly Pokémon used []{move:water-pledge} earlier this turn, moves used by any friendly Pokémon have doubled effect chance for four turns (including this one).'),
 (327, 'EN', 'With [Fire Pledge]{move:fire-pledge}, damages opposing Pokémon for 1/8 their max HP every turn for four turns.', 'Inflicts $damage damage. If a friendly Pokémon used []{move:fire-pledge} earlier this turn, all opposing Pokémon will take 1/8 their max HP in damage at the end of every turn for four turns (including this one).'),
 (328, 'EN', 'Raises the user''s Attack and Special Attack by one stage each.', 'Raises the user''s attack and special attack by one stage each.'),
 (329, 'EN', 'Raises the user''s Defense by three stages.', 'Raises the user''s defense by three stages.'),
 (330, 'EN', 'Has a $effect_chance% chance to put the target to sleep.', 'Inflicts $damage damage. Has a $effect_chance% chance to put the target to sleep.
If the user is a []{pokemon:meloetta}, it will toggle between Aria and Pirouette Forme.'),
 (331, 'EN', 'Lowers the target''s Speed by one stage.', 'Inflicts $damage damage. Lowers the target''s speed by one stage.'),
 (332, 'EN', 'Requires a turn to charge before attacking. Has a $effect_chance% chance to paralyze the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to paralyze the target. User charges for one turn before attacking.'),
 (333, 'EN', 'Requires a turn to charge before attacking. Has a $effect_chance% chance to burn the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to burn the target. User charges for one turn before attacking.'),
 (335, 'EN', 'Lowers the user''s Defense, Special Defense, and Speed by one stage each.', 'Inflicts $damage damage. Lowers the user''s defense, special defense, and speed by one stage each.'),
 (336, 'EN', 'With [Fusion Bolt]{move:fusion-bolt}, inflicts double damage.', 'Inflicts $damage damage. If a friendly Pokémon used []{move:fusion-bolt} earlier this turn, this move has double power.'),
 (337, 'EN', 'With [Fusion Flare]{move:fusion-flare}, inflicts double damage.', 'Inflicts $damage damage. If a friendly Pokémon used []{move:fusion-flare} earlier this turn, this move has double power.'),
 (338, 'EN', 'Has a $effect_chance% chance to confuse the target.', 'Inflicts $damage damage. Has a $effect_chance% chance to [confuse]{mechanic:confuse} the target.

This move can hit Pokémon under the effect of []{move:bounce}, []{move:fly}, or []{move:sky-drop}.

During []{move:rain-dance}, this move has 100% accuracy. During []{move:sunny-day}, this move has 50% accuracy.'),
 (339, 'EN', 'Deals both fighting and []{type:flying}-type damage.', 'Inflicts regular damage. For the purposes of type effectiveness, this move is both fighting- and []{type:flying}-type: its final effectiveness is determined by multiplying the effectiveness of each type against each of the target''s types.

For all other purposes, this move is pure Fighting-type. If this move''s type is changed, its Fighting typing is overwritten, and its secondary type remains Flying.

If the target has used []{move:minimize} since entering battle, this move has double power and will never miss.'),
 (340, 'EN', 'Protects all friendly Pokémon from damaging moves. Only works on the first turn after the user is sent out.', 'Protects all friendly Pokémon from damaging moves. Only works on the first turn after the user is sent out.'),
 (341, 'EN', 'Can only be used after the user has eaten a berry.', 'Inflicts regular damage. Can only be used if the user has eaten a berry since the beginning of the battle.

After the user eats a berry, it may use this move any number of times until the end of the battle, even if it switches out. Eating a held berry, eating a berry via []{move:bug-bite} or []{move:pluck}, or being the target of a [Flung]{move:fling} berry will enable this move. Feeding a Pokémon a berry from the bag or using []{move:natural-gift} will not.

If the trainer chooses this move when it cannot be used, the choice is rejected outright and the trainer must choose another move.'),
 (342, 'EN', 'Raises the Attack and Special Attack of all grass Pokémon in battle.', 'Raises the Attack and Special Attack of all grass Pokémon in battle.'),
 (343, 'EN', 'Covers the opposing field, lowering opponents'' Speed by one stage upon switching in.', 'Shoots a web over the opponents'' side of the field, which lowers the Speed of any opposing Pokémon that enters the field by one stage.

Pokémon in the air, such as []{type:flying}-types and those with []{ability:levitate}, are unaffected. []{move:rapid-spin} removes Sticky Web from the user''s side of the field; []{move:defog} removes it from both sides.'),
 (344, 'EN', 'Raises the user''s Attack by two stages if it KOs the target.', 'Inflicts regular damage. Raises the user''s Attack by two stages if it KOs the target.'),
 (345, 'EN', 'Adds ghost to the target''s types.', 'Adds ghost to the target''s types.'),
 (346, 'EN', 'Lowers the target''s Attack and Special Attack by one stage.', 'Lowers the target''s Attack and Special Attack by one stage.'),
 (347, 'EN', 'Changes all normal moves to []{type:electric} moves for the rest of the turn.', 'Changes all Pokémon''s normal moves to []{type:electric} moves for the rest of the turn.'),
 (348, 'EN', 'Heals the user for half the total damage dealt to all targets.', 'Heals the user for half the total damage dealt to all targets.'),
 (349, 'EN', 'Adds grass to the target''s types.', 'Adds grass to the target''s types.'),
 (350, 'EN', 'Super-effective against []{type:water}.', 'Deals regular damage. This move is super-effective against the []{type:water} type.

The target''s other type will affect damage as usual. If this move''s type is changed, it remains super-effective against Water regardless of its type.'),
 (351, 'EN', 'Lowers all targets'' Attack and Special Attack by one stage. Makes the user switch out.', 'Lowers all targets'' Attack and Special Attack by one stage. Makes the user switch out.'),
 (352, 'EN', 'Inverts the target''s stat modifiers.', 'Inverts the target''s stat modifiers.'),
 (353, 'EN', 'Drains 75% of the damage inflicted to heal the user.', 'Deals regular damage. Drains 75% of the damage inflicted to heal the user.'),
 (354, 'EN', 'Protects all friendly Pokémon from non-damaging moves.', 'Protects all friendly Pokémon from non-damaging moves for the rest of the turn.

Unlike other blocking moves, this move may be used consecutively without its chance of success falling.'),
 (355, 'EN', 'Raises the Defense of all grass Pokémon in battle.', 'Raises the Defense of all grass Pokémon in battle.'),
 (356, 'EN', 'For five turns, heals all Pokémon on the ground for 1/16 max HP each turn and strengthens their grass moves to 1.5× their power.', 'For five turns, heals all Pokémon on the ground for 1/16 their max HP each turn and strengthens their grass moves to 1.5× their power.

Changes []{move:nature-power} to []{move:energy-ball}.'),
 (357, 'EN', 'For five turns, protects all Pokémon on the ground from major status ailments and confusion, and halves the power of incoming []{type:dragon} moves.', 'For five turns, protects all Pokémon on the ground from major status ailments and confusion and weakens []{type:dragon} moves used against them to 0.5× their power.

Changes []{move:nature-power} to []{move:moonblast}.'),
 (358, 'EN', 'Changes the target''s move''s type to []{type:electric} if it hasn''t moved yet this turn.', 'Changes the target''s move''s type to []{type:electric} if it hasn''t moved yet this turn.'),
 (359, 'EN', 'Prevents all Pokémon from fleeing or switching out during the next turn.', 'Prevents all Pokémon from fleeing or switching out during the next turn.'),
 (360, 'EN', 'Blocks damaging attacks and lowers attacking Pokémon''s Attack by two stages on contact. Switches Aegislash to Shield Forme.', 'Blocks damaging attacks and lowers attacking Pokémon''s Attack by two stages on contact. Switches Aegislash to Shield Forme.'),
 (361, 'EN', 'Lowers the target''s Special Attack by one stage.', 'Lowers the target''s Special Attack by one stage.'),
 (362, 'EN', 'Blocks damaging attacks and damages attacking Pokémon for 1/8 their max HP.', 'Blocks damaging attacks and damages attacking Pokémon for 1/8 their max HP.'),
 (363, 'EN', 'Raises a selected ally''s Special Defense by one stage.', 'Raises a selected ally''s Special Defense by one stage.'),
 (364, 'EN', 'Lowers the target''s Attack, Special Attack, and Speed by one stage if it is poisoned.', 'Lowers the target''s Attack, Special Attack, and Speed by one stage if it is poisoned.'),
 (365, 'EN', 'Explodes if the target uses a []{type:fire} move this turn, damaging it for 1/4 its max HP and preventing the move.', 'Explodes if the target uses a []{type:fire} move this turn, damaging it for 1/4 its max HP and preventing the move.'),
 (366, 'EN', 'Takes one turn to charge, then raises the user''s Special Attack, Special Defense, and Speed by two stages.', 'Takes one turn to charge, then raises the user''s Special Attack, Special Defense, and Speed by two stages.'),
 (367, 'EN', 'Raises the Defense and Special Defense of all friendly Pokémon with []{ability:plus} or []{ability:minus} by one stage.', 'Raises the Defense and Special Defense of all friendly Pokémon with []{ability:plus} or []{ability:minus} by one stage.'),
 (368, 'EN', 'For five turns, prevents all Pokémon on the ground from sleeping and strengthens their []{type:electric} moves to 1.5× their power.', 'For five turns, prevents all Pokémon on the ground from sleeping and strengthens their []{type:electric} moves to 1.5× their power.

Changes []{move:nature-power} to []{move:thunderbolt}.'),
 (369, 'EN', 'Lowers the target''s Special Attack by two stages.', 'Lowers the target''s Special Attack by two stages.'),
 (370, 'EN', 'Doubles prize money.', 'Doubles prize money. Stacks with a held item. Only works once per battle.');

-- Attack targets
CREATE TABLE IF NOT EXISTS `attack_targets` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Attack target ID (Primary key)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `attack_targets` (`id`) VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12), (13), (14);

CREATE TABLE IF NOT EXISTS `attack_targets_translation` (
  `attack_targets_id` int(2) unsigned NOT NULL COMMENT 'Attack target ID (Foreign)',
  `languages_id` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `name` varchar(255) NOT NULL COMMENT 'Name of the attack target',
  `description` text NOT NULL COMMENT 'Description of the attack target',
  CONSTRAINT FK_ATTACK_TARGETS FOREIGN KEY (attack_targets_id) REFERENCES attack_targets(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ATTACK_TARGETS_LANGUAGE FOREIGN KEY (languages_id) REFERENCES languages(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (languages_id, attack_targets_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `attack_targets_translation` (`attack_targets_id`, `languages_id`, `name`, `description`) VALUES
 (1, 'EN', 'Specific attack', 'One specific attack. How this attack is chosen depends upon on the attack being used.'),
 (1, 'FR', 'Attaque spécifique', 'Une attaque spécifique. Qui est affecté par l''ataque dépdent de l''ataque utilisée.'),
 (2, 'EN', 'Selected Pokémon by Me-First', 'One other Pokémon on the field, selected by the trainer (with Me-First). Stolen attack reuse the same target.'),
 (2, 'FR', 'Pokémon séléctionné par Moi d''abord', 'Un autre pokemon du champ de bataille, séléctionné par l''ataquant (avec Moi d''abord). Attaque volée qui est utilisée sur la même cible.'),
 (3, 'EN', 'Selected adjacent ally', 'Adjacent ally (if any) selected by the trainer.'),
 (3, 'FR', 'Allié adjacent sélectionné', 'Allié adjacent (si il y en a) sélectionné par le l''attaquant.'),
 (4, 'EN', 'User''s field', 'The user''s side of the field. Affects the user and its ally (if any).'),
 (4, 'FR', 'Champ de bataille de l''utilisateur', 'Champ de bataille de l''utilisateur. Affecte l''utilisateur et ses alliés (si il y en a).'),
 (5, 'EN', 'Selected user or ally', 'Either the user or one ally, selected by the trainer.'),
 (5, 'FR', 'Utilisateur ou allié sélectionné', 'Soit l''utilisateur ou un allié, sélectionné par le l''attaquant.'),
 (6, 'EN', 'Opponent''s field', 'The opposing side of the field. Affects all opposing Pokémon.'),
 (6, 'FR', 'Champ de bataille des ennemis', 'Le côté opposé du champ de bataille. Affecte tous les Pokémon adverses.'),
 (7, 'EN', 'User', 'The user.'),
 (7, 'FR', 'Utilisateur', 'L''utilisateur.'),
 (8, 'EN', 'Random adjacent enemy', 'One enemy, selected at random.'),
 (8, 'FR', 'Ennemi adjacent aléatoire', 'Un ennemi, sélectionné au hasard.'),
 (9, 'EN', 'All adjacent Pokémon', 'All adjacent Pokémon.'),
 (9, 'FR', 'Tous les Pokémon adjacents', 'Tous les Pokémon adjacents.'),
 (10, 'EN', 'Selected adjacent Pokémon', 'One adjacent Pokémon, selected by the trainer.'),
 (10, 'FR', 'Pokémon adjacent sélectionné par l''attaquant', 'Un Pokémon adjacent, sélectionné par l''attaquant.'),
 (11, 'EN', 'All adjacent enemies', 'All adjacent enemies.'),
 (11, 'FR', 'Tous les ennemis adjacents', 'Tous les ennemis adjacents.'),
 (12, 'EN', 'Entire field', 'The entire field. Affects all Pokémon.'),
 (12, 'FR', 'Champ de bataille entier', 'Champ de bataille entier. Affecte tous les Pokémon.'),
 (13, 'EN', 'User and allies', 'The user and its allies.'),
 (13, 'FR', 'Utilisateur et alliés', 'L''utilisateur et ses alliés.'),
 (14, 'EN', 'All Pokémon (including user)', 'Every Pokémon on the field (including user).'),
 (14, 'FR', 'Tous les Pokémon (y compris l''utilisateur)', 'Tous les Pokémon sur le champ de bataille (y compris l''utilisateur).');

-- Attacks
CREATE TABLE IF NOT EXISTS `attacks` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Attack ID (Primary key)',
  `type_id` int(2) unsigned NOT NULL COMMENT 'Attack type ID (Foreign)',
  `class_id` int(2) unsigned NOT NULL COMMENT 'Attack class ID (Foreign)',
  `target_id` int(2) unsigned NOT NULL COMMENT 'Target ID (Foreign)',
  `effect_id` int(3) unsigned COMMENT 'Attack effect ID (Foreign)',
  `power` int(3) unsigned COMMENT 'Power of the attack',
  `pp` int(2) unsigned COMMENT 'Max launched attacks by fight',
  `priority` int(2) NOT NULL COMMENT 'Priority of the attack',
  `effect_chance` int(3) unsigned COMMENT 'Attack effect chance to hit (percentage)',
  `accuracy` int(3) unsigned COMMENT 'Precision in fight (percentage)',
  CONSTRAINT FK_TYPE_FOR_ATTACKS FOREIGN KEY (type_id) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_CLASS_FOR_ATTACKS FOREIGN KEY (class_id) REFERENCES attack_classes(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_TARGET_FOR_ATTACKS FOREIGN KEY (target_id) REFERENCES attack_targets(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_EFFECT_FOR_ATTACKS FOREIGN KEY (effect_id) REFERENCES attack_effects(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `attacks` (`id`, `type_id`, `power`, `pp`, `accuracy`, `priority`, `target_id`, `class_id`, `effect_id`, `effect_chance`) VALUES
  (1, 1, 40, 35, 100, 0, 10, 2, 1, NULL),
  (2, 2, 50, 25, 100, 0, 10, 2, 44, NULL),
  (3, 1, 15, 10, 85, 0, 10, 2, 30, NULL),
  (4, 1, 18, 15, 85, 0, 10, 2, 30, NULL),
  (5, 1, 80, 20, 85, 0, 10, 2, 1, NULL),
  (6, 1, 40, 20, 100, 0, 10, 2, 35, NULL),
  (7, 10, 75, 15, 100, 0, 10, 2, 5, 10),
  (8, 15, 75, 15, 100, 0, 10, 2, 6, 10),
  (9, 13, 75, 15, 100, 0, 10, 2, 7, 10),
  (10, 1, 40, 35, 100, 0, 10, 2, 1, NULL),
  (11, 1, 55, 30, 100, 0, 10, 2, 1, NULL),
  (12, 1, NULL, 5, 30, 0, 10, 2, 39, NULL),
  (13, 1, 80, 10, 100, 0, 11, 3, 40, NULL),
  (14, 1, NULL, 20, NULL, 0, 7, 1, 51, NULL),
  (15, 1, 50, 30, 95, 0, 10, 2, 1, NULL),
  (16, 3, 40, 35, 100, 0, 10, 3, 150, NULL),
  (17, 3, 60, 35, 100, 0, 10, 2, 1, NULL),
  (18, 1, NULL, 20, NULL, -6, 10, 1, 29, NULL),
  (19, 3, 90, 15, 95, 0, 10, 2, 156, NULL),
  (20, 1, 15, 20, 85, 0, 10, 2, 43, 100),
  (21, 1, 80, 20, 75, 0, 10, 2, 1, NULL),
  (22, 12, 45, 25, 100, 0, 10, 2, 1, NULL),
  (23, 1, 65, 20, 100, 0, 10, 2, 151, 30),
  (24, 2, 30, 30, 100, 0, 10, 2, 45, NULL),
  (25, 1, 120, 5, 75, 0, 10, 2, 1, NULL),
  (26, 2, 100, 10, 95, 0, 10, 2, 46, NULL),
  (27, 2, 60, 15, 85, 0, 10, 2, 32, 30),
  (28, 5, NULL, 15, 100, 0, 10, 1, 24, NULL),
  (29, 1, 70, 15, 100, 0, 10, 2, 32, 30),
  (30, 1, 65, 25, 100, 0, 10, 2, 1, NULL),
  (31, 1, 15, 20, 85, 0, 10, 2, 30, NULL),
  (32, 1, NULL, 5, 30, 0, 10, 2, 39, NULL),
  (33, 1, 50, 35, 100, 0, 10, 2, 1, NULL),
  (34, 1, 85, 15, 100, 0, 10, 2, 7, 30),
  (35, 1, 15, 20, 90, 0, 10, 2, 43, 100),
  (36, 1, 90, 20, 85, 0, 10, 2, 49, NULL),
  (37, 1, 120, 10, 100, 0, 8, 2, 28, NULL),
  (38, 1, 120, 15, 100, 0, 10, 2, 199, NULL),
  (39, 1, NULL, 30, 100, 0, 11, 1, 20, NULL),
  (40, 4, 15, 35, 100, 0, 10, 2, 3, 30),
  (41, 7, 25, 20, 100, 0, 10, 2, 78, 20),
  (42, 7, 25, 20, 95, 0, 10, 2, 30, NULL),
  (43, 1, NULL, 30, 100, 0, 11, 1, 20, 100),
  (44, 17, 60, 25, 100, 0, 10, 2, 32, 30),
  (45, 1, NULL, 40, 100, 0, 11, 1, 19, NULL),
  (46, 1, NULL, 20, NULL, -6, 10, 1, 29, NULL),
  (47, 1, NULL, 15, 55, 0, 10, 1, 2, NULL),
  (48, 1, NULL, 20, 55, 0, 10, 1, 50, NULL),
  (49, 1, NULL, 20, 90, 0, 10, 3, 131, NULL),
  (50, 1, NULL, 20, 100, 0, 10, 1, 87, NULL),
  (51, 4, 40, 30, 100, 0, 11, 3, 73, 10),
  (52, 10, 40, 25, 100, 0, 10, 3, 5, 10),
  (53, 10, 90, 15, 100, 0, 10, 3, 5, 10),
  (54, 15, NULL, 30, NULL, 0, 4, 1, 47, NULL),
  (55, 11, 40, 25, 100, 0, 10, 3, 1, NULL),
  (56, 11, 110, 5, 80, 0, 10, 3, 1, NULL),
  (57, 11, 90, 15, 100, 0, 9, 3, 258, NULL),
  (58, 15, 90, 10, 100, 0, 10, 3, 6, 10),
  (59, 15, 110, 5, 70, 0, 11, 3, 261, 10),
  (60, 14, 65, 20, 100, 0, 10, 3, 77, 10),
  (61, 11, 65, 20, 100, 0, 10, 3, 71, 10),
  (62, 15, 65, 20, 100, 0, 10, 3, 69, 10),
  (63, 1, 150, 5, 90, 0, 10, 3, 81, NULL),
  (64, 3, 35, 35, 100, 0, 10, 2, 1, NULL),
  (65, 3, 80, 20, 100, 0, 10, 2, 1, NULL),
  (66, 2, 80, 20, 80, 0, 10, 2, 49, NULL),
  (67, 2, NULL, 20, 100, 0, 10, 2, 197, NULL),
  (68, 2, NULL, 20, 100, -5, 1, 2, 90, NULL),
  (69, 2, NULL, 20, 100, 0, 10, 2, 88, NULL),
  (70, 1, 80, 15, 100, 0, 10, 2, 1, NULL),
  (71, 12, 20, 25, 100, 0, 10, 3, 4, NULL),
  (72, 12, 40, 15, 100, 0, 10, 3, 4, NULL),
  (73, 12, NULL, 10, 90, 0, 10, 1, 85, NULL),
  (74, 1, NULL, 20, NULL, 0, 7, 1, 317, NULL),
  (75, 12, 55, 25, 95, 0, 11, 2, 44, NULL),
  (76, 12, 120, 10, 100, 0, 10, 3, 152, NULL),
  (77, 4, NULL, 35, 75, 0, 10, 1, 67, NULL),
  (78, 12, NULL, 30, 75, 0, 10, 1, 68, NULL),
  (79, 12, NULL, 15, 75, 0, 10, 1, 2, NULL),
  (80, 12, 120, 10, 100, 0, 8, 3, 28, NULL),
  (81, 7, NULL, 40, 95, 0, 11, 1, 21, NULL),
  (82, 16, NULL, 10, 100, 0, 10, 3, 42, NULL),
  (83, 10, 35, 15, 85, 0, 10, 3, 43, 100),
  (84, 13, 40, 30, 100, 0, 10, 3, 7, 10),
  (85, 13, 90, 15, 100, 0, 10, 3, 7, 10),
  (86, 13, NULL, 20, 100, 0, 10, 1, 68, NULL),
  (87, 13, 110, 10, 70, 0, 10, 3, 153, 30),
  (88, 6, 50, 15, 90, 0, 10, 2, 1, NULL),
  (89, 5, 100, 10, 100, 0, 9, 2, 148, NULL),
  (90, 5, NULL, 5, 30, 0, 10, 2, 39, NULL),
  (91, 5, 80, 10, 100, 0, 10, 2, 257, NULL),
  (92, 4, NULL, 10, 90, 0, 10, 1, 34, NULL),
  (93, 14, 50, 25, 100, 0, 10, 3, 77, 10),
  (94, 14, 90, 10, 100, 0, 10, 3, 73, 10),
  (95, 14, NULL, 20, 60, 0, 10, 1, 2, NULL),
  (96, 14, NULL, 40, NULL, 0, 7, 1, 11, NULL),
  (97, 14, NULL, 30, NULL, 0, 7, 1, 53, NULL),
  (98, 1, 40, 30, 100, 1, 10, 2, 104, NULL),
  (99, 1, 20, 20, 100, 0, 10, 2, 82, NULL),
  (100, 14, NULL, 20, NULL, 0, 7, 1, 154, NULL),
  (101, 8, NULL, 15, 100, 0, 10, 3, 88, NULL),
  (102, 1, NULL, 10, NULL, 0, 10, 1, 83, NULL),
  (103, 1, NULL, 40, 85, 0, 10, 1, 60, NULL),
  (104, 1, NULL, 15, NULL, 0, 7, 1, 17, NULL),
  (105, 1, NULL, 10, NULL, 0, 7, 1, 33, NULL),
  (106, 1, NULL, 30, NULL, 0, 7, 1, 12, NULL),
  (107, 1, NULL, 10, NULL, 0, 7, 1, 109, NULL),
  (108, 1, NULL, 20, 100, 0, 10, 1, 24, NULL),
  (109, 8, NULL, 10, 100, 0, 10, 1, 50, NULL),
  (110, 11, NULL, 40, NULL, 0, 7, 1, 12, NULL),
  (111, 1, NULL, 40, NULL, 0, 7, 1, 157, NULL),
  (112, 14, NULL, 20, NULL, 0, 7, 1, 52, NULL),
  (113, 14, NULL, 30, NULL, 0, 4, 1, 36, NULL),
  (114, 15, NULL, 30, NULL, 0, 12, 1, 26, NULL),
  (115, 14, NULL, 20, NULL, 0, 4, 1, 66, NULL),
  (116, 1, NULL, 30, NULL, 0, 7, 1, 48, NULL),
  (117, 1, NULL, 10, NULL, 1, 7, 2, 27, NULL),
  (118, 1, NULL, 10, NULL, 0, 7, 1, 84, NULL),
  (119, 3, NULL, 20, NULL, 0, 10, 1, 10, NULL),
  (120, 1, 200, 5, 100, 0, 9, 2, 8, NULL),
  (121, 1, 100, 10, 75, 0, 10, 2, 1, NULL),
  (122, 8, 30, 30, 100, 0, 10, 2, 7, 30),
  (123, 4, 30, 20, 70, 0, 10, 3, 3, 40),
  (124, 4, 65, 20, 100, 0, 10, 3, 3, 30),
  (125, 5, 65, 20, 85, 0, 10, 2, 32, 10),
  (126, 10, 110, 5, 85, 0, 10, 3, 5, 10),
  (127, 11, 80, 15, 100, 0, 10, 2, 32, 20),
  (128, 11, 35, 15, 85, 0, 10, 2, 43, 100),
  (129, 1, 60, 20, NULL, 0, 11, 3, 18, NULL),
  (130, 1, 130, 10, 100, 0, 10, 2, 146, 100),
  (131, 1, 20, 15, 100, 0, 10, 2, 30, NULL),
  (132, 1, 10, 35, 100, 0, 10, 2, 71, 10),
  (133, 14, NULL, 20, NULL, 0, 7, 1, 55, NULL),
  (134, 14, NULL, 15, 80, 0, 10, 1, 24, NULL),
  (135, 1, NULL, 10, NULL, 0, 7, 1, 33, NULL),
  (136, 2, 130, 10, 90, 0, 10, 2, 46, NULL),
  (137, 1, NULL, 30, 100, 0, 10, 1, 68, NULL),
  (138, 14, 100, 15, 100, 0, 10, 3, 9, NULL),
  (139, 4, NULL, 40, 90, 0, 11, 1, 67, NULL),
  (140, 1, 15, 20, 85, 0, 10, 2, 30, NULL),
  (141, 7, 20, 15, 100, 0, 10, 2, 4, NULL),
  (142, 1, NULL, 10, 75, 0, 10, 1, 2, NULL),
  (143, 3, 140, 5, 90, 0, 10, 2, 76, 30),
  (144, 1, NULL, 10, NULL, 0, 10, 1, 58, NULL),
  (145, 11, 40, 30, 100, 0, 11, 3, 71, 10),
  (146, 1, 70, 10, 100, 0, 10, 2, 77, 20),
  (147, 12, NULL, 15, 100, 0, 10, 1, 2, NULL),
  (148, 1, NULL, 20, 100, 0, 10, 1, 24, NULL),
  (149, 14, NULL, 15, 100, 0, 10, 3, 89, NULL),
  (150, 1, NULL, 40, NULL, 0, 7, 1, 86, NULL),
  (151, 4, NULL, 20, NULL, 0, 7, 1, 52, NULL),
  (152, 11, 100, 10, 90, 0, 10, 2, 44, NULL),
  (153, 1, 250, 5, 100, 0, 9, 2, 8, NULL),
  (154, 1, 18, 15, 80, 0, 10, 2, 30, NULL),
  (155, 5, 50, 10, 90, 0, 10, 2, 45, NULL),
  (156, 14, NULL, 10, NULL, 0, 7, 1, 38, NULL),
  (157, 6, 75, 10, 90, 0, 11, 2, 32, 30),
  (158, 1, 80, 15, 90, 0, 10, 2, 32, 10),
  (159, 1, NULL, 30, NULL, 0, 7, 1, 11, NULL),
  (160, 1, NULL, 30, NULL, 0, 7, 1, 31, NULL),
  (161, 1, 80, 10, 100, 0, 10, 3, 37, 20),
  (162, 1, NULL, 10, 90, 0, 10, 2, 41, NULL),
  (163, 1, 70, 20, 100, 0, 10, 2, 44, NULL),
  (164, 1, NULL, 10, NULL, 0, 7, 1, 80, NULL),
  (165, 1, 50, NULL, NULL, 0, 8, 2, 255, NULL),
  (166, 1, NULL, 1, NULL, 0, 10, 1, 96, NULL),
  (167, 2, 10, 10, 90, 0, 10, 2, 105, NULL),
  (168, 17, 60, 25, 100, 0, 10, 2, 106, NULL),
  (169, 7, NULL, 10, NULL, 0, 10, 1, 107, NULL),
  (170, 1, NULL, 5, NULL, 0, 10, 1, 95, NULL),
  (171, 8, NULL, 15, 100, 0, 10, 1, 108, NULL),
  (172, 10, 60, 25, 100, 0, 10, 2, 126, 10),
  (173, 1, 50, 15, 100, 0, 10, 3, 93, 30),
  (174, 8, NULL, 10, NULL, 0, 1, 1, 110, NULL),
  (175, 1, NULL, 15, 100, 0, 10, 2, 100, NULL),
  (176, 1, NULL, 30, NULL, 0, 10, 1, 94, NULL),
  (177, 3, 100, 5, 95, 0, 10, 3, 44, NULL),
  (178, 12, NULL, 40, 100, 0, 11, 1, 61, NULL),
  (179, 2, NULL, 15, 100, 0, 10, 2, 100, NULL),
  (180, 8, NULL, 10, 100, 0, 10, 1, 101, NULL),
  (181, 15, 40, 25, 100, 0, 11, 3, 6, 10),
  (182, 1, NULL, 10, NULL, 4, 7, 1, 112, NULL),
  (183, 2, 40, 30, 100, 1, 10, 2, 104, NULL),
  (184, 1, NULL, 10, 100, 0, 10, 1, 61, NULL),
  (185, 17, 60, 20, NULL, 0, 10, 2, 18, NULL),
  (186, 18, NULL, 10, 75, 0, 10, 1, 50, NULL),
  (187, 1, NULL, 10, NULL, 0, 7, 1, 143, NULL),
  (188, 4, 90, 10, 100, 0, 10, 3, 3, 30),
  (189, 5, 20, 10, 100, 0, 10, 3, 74, 100),
  (190, 11, 65, 10, 85, 0, 10, 3, 74, 50),
  (191, 5, NULL, 20, NULL, 0, 6, 1, 113, NULL),
  (192, 13, 120, 5, 50, 0, 10, 3, 7, 100),
  (193, 1, NULL, 40, NULL, 0, 10, 1, 114, NULL),
  (194, 8, NULL, 5, NULL, 0, 7, 1, 99, NULL),
  (195, 1, NULL, 5, NULL, 0, 14, 1, 115, NULL),
  (196, 15, 55, 15, 95, 0, 11, 3, 71, 100),
  (197, 2, NULL, 5, NULL, 4, 7, 1, 112, NULL),
  (198, 5, 25, 10, 90, 0, 10, 2, 30, NULL),
  (199, 1, NULL, 5, NULL, 0, 10, 1, 95, NULL),
  (200, 16, 120, 10, 100, 0, 8, 2, 28, NULL),
  (201, 6, NULL, 10, NULL, 0, 12, 1, 116, NULL),
  (202, 12, 75, 10, 100, 0, 10, 3, 4, NULL),
  (203, 1, NULL, 10, NULL, 4, 7, 1, 117, NULL),
  (204, 18, NULL, 20, 100, 0, 10, 1, 59, NULL),
  (205, 6, 30, 20, 90, 0, 10, 2, 118, NULL),
  (206, 1, 40, 40, 100, 0, 10, 2, 102, NULL),
  (207, 1, NULL, 15, 90, 0, 10, 1, 119, NULL),
  (208, 1, NULL, 10, NULL, 0, 7, 1, 33, NULL),
  (209, 13, 65, 20, 100, 0, 10, 2, 7, 30),
  (210, 7, 40, 20, 95, 0, 10, 2, 120, NULL),
  (211, 9, 70, 25, 90, 0, 10, 2, 139, 10),
  (212, 1, NULL, 5, NULL, 0, 10, 1, 107, NULL),
  (213, 1, NULL, 15, 100, 0, 10, 1, 121, NULL),
  (214, 1, NULL, 10, NULL, 0, 7, 1, 98, NULL),
  (215, 1, NULL, 5, NULL, 0, 13, 1, 103, NULL),
  (216, 1, NULL, 20, 100, 0, 10, 2, 122, NULL),
  (217, 1, NULL, 15, 90, 0, 10, 2, 123, NULL),
  (218, 1, NULL, 20, 100, 0, 10, 2, 124, NULL),
  (219, 1, NULL, 25, NULL, 0, 4, 1, 125, NULL),
  (220, 1, NULL, 20, NULL, 0, 10, 1, 92, NULL),
  (221, 10, 100, 5, 95, 0, 10, 2, 126, 50),
  (222, 5, NULL, 30, 100, 0, 9, 2, 127, NULL),
  (223, 2, 100, 5, 50, 0, 10, 2, 77, 100),
  (224, 7, 120, 10, 85, 0, 10, 2, 1, NULL),
  (225, 16, 60, 20, 100, 0, 10, 3, 7, 30),
  (226, 1, NULL, 40, NULL, 0, 7, 1, 128, NULL),
  (227, 1, NULL, 5, 100, 0, 10, 1, 91, NULL),
  (228, 17, 40, 20, 100, 0, 10, 2, 129, NULL),
  (229, 1, 20, 40, 100, 0, 10, 2, 130, NULL),
  (230, 1, NULL, 20, 100, 0, 11, 1, 25, NULL),
  (231, 9, 100, 15, 75, 0, 10, 2, 70, 30),
  (232, 9, 50, 35, 95, 0, 10, 2, 140, 10),
  (233, 2, 70, 10, NULL, -1, 10, 2, 79, NULL),
  (234, 1, NULL, 5, NULL, 0, 7, 1, 133, NULL),
  (235, 12, NULL, 5, NULL, 0, 7, 1, 133, NULL),
  (236, 18, NULL, 5, NULL, 0, 7, 1, 133, NULL),
  (237, 1, 60, 15, 100, 0, 10, 3, 136, NULL),
  (238, 2, 100, 5, 80, 0, 10, 2, 44, NULL),
  (239, 16, 40, 20, 100, 0, 11, 3, 147, 20),
  (240, 11, NULL, 5, NULL, 0, 12, 1, 137, NULL),
  (241, 10, NULL, 5, NULL, 0, 12, 1, 138, NULL),
  (242, 17, 80, 15, 100, 0, 10, 2, 70, 20),
  (243, 14, NULL, 20, 100, -5, 1, 3, 145, NULL),
  (244, 1, NULL, 10, NULL, 0, 10, 1, 144, NULL),
  (245, 1, 80, 5, 100, 2, 10, 2, 104, NULL),
  (246, 6, 60, 5, 100, 0, 10, 3, 141, 10),
  (247, 8, 80, 15, 100, 0, 10, 3, 73, 20),
  (248, 14, 120, 10, 100, 0, 10, 3, 149, NULL),
  (249, 2, 40, 15, 100, 0, 10, 2, 70, 50),
  (250, 11, 35, 15, 85, 0, 10, 3, 262, 100),
  (251, 17, NULL, 10, 100, 0, 10, 2, 155, NULL),
  (252, 1, 40, 10, 100, 3, 10, 2, 159, 100),
  (253, 1, 90, 10, 100, 0, 8, 3, 160, NULL),
  (254, 1, NULL, 20, NULL, 0, 7, 1, 161, NULL),
  (255, 1, NULL, 10, 100, 0, 10, 3, 162, NULL),
  (256, 1, NULL, 10, NULL, 0, 7, 1, 163, NULL),
  (257, 10, 95, 10, 90, 0, 11, 3, 5, 10),
  (258, 15, NULL, 10, NULL, 0, 12, 1, 165, NULL),
  (259, 17, NULL, 15, 100, 0, 10, 1, 166, NULL),
  (260, 17, NULL, 15, 100, 0, 10, 1, 167, NULL),
  (261, 10, NULL, 15, 85, 0, 10, 1, 168, NULL),
  (262, 17, NULL, 10, 100, 0, 10, 1, 169, NULL),
  (263, 1, 70, 20, 100, 0, 10, 2, 170, NULL),
  (264, 2, 150, 20, 100, -3, 10, 2, 171, NULL),
  (265, 1, 70, 10, 100, 0, 10, 2, 172, NULL),
  (266, 1, NULL, 20, NULL, 2, 7, 1, 173, NULL),
  (267, 1, NULL, 20, NULL, 0, 10, 1, 174, NULL),
  (268, 13, NULL, 20, NULL, 0, 7, 1, 175, NULL),
  (269, 17, NULL, 20, 100, 0, 10, 1, 176, NULL),
  (270, 1, NULL, 20, NULL, 5, 3, 1, 177, NULL),
  (271, 14, NULL, 10, 100, 0, 10, 1, 178, NULL),
  (272, 14, NULL, 10, NULL, 0, 10, 1, 179, NULL),
  (273, 1, NULL, 10, NULL, 0, 7, 1, 180, NULL),
  (274, 1, NULL, 20, NULL, 0, 7, 1, 181, NULL),
  (275, 12, NULL, 20, NULL, 0, 7, 1, 182, NULL),
  (276, 2, 120, 5, 100, 0, 10, 2, 183, 100),
  (277, 14, NULL, 15, NULL, 4, 7, 1, 184, NULL),
  (278, 1, NULL, 10, NULL, 0, 7, 1, 185, NULL),
  (279, 2, 60, 10, 100, -4, 10, 2, 186, NULL),
  (280, 2, 75, 15, 100, 0, 10, 2, 187, NULL),
  (281, 1, NULL, 10, NULL, 0, 10, 1, 188, NULL),
  (282, 17, 65, 20, 100, 0, 10, 2, 189, NULL),
  (283, 1, NULL, 5, 100, 0, 10, 2, 190, NULL),
  (284, 10, 150, 5, 100, 0, 11, 3, 191, NULL),
  (285, 14, NULL, 10, NULL, 0, 10, 1, 192, NULL),
  (286, 14, NULL, 10, NULL, 0, 7, 1, 193, NULL),
  (287, 1, NULL, 20, NULL, 0, 7, 1, 194, NULL),
  (288, 8, NULL, 5, NULL, 0, 7, 1, 195, NULL),
  (289, 17, NULL, 10, NULL, 4, 7, 1, 196, NULL),
  (290, 1, 70, 20, 100, 0, 10, 2, 198, 30),
  (291, 11, 80, 10, 100, 0, 10, 2, 256, NULL),
  (292, 2, 15, 20, 100, 0, 10, 2, 30, NULL),
  (293, 1, NULL, 20, NULL, 0, 7, 1, 214, NULL),
  (294, 7, NULL, 20, NULL, 0, 7, 1, 322, NULL),
  (295, 14, 70, 5, 100, 0, 10, 3, 73, 50),
  (296, 14, 70, 5, 100, 0, 10, 3, 72, 50),
  (297, 3, NULL, 15, 100, 0, 10, 1, 59, NULL),
  (298, 1, NULL, 20, 100, 0, 9, 1, 200, NULL),
  (299, 10, 85, 10, 90, 0, 10, 2, 201, 10),
  (300, 5, NULL, 15, NULL, 0, 12, 1, 202, NULL),
  (301, 15, 30, 20, 90, 0, 10, 2, 118, NULL),
  (302, 12, 60, 15, 100, 0, 10, 2, 32, 30),
  (303, 1, NULL, 10, NULL, 0, 7, 1, 33, NULL),
  (304, 1, 90, 10, 100, 0, 11, 3, 1, NULL),
  (305, 4, 50, 15, 100, 0, 10, 2, 203, 50),
  (306, 1, 75, 10, 95, 0, 10, 2, 70, 50),
  (307, 10, 150, 5, 90, 0, 10, 3, 81, NULL),
  (308, 11, 150, 5, 90, 0, 10, 3, 81, NULL),
  (309, 9, 90, 10, 90, 0, 10, 2, 140, 20),
  (310, 8, 30, 15, 100, 0, 10, 2, 32, 30),
  (311, 1, 50, 10, 100, 0, 10, 3, 204, NULL),
  (312, 12, NULL, 5, NULL, 0, 13, 1, 103, NULL),
  (313, 17, NULL, 20, 100, 0, 10, 1, 63, NULL),
  (314, 3, 60, 25, 95, 0, 11, 3, 44, NULL),
  (315, 10, 130, 5, 90, 0, 10, 3, 205, 100),
  (316, 1, NULL, 40, NULL, 0, 10, 1, 114, NULL),
  (317, 6, 60, 15, 95, 0, 10, 2, 71, 100),
  (318, 7, 60, 5, 100, 0, 10, 3, 141, 10),
  (319, 9, NULL, 40, 85, 0, 10, 1, 63, NULL),
  (320, 12, NULL, 15, 55, 0, 10, 1, 2, NULL),
  (321, 1, NULL, 20, 100, 0, 10, 1, 206, NULL),
  (322, 14, NULL, 20, NULL, 0, 7, 1, 207, NULL),
  (323, 11, 150, 5, 100, 0, 11, 3, 191, NULL),
  (324, 7, 75, 15, 100, 0, 10, 3, 77, 10),
  (325, 8, 60, 20, NULL, 0, 10, 2, 18, NULL),
  (326, 14, 80, 20, 100, 0, 10, 3, 32, 10),
  (327, 2, 85, 15, 90, 0, 10, 2, 208, NULL),
  (328, 5, 35, 15, 85, 0, 10, 2, 43, 100),
  (329, 15, NULL, 5, 30, 0, 10, 3, 39, NULL),
  (330, 11, 90, 10, 85, 0, 11, 3, 74, 30),
  (331, 12, 25, 30, 100, 0, 10, 2, 30, NULL),
  (332, 3, 60, 20, NULL, 0, 10, 2, 18, NULL),
  (333, 15, 25, 30, 100, 0, 10, 2, 30, NULL),
  (334, 9, NULL, 15, NULL, 0, 7, 1, 52, NULL),
  (335, 1, NULL, 5, NULL, 0, 10, 1, 107, NULL),
  (336, 1, NULL, 40, NULL, 0, 7, 1, 11, NULL),
  (337, 16, 80, 15, 100, 0, 10, 2, 1, NULL),
  (338, 12, 150, 5, 90, 0, 10, 3, 81, NULL),
  (339, 2, NULL, 20, NULL, 0, 7, 1, 209, NULL),
  (340, 3, 85, 5, 85, 0, 10, 2, 264, 30),
  (341, 5, 55, 15, 95, 0, 10, 3, 71, 100),
  (342, 4, 50, 25, 100, 0, 10, 2, 210, 10),
  (343, 1, 60, 25, 100, 0, 10, 2, 106, NULL),
  (344, 13, 120, 15, 100, 0, 10, 2, 263, 10),
  (345, 12, 60, 20, NULL, 0, 10, 3, 18, NULL),
  (346, 11, NULL, 15, NULL, 0, 12, 1, 211, NULL),
  (347, 14, NULL, 20, NULL, 0, 7, 1, 212, NULL),
  (348, 12, 90, 15, 100, 0, 10, 2, 44, NULL),
  (349, 16, NULL, 20, NULL, 0, 7, 1, 213, NULL),
  (350, 6, 25, 10, 90, 0, 10, 2, 30, NULL),
  (351, 13, 60, 20, NULL, 0, 10, 3, 18, NULL),
  (352, 11, 60, 20, 100, 0, 10, 3, 77, 20),
  (353, 9, 140, 5, 100, 0, 10, 3, 149, NULL),
  (354, 14, 140, 5, 90, 0, 10, 3, 205, 100),
  (355, 3, NULL, 10, NULL, 0, 7, 1, 215, NULL),
  (356, 14, NULL, 5, NULL, 0, 12, 1, 216, NULL),
  (357, 14, NULL, 40, NULL, 0, 10, 1, 217, NULL),
  (358, 2, 70, 10, 100, 0, 10, 2, 218, NULL),
  (359, 2, 100, 10, 90, 0, 10, 2, 219, 100),
  (360, 9, NULL, 5, 100, 0, 10, 2, 220, NULL),
  (361, 14, NULL, 10, NULL, 0, 7, 1, 221, NULL),
  (362, 11, 65, 10, 100, 0, 10, 3, 222, NULL),
  (363, 1, NULL, 15, 100, 0, 10, 2, 223, NULL),
  (364, 1, 30, 10, 100, 2, 10, 2, 224, NULL),
  (365, 3, 60, 20, 100, 0, 10, 2, 225, NULL),
  (366, 3, NULL, 15, NULL, 0, 4, 1, 226, NULL),
  (367, 1, NULL, 30, NULL, 0, 5, 1, 227, NULL),
  (368, 9, NULL, 10, 100, 0, 1, 2, 228, NULL),
  (369, 7, 70, 20, 100, 0, 10, 2, 229, NULL),
  (370, 2, 120, 5, 100, 0, 10, 2, 230, 100),
  (371, 17, 50, 10, 100, 0, 10, 2, 231, NULL),
  (372, 17, 60, 10, 100, 0, 10, 2, 232, NULL),
  (373, 17, NULL, 15, 100, 0, 10, 1, 233, NULL),
  (374, 17, NULL, 10, 100, 0, 10, 2, 234, NULL),
  (375, 14, NULL, 10, 100, 0, 10, 1, 235, NULL),
  (376, 1, NULL, 5, NULL, 0, 10, 3, 236, NULL),
  (377, 14, NULL, 15, 100, 0, 11, 1, 237, NULL),
  (378, 1, NULL, 5, 100, 0, 10, 3, 238, NULL),
  (379, 14, NULL, 10, NULL, 0, 7, 1, 239, NULL),
  (380, 4, NULL, 10, 100, 0, 10, 1, 240, NULL),
  (381, 1, NULL, 30, NULL, 0, 4, 1, 241, NULL),
  (382, 1, NULL, 20, NULL, 0, 2, 1, 242, NULL),
  (383, 1, NULL, 20, NULL, 0, 7, 1, 243, NULL),
  (384, 14, NULL, 10, NULL, 0, 10, 1, 244, NULL),
  (385, 14, NULL, 10, NULL, 0, 10, 1, 245, NULL),
  (386, 17, NULL, 5, 100, 0, 10, 2, 246, NULL),
  (387, 1, 140, 5, 100, 0, 10, 2, 247, NULL),
  (388, 12, NULL, 10, 100, 0, 10, 1, 248, NULL),
  (389, 17, 80, 5, 100, 1, 10, 2, 249, NULL),
  (390, 4, NULL, 20, NULL, 0, 6, 1, 250, NULL),
  (391, 14, NULL, 10, NULL, 0, 10, 1, 251, NULL),
  (392, 11, NULL, 20, NULL, 0, 7, 1, 252, NULL),
  (393, 13, NULL, 10, NULL, 0, 7, 1, 253, NULL),
  (394, 10, 120, 15, 100, 0, 10, 2, 254, 10),
  (395, 2, 60, 10, 100, 0, 10, 2, 7, 30),
  (396, 2, 80, 20, NULL, 0, 10, 3, 18, NULL),
  (397, 6, NULL, 20, NULL, 0, 7, 1, 53, NULL),
  (398, 4, 80, 20, 100, 0, 10, 2, 3, 30),
  (399, 17, 80, 15, 100, 0, 10, 3, 32, 20),
  (400, 17, 70, 15, 100, 0, 10, 2, 44, NULL),
  (401, 11, 90, 10, 90, 0, 10, 2, 1, NULL),
  (402, 12, 80, 15, 100, 0, 10, 2, 1, NULL),
  (403, 3, 75, 15, 95, 0, 10, 3, 32, 30),
  (404, 7, 80, 15, 100, 0, 10, 2, 1, NULL),
  (405, 7, 90, 10, 100, 0, 10, 3, 73, 10),
  (406, 16, 85, 10, 100, 0, 10, 3, 1, NULL),
  (407, 16, 100, 10, 75, 0, 10, 2, 32, 20),
  (408, 6, 80, 20, 100, 0, 10, 3, 1, NULL),
  (409, 2, 75, 10, 100, 0, 10, 2, 4, NULL),
  (410, 2, 40, 30, 100, 1, 10, 3, 104, NULL),
  (411, 2, 120, 5, 70, 0, 10, 3, 73, 10),
  (412, 12, 90, 10, 100, 0, 10, 3, 73, 10),
  (413, 3, 120, 15, 100, 0, 10, 2, 199, NULL),
  (414, 5, 90, 10, 100, 0, 10, 3, 73, 10),
  (415, 17, NULL, 10, 100, 0, 10, 1, 178, NULL),
  (416, 1, 150, 5, 90, 0, 10, 2, 81, NULL),
  (417, 17, NULL, 20, NULL, 0, 7, 1, 54, NULL),
  (418, 9, 40, 30, 100, 1, 10, 2, 104, NULL),
  (419, 15, 60, 10, 100, -4, 10, 2, 186, NULL),
  (420, 15, 40, 30, 100, 1, 10, 2, 104, NULL),
  (421, 8, 70, 15, 100, 0, 10, 2, 44, NULL),
  (422, 13, 65, 15, 95, 0, 10, 2, 276, 10),
  (423, 15, 65, 15, 95, 0, 10, 2, 275, 10),
  (424, 10, 65, 15, 95, 0, 10, 2, 274, 10),
  (425, 8, 40, 30, 100, 1, 10, 2, 104, NULL),
  (426, 5, 65, 10, 85, 0, 10, 3, 74, 30),
  (427, 14, 70, 20, 100, 0, 10, 2, 44, NULL),
  (428, 14, 80, 15, 90, 0, 10, 2, 32, 20),
  (429, 9, 65, 10, 85, 0, 10, 3, 74, 30),
  (430, 9, 80, 10, 100, 0, 10, 3, 73, 10),
  (431, 1, 90, 20, 85, 0, 10, 2, 77, 20),
  (432, 3, NULL, 15, NULL, 0, 10, 1, 259, NULL),
  (433, 14, NULL, 5, NULL, -7, 12, 1, 260, NULL),
  (434, 16, 130, 5, 90, 0, 10, 3, 205, 100),
  (435, 13, 80, 15, 100, 0, 9, 3, 7, 30),
  (436, 10, 80, 15, 100, 0, 9, 3, 5, 30),
  (437, 12, 130, 5, 90, 0, 10, 3, 205, 100),
  (438, 12, 120, 10, 85, 0, 10, 2, 1, NULL),
  (439, 6, 150, 5, 90, 0, 10, 2, 81, NULL),
  (440, 4, 70, 20, 100, 0, 10, 2, 210, 10),
  (441, 4, 120, 5, 80, 0, 10, 2, 3, 30),
  (442, 9, 80, 15, 100, 0, 10, 2, 32, 30),
  (443, 9, 60, 20, NULL, 0, 10, 2, 18, NULL),
  (444, 6, 100, 5, 80, 0, 10, 2, 44, NULL),
  (445, 1, NULL, 20, 100, 0, 11, 1, 266, NULL),
  (446, 6, NULL, 20, NULL, 0, 6, 1, 267, NULL),
  (447, 12, NULL, 20, 100, 0, 10, 3, 197, NULL),
  (448, 3, 65, 20, 100, 0, 10, 3, 268, 100),
  (449, 1, 100, 10, 100, 0, 10, 3, 269, NULL),
  (450, 7, 60, 20, 100, 0, 10, 2, 225, NULL),
  (451, 13, 50, 10, 90, 0, 10, 3, 277, 70),
  (452, 12, 120, 15, 100, 0, 10, 2, 199, NULL),
  (453, 11, 40, 20, 100, 1, 10, 2, 104, NULL),
  (454, 7, 90, 15, 100, 0, 10, 2, 44, NULL),
  (455, 7, NULL, 10, NULL, 0, 7, 1, 207, NULL),
  (456, 7, NULL, 10, NULL, 0, 7, 1, 33, NULL),
  (457, 6, 150, 5, 80, 0, 10, 2, 270, NULL),
  (458, 1, 35, 10, 90, 0, 10, 2, 45, NULL),
  (459, 16, 150, 5, 90, 0, 10, 3, 81, NULL),
  (460, 16, 100, 5, 95, 0, 10, 3, 44, NULL),
  (461, 14, NULL, 10, NULL, 0, 7, 1, 271, NULL),
  (462, 1, NULL, 5, 100, 0, 10, 2, 238, NULL),
  (463, 10, 100, 5, 75, 0, 10, 3, 43, 100),
  (464, 17, NULL, 10, 80, 0, 11, 1, 2, NULL),
  (465, 12, 120, 5, 85, 0, 10, 3, 272, 40),
  (466, 8, 60, 5, 100, 0, 10, 3, 141, 10),
  (467, 8, 120, 5, 100, 0, 10, 2, 273, NULL),
  (468, 17, NULL, 15, NULL, 0, 7, 1, 278, NULL),
  (469, 6, NULL, 10, NULL, 3, 4, 1, 279, NULL),
  (470, 14, NULL, 10, NULL, 0, 10, 1, 280, NULL),
  (471, 14, NULL, 10, NULL, 0, 10, 1, 281, NULL),
  (472, 14, NULL, 10, NULL, 0, 12, 1, 282, NULL),
  (473, 14, 80, 10, 100, 0, 10, 3, 283, NULL),
  (474, 4, 65, 10, 100, 0, 10, 3, 284, NULL),
  (475, 9, NULL, 15, NULL, 0, 7, 1, 285, NULL),
  (476, 7, NULL, 20, NULL, 2, 7, 1, 173, NULL),
  (477, 14, NULL, 15, NULL, 0, 10, 1, 286, NULL),
  (478, 14, NULL, 10, NULL, 0, 12, 1, 287, NULL),
  (479, 6, 50, 15, 100, 0, 10, 2, 288, 100),
  (480, 2, 60, 10, 100, 0, 10, 2, 289, NULL),
  (481, 10, 70, 15, 100, 0, 10, 3, 290, NULL),
  (482, 4, 95, 10, 100, 0, 9, 3, 3, 10),
  (483, 7, NULL, 20, NULL, 0, 7, 1, 291, NULL),
  (484, 9, NULL, 10, 100, 0, 10, 2, 292, NULL),
  (485, 14, 120, 10, 100, 0, 9, 3, 293, NULL),
  (486, 13, NULL, 10, 100, 0, 10, 3, 294, NULL),
  (487, 11, NULL, 20, 100, 0, 10, 1, 295, NULL),
  (488, 10, 50, 20, 100, 0, 10, 2, 296, 100),
  (489, 4, NULL, 20, NULL, 0, 7, 1, 323, NULL),
  (490, 2, 65, 20, 100, 0, 10, 2, 21, 100),
  (491, 4, 40, 20, 100, 0, 10, 3, 297, 100),
  (492, 17, 95, 15, 100, 0, 10, 2, 298, NULL),
  (493, 1, NULL, 15, 100, 0, 10, 1, 299, NULL),
  (494, 1, NULL, 15, 100, 0, 10, 1, 300, NULL),
  (495, 1, NULL, 15, NULL, 0, 10, 1, 301, NULL),
  (496, 1, 60, 15, 100, 0, 10, 3, 302, NULL),
  (497, 1, 40, 15, 100, 0, 10, 3, 303, NULL),
  (498, 1, 70, 20, 100, 0, 10, 2, 304, NULL),
  (499, 4, 50, 15, NULL, 0, 10, 3, 305, NULL),
  (500, 14, 20, 10, 100, 0, 10, 3, 306, NULL),
  (501, 2, NULL, 15, NULL, 3, 4, 1, 307, NULL),
  (502, 14, NULL, 15, NULL, 1, 7, 1, 308, NULL),
  (503, 11, 80, 15, 100, 0, 10, 3, 5, 30),
  (504, 1, NULL, 15, NULL, 0, 7, 1, 309, NULL),
  (505, 14, NULL, 10, NULL, 0, 10, 1, 310, NULL),
  (506, 8, 65, 10, 100, 0, 10, 3, 311, NULL),
  (507, 3, 60, 10, 100, 0, 10, 2, 312, NULL),
  (508, 9, NULL, 10, NULL, 0, 7, 1, 313, NULL),
  (509, 2, 60, 10, 90, -6, 10, 2, 314, NULL),
  (510, 10, 60, 15, 100, 0, 11, 3, 315, NULL),
  (511, 17, NULL, 15, 100, 0, 10, 1, 316, NULL),
  (512, 3, 55, 15, 100, 0, 10, 2, 318, NULL),
  (513, 1, NULL, 15, NULL, 0, 10, 1, 319, NULL),
  (514, 1, 70, 5, 100, 0, 10, 2, 320, NULL),
  (515, 2, NULL, 5, 100, 0, 10, 3, 321, NULL),
  (516, 1, NULL, 15, NULL, 0, 10, 1, 324, NULL),
  (517, 10, 100, 5, 50, 0, 10, 3, 5, 100),
  (518, 11, 80, 10, 100, 0, 10, 3, 325, NULL),
  (519, 10, 80, 10, 100, 0, 10, 3, 326, NULL),
  (520, 12, 80, 10, 100, 0, 10, 3, 327, NULL),
  (521, 13, 70, 20, 100, 0, 10, 3, 229, NULL),
  (522, 7, 50, 20, 100, 0, 11, 3, 72, 100),
  (523, 5, 60, 20, 100, 0, 9, 2, 71, 100),
  (524, 15, 60, 10, 90, 0, 10, 3, 289, 100),
  (525, 16, 60, 10, 90, -6, 10, 2, 314, NULL),
  (526, 1, NULL, 30, NULL, 0, 7, 1, 328, NULL),
  (527, 13, 55, 15, 95, 0, 11, 3, 21, 100),
  (528, 13, 90, 15, 100, 0, 10, 2, 49, NULL),
  (529, 5, 80, 10, 95, 0, 10, 2, 44, NULL),
  (530, 16, 40, 15, 90, 0, 10, 2, 45, NULL),
  (531, 14, 60, 25, 100, 0, 10, 2, 32, 30),
  (532, 12, 75, 10, 100, 0, 10, 2, 4, NULL),
  (533, 2, 90, 15, 100, 0, 10, 2, 304, NULL),
  (534, 11, 75, 10, 95, 0, 10, 2, 70, 50),
  (535, 10, NULL, 10, 100, 0, 10, 2, 292, NULL),
  (536, 12, 65, 10, 90, 0, 10, 3, 74, 50),
  (537, 7, 65, 20, 100, 0, 10, 2, 151, 30),
  (538, 12, NULL, 10, NULL, 0, 7, 1, 329, NULL),
  (539, 17, 85, 10, 95, 0, 10, 3, 74, 40),
  (540, 14, 100, 10, 100, 0, 10, 3, 283, NULL),
  (541, 1, 25, 10, 85, 0, 10, 2, 30, NULL),
  (542, 3, 110, 10, 70, 0, 10, 3, 338, 30),
  (543, 1, 120, 15, 100, 0, 10, 2, 49, NULL),
  (544, 9, 50, 15, 85, 0, 10, 2, 45, NULL),
  (545, 10, 100, 5, 100, 0, 9, 3, 5, 30),
  (546, 1, 120, 5, 100, 0, 10, 3, 269, NULL),
  (547, 1, 75, 10, 100, 0, 11, 3, 330, 10),
  (548, 2, 85, 10, 100, 0, 10, 3, 283, NULL),
  (549, 15, 65, 10, 95, 0, 11, 3, 331, 100),
  (550, 13, 130, 5, 85, 0, 10, 2, 7, 20),
  (551, 10, 130, 5, 85, 0, 10, 3, 5, 20),
  (552, 10, 80, 10, 100, 0, 10, 3, 277, 50),
  (553, 15, 140, 5, 90, 0, 10, 2, 332, 30),
  (554, 15, 140, 5, 90, 0, 10, 3, 333, 30),
  (555, 17, 55, 15, 95, 0, 11, 3, 72, 100),
  (556, 15, 85, 10, 90, 0, 10, 2, 32, 30),
  (557, 10, 180, 5, 95, 0, 10, 2, 335, 100),
  (558, 10, 100, 5, 100, 0, 10, 3, 336, NULL),
  (559, 13, 100, 5, 100, 0, 10, 2, 337, NULL),
  (560, 2, 80, 10, 95, 0, 10, 2, 339, NULL),
  (561, 2, NULL, 10, NULL, 0, 4, 1, 340, NULL),
  (562, 4, 120, 10, 90, 0, 10, 3, 341, NULL),
  (563, 5, NULL, 10, NULL, 0, 14, 1, 342, 100),
  (564, 7, NULL, 20, NULL, 0, 6, 1, 343, NULL),
  (565, 7, 30, 25, 100, 0, 10, 2, 344, NULL),
  (566, 8, 90, 10, 100, 0, 10, 2, 273, NULL),
  (567, 8, NULL, 20, 100, 0, 10, 1, 345, NULL),
  (568, 1, NULL, 30, 100, 0, 10, 1, 346, 100),
  (569, 13, NULL, 25, NULL, 1, 12, 1, 347, NULL),
  (570, 13, 50, 20, 100, 0, 9, 3, 348, NULL),
  (571, 12, NULL, 20, 100, 0, 10, 1, 349, NULL),
  (572, 12, 90, 15, 100, 0, 9, 2, 1, NULL),
  (573, 15, 70, 20, 100, 0, 10, 3, 350, 10),
  (574, 18, 40, 15, NULL, 0, 11, 3, 18, NULL),
  (575, 17, NULL, 20, 100, 0, 10, 1, 351, 100),
  (576, 17, NULL, 20, NULL, 0, 10, 1, 352, NULL),
  (577, 18, 50, 10, 100, 0, 10, 3, 353, NULL),
  (578, 18, NULL, 10, NULL, 3, 4, 1, 354, NULL),
  (579, 18, NULL, 10, NULL, 0, 14, 1, 355, 100),
  (580, 12, NULL, 10, NULL, 0, 12, 1, 356, NULL),
  (581, 18, NULL, 10, NULL, 0, 12, 1, 357, NULL),
  (582, 13, NULL, 20, NULL, 0, 10, 1, 358, NULL),
  (583, 18, 90, 10, 90, 0, 10, 2, 69, 10),
  (584, 18, 40, 30, 100, 0, 10, 3, 1, NULL),
  (585, 18, 95, 15, 100, 0, 10, 3, 72, 30),
  (586, 1, 140, 10, 100, 0, 9, 3, 1, NULL),
  (587, 18, NULL, 10, NULL, 0, 12, 1, 359, NULL),
  (588, 9, NULL, 10, NULL, 4, 7, 1, 360, NULL),
  (589, 1, NULL, 20, NULL, 0, 10, 1, 19, 100),
  (590, 1, NULL, 20, NULL, 0, 10, 1, 361, 100),
  (591, 6, 100, 5, 95, 0, 11, 2, 139, 50),
  (592, 11, 110, 5, 95, 0, 10, 3, 5, 30),
  (594, 11, 15, 20, 100, 1, 10, 2, 30, NULL),
  (595, 10, 65, 10, 100, 0, 10, 3, 72, 100),
  (596, 12, NULL, 10, NULL, 4, 7, 1, 362, NULL),
  (597, 18, NULL, 20, NULL, 0, 3, 1, 363, NULL),
  (598, 13, NULL, 15, 100, 0, 10, 1, 369, NULL),
  (599, 4, NULL, 20, 100, 0, 11, 1, 364, 100),
  (600, 7, NULL, 20, 100, 1, 10, 1, 365, NULL),
  (601, 18, NULL, 10, NULL, 0, 7, 1, 366, NULL),
  (602, 13, NULL, 20, NULL, 0, 13, 1, 367, NULL),
  (603, 1, NULL, 30, NULL, 0, 4, 1, 370, NULL),
  (604, 13, NULL, 10, NULL, 0, 12, 1, 368, NULL),
  (605, 18, 80, 10, 100, 0, 11, 3, 1, NULL),
  (608, 18, NULL, 30, 100, 1, 10, 1, 19, NULL),
  (609, 13, 20, 20, 100, 0, 10, 2, 7, 100),
  (610, 1, 40, 40, 100, 0, 10, 2, 102, NULL),
  (611, 7, 20, 20, 100, 0, 10, 3, 43, 100),
  (612, 2, 40, 20, 100, 0, 10, 2, 140, 100),
  (613, 3, 80, 10, 100, 0, 10, 3, 353, NULL),
  (616, 5, 90, 10, 100, 0, 11, 2, 1, NULL),
  (617, 18, 140, 5, 90, 0, 10, 3, 270, NULL),
  (618, 11, 110, 10, 85, 0, 11, 3, 1, NULL),
  (619, 5, 120, 10, 85, 0, 11, 2, 1, NULL),
  (620, 3, 120, 5, 100, 0, 10, 2, 230, NULL),
  (621, 17, 100, 5, NULL, 0, 10, 2, 360, NULL);

CREATE TABLE IF NOT EXISTS `attacks_translation` (
  `languages_id` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `attacks_id` int(4) unsigned NOT NULL COMMENT 'Attack ID (Foreign)',
  `name` varchar(255) NOT NULL COMMENT 'Name of the attack',
  `description` text NOT NULL COMMENT 'Description of the attack',
  CONSTRAINT FK_ATTACK_ID FOREIGN KEY (attacks_id) REFERENCES attacks(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ATTACK_LANGUAGE FOREIGN KEY (languages_id) REFERENCES languages(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (languages_id, attacks_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `attacks_translation` (`languages_id`, `attacks_id`, `name`, `description`) VALUES
('FR', 1, 'Écras\'Face', 'Écrase l’ennemi avec les pattes avant ou la queue, par exemple.'),
('EN', 1, 'Pound', 'The target is physically pounded with a long tail, a foreleg, or the like.'),
('FR', 2, 'Poing-Karaté', 'L’ennemi est tranché violemment. Taux de critiques élevé.'),
('EN', 2, 'Karate Chop', 'The target is attacked with a sharp chop. Critical hits land more easily.'),
('FR', 3, 'Torgnoles', 'Gifle rapidement l’ennemi de deux à cinq fois d’affilée.'),
('EN', 3, 'Double Slap', 'The target is slapped repeatedly, back and forth, two to five times in a row.'),
('FR', 4, 'Poing Comète', 'Une tornade de coups de poing qui frappe de deux à cinq fois d’affilée.'),
('EN', 4, 'Comet Punch', 'The target is hit with a flurry of punches that strike two to five times in a row.'),
('FR', 5, 'Ultimapoing', 'L’ennemi reçoit un coup de poing d’une puissance incroyable.'),
('EN', 5, 'Mega Punch', 'The target is slugged by a punch thrown with muscle-packed power.'),
('FR', 6, 'Jackpot', 'Des pièces sont lancées sur l’ennemi. Permet d’obtenir de l’argent à la fin du combat.'),
('EN', 6, 'Pay Day', 'Numerous coins are hurled at the target to inflict damage. Money is earned after the battle.'),
('FR', 7, 'Poing de Feu', 'Un coup de poing enflammé vient frapper l’ennemi. Peut le brûler.'),
('EN', 7, 'Fire Punch', 'The target is punched with a fiery fist. This may also leave the target with a burn.'),
('FR', 8, 'Poing-Glace', 'Un coup de poing glacé vient frapper l’ennemi. Peut le geler.'),
('EN', 8, 'Ice Punch', 'The target is punched with an icy fist. This may also leave the target frozen.'),
('FR', 9, 'Poing-Éclair', 'Un coup de poing électrique vient frapper l’ennemi. Peut le paralyser.'),
('EN', 9, 'Thunder Punch', 'The target is punched with an electrified fist. This may also leave the target with paralysis.'),
('FR', 10, 'Griffe', 'Lacère l’ennemi avec des griffes acérées pour lui infliger des dégâts.'),
('EN', 10, 'Scratch', 'Hard, pointed, sharp claws rake the target to inflict damage.'),
('FR', 11, 'Force Poigne', 'L’ennemi est attrapé et compressé par les côtés.'),
('EN', 11, 'Vice Grip', 'The target is gripped and squeezed from both sides to inflict damage.'),
('FR', 12, 'Guillotine', 'De méchantes pinces lacèrent l’ennemi, le mettant K.O. sur le coup s’il est touché.'),
('EN', 12, 'Guillotine', 'A vicious, tearing attack with big pincers. The target faints instantly if this attack hits.'),
('FR', 13, 'Coupe-Vent', 'Attaque en deux tours. Des lames de vent frappent l’ennemi au second tour. Taux de critiques élevé.'),
('EN', 13, 'Razor Wind', 'A two-turn attack. Blades of wind hit opposing Pokémon on the second turn. Critical hits land more easily.'),
('FR', 14, 'Danse-Lames', 'Danse frénétique qui exalte l’esprit combatif. Augmente beaucoup l’Attaque du lanceur.'),
('EN', 14, 'Swords Dance', 'A frenetic dance to uplift the fighting spirit. This sharply raises the user’s Attack stat.'),
('FR', 15, 'Coupe', 'Coupe l’ennemi avec des lames ou des griffes. Hors combat, permet de couper des arbres fins.'),
('EN', 15, 'Cut', 'The target is cut with a scythe or claw. This can also be used to cut down thin trees.'),
('FR', 16, 'Tornade', 'Le lanceur bat des ailes pour générer une bourrasque qui blesse l’ennemi.'),
('EN', 16, 'Gust', 'A gust of wind is whipped up by wings and launched at the target to inflict damage.'),
('FR', 17, 'Cru-Aile', 'L’ennemi est frappé par de larges ailes déployées pour infliger des dégâts.'),
('EN', 17, 'Wing Attack', 'The target is struck with large, imposing wings spread wide to inflict damage.'),
('FR', 18, 'Cyclone', 'Éjecte le Pokémon ennemi et le remplace par un autre. Dans la nature, met fin au combat.'),
('EN', 18, 'Whirlwind', 'The target is blown away, and a different Pokémon is dragged out. In the wild, this ends a battle against a single Pokémon.'),
('FR', 19, 'Vol', 'Le lanceur s’envole au premier tour et frappe au second. Permet aussi de voler jusqu’à une ville déjà visitée.'),
('EN', 19, 'Fly', 'The user soars and then strikes its target on the next turn. This can also be used to fly to any familiar town.'),
('FR', 20, 'Étreinte', 'Ligote l’ennemi avec les tentacules ou le corps pour l’écraser durant quatre à cinq tours.'),
('EN', 20, 'Bind', 'Things such as long bodies or tentacles are used to bind and squeeze the target for four to five turns.'),
('FR', 21, 'Souplesse', 'Fouette l’ennemi avec la queue ou une liane, par exemple, pour infliger des dégâts.'),
('EN', 21, 'Slam', 'The target is slammed with a long tail, vines, or the like to inflict damage.'),
('FR', 22, 'Fouet Lianes', 'Fouette l’ennemi avec de fines lianes pour infliger des dégâts.'),
('EN', 22, 'Vine Whip', 'The target is struck with slender, whiplike vines to inflict damage.'),
('FR', 23, 'Écrasement', 'Écrase l’ennemi avec un énorme pied. Peut aussi l’apeurer.'),
('EN', 23, 'Stomp', 'The target is stomped with a big foot. This may also make the target flinch.'),
('FR', 24, 'Double Pied', 'Deux coups de pied qui frappent l’ennemi deux fois d’affilée.'),
('EN', 24, 'Double Kick', 'The target is quickly kicked twice in succession using both feet.'),
('FR', 25, 'Ultimawashi', 'Un coup de pied surpuissant et intense qui frappe l’ennemi.'),
('EN', 25, 'Mega Kick', 'The target is attacked by a kick launched with muscle-packed power.'),
('FR', 26, 'Pied Sauté', 'Le lanceur s’envole pour décocher un coup de pied sauté. S’il échoue, le lanceur se blesse.'),
('EN', 26, 'Jump Kick', 'The user jumps up high, then strikes with a kick. If the kick misses, the user hurts itself.'),
('FR', 27, 'Mawashi Geri', 'Le lanceur effectue un coup de pied tournoyant et extrêmement rapide. Peut apeurer l’ennemi.'),
('EN', 27, 'Rolling Kick', 'The user lashes out with a quick, spinning kick. This may also make the target flinch.'),
('FR', 28, 'Jet de Sable', 'Lance du sable au visage de l’ennemi pour baisser sa Précision.'),
('EN', 28, 'Sand Attack', 'Sand is hurled in the target’s face, reducing the target’s accuracy.'),
('FR', 29, 'Coup d\'Boule', 'Le lanceur donne un coup de tête. Peut apeurer l’ennemi.'),
('EN', 29, 'Headbutt', 'The user sticks out its head and attacks by charging straight into the target. This may also make the target flinch.'),
('FR', 30, 'Koud\'Korne', 'Frappe l’ennemi d’un coup de corne pointue pour infliger des dégâts.'),
('EN', 30, 'Horn Attack', 'The target is jabbed with a sharply pointed horn to inflict damage.'),
('FR', 31, 'Furie', 'Frappe l’ennemi deux à cinq fois d’affilée avec un bec ou une corne, par exemple.'),
('EN', 31, 'Fury Attack', 'The target is jabbed repeatedly with a horn or beak two to five times in a row.'),
('FR', 32, 'Empal\'Korne', 'Un coup de corne en vrille qui empale l’ennemi, le mettant K.O. sur le coup s’il est touché.'),
('EN', 32, 'Horn Drill', 'The user stabs the target with a horn that rotates like a drill. The target faints instantly if this attack hits.'),
('FR', 33, 'Charge', 'Le lanceur charge l’ennemi et le percute de tout son corps.'),
('EN', 33, 'Tackle', 'A physical attack in which the user charges and slams into the target with its whole body.'),
('FR', 34, 'Plaquage', 'Le lanceur se laisse tomber sur l’ennemi de tout son poids. Peut aussi le paralyser.'),
('EN', 34, 'Body Slam', 'The user drops onto the target with its full body weight. This may also leave the target with paralysis.'),
('FR', 35, 'Ligotage', 'Le lanceur ligote l’ennemi avec des lianes ou son corps pour l’écraser durant quatre à cinq tours.'),
('EN', 35, 'Wrap', 'A long body or vines are used to wrap and squeeze the target for four to five turns.'),
('FR', 36, 'Bélier', 'Une charge violente qui blesse aussi légèrement le lanceur.'),
('EN', 36, 'Take Down', 'A reckless, full-body charge attack for slamming into the target. This also damages the user a little.'),
('FR', 37, 'Mania', 'Une attaque furieuse qui dure de deux à trois tours. Le lanceur devient confus.'),
('EN', 37, 'Thrash', 'The user rampages and attacks for two to three turns. The user then becomes confused.'),
('FR', 38, 'Damoclès', 'Une charge dangereuse et imprudente. Blesse aussi gravement le lanceur.'),
('EN', 38, 'Double-Edge', 'A reckless, life-risking tackle. This also damages the user quite a lot.'),
('FR', 39, 'Mimi-Queue', 'Le lanceur remue son adorable queue pour tromper la vigilance de l’ennemi et baisser sa Défense.'),
('EN', 39, 'Tail Whip', 'The user wags its tail cutely, making opposing Pokémon less wary and lowering their Defense stat.'),
('FR', 40, 'Dard-Venin', 'Un dard toxique qui transperce l’ennemi. Peut aussi l’empoisonner.'),
('EN', 40, 'Poison Sting', 'The user stabs the target with a poisonous stinger. This may also poison the target.'),
('FR', 41, 'Double-Dard', 'Un double coup de dard qui transperce l’ennemi deux fois d’affilée. Peut aussi l’empoisonner.'),
('EN', 41, 'Twineedle', 'The user damages the target twice in succession by jabbing it with two spikes. This may also poison the target.'),
('FR', 42, 'Dard-Nuée', 'Envoie une rafale de dards. Peut toucher de deux à cinq fois.'),
('EN', 42, 'Pin Missile', 'Sharp spikes are shot at the target in rapid succession. They hit two to five times in a row.'),
('FR', 43, 'Groz\'Yeux', 'Le lanceur fait les gros yeux à l’ennemi pour l’intimider et baisser sa Défense.'),
('EN', 43, 'Leer', 'The user gives opposing Pokémon an intimidating leer that lowers the Defense stat.'),
('FR', 44, 'Morsure', 'L’ennemi est mordu par de tranchantes canines. Peut l’apeurer.'),
('EN', 44, 'Bite', 'The target is bitten with viciously sharp fangs. This may also make the target flinch.'),
('FR', 45, 'Rugissement', 'Le lanceur pousse un cri tout mimi pour tromper la vigilance de l’ennemi et baisser son Attaque.'),
('EN', 45, 'Growl', 'The user growls in an endearing way, making opposing Pokémon less wary. This lowers their Attack stats.'),
('FR', 46, 'Hurlement', 'Effraie le Pokémon ennemi et le remplace par un autre. Dans la nature, met fin au combat.'),
('EN', 46, 'Roar', 'The target is scared off, and a different Pokémon is dragged out. In the wild, this ends a battle against a single Pokémon.'),
('FR', 47, 'Berceuse', 'Une berceuse plonge l’ennemi dans un profond sommeil.'),
('EN', 47, 'Sing', 'A soothing lullaby is sung in a calming voice that puts the target into a deep slumber.'),
('FR', 48, 'Ultrason', 'Le lanceur produit d’étranges ondes sonores qui rendent la cible confuse.'),
('EN', 48, 'Supersonic', 'The user generates odd sound waves from its body that confuse the target.'),
('FR', 49, 'Sonicboom', 'Une onde de choc destructrice qui inflige toujours 20 PV de dégâts.'),
('EN', 49, 'Sonic Boom', 'The target is hit with a destructive shock wave that always inflicts 20 HP damage.'),
('FR', 50, 'Entrave', 'Empêche l’ennemi d’employer à nouveau sa dernière attaque. Dure quatre tours.'),
('EN', 50, 'Disable', 'For four turns, this move prevents the target from using the move it last used.'),
('FR', 51, 'Acide', 'Le lanceur attaque l’ennemi avec un jet d’acide corrosif. Peut aussi baisser sa Défense Spéciale.'),
('EN', 51, 'Acid', 'The opposing Pokémon are attacked with a spray of harsh acid. This may also lower their Sp. Def stats.'),
('FR', 52, 'Flammèche', 'L’ennemi est attaqué par une faible flamme. Peut aussi le brûler.'),
('EN', 52, 'Ember', 'The target is attacked with small flames. This may also leave the target with a burn.'),
('FR', 53, 'Lance-Flammes', 'L’ennemi reçoit un torrent de flammes. Peut aussi le brûler.'),
('EN', 53, 'Flamethrower', 'The target is scorched with an intense blast of fire. This may also leave the target with a burn.'),
('FR', 54, 'Brume', 'Une brume blanche enveloppe l’équipe du lanceur et empêche la réduction des stats pour cinq tours.'),
('EN', 54, 'Mist', 'The user cloaks itself and its allies in a white mist that prevents any of their stats from being lowered for five turns.'),
('FR', 55, 'Pistolet à O', 'De l’eau est projetée avec force sur l’ennemi.'),
('EN', 55, 'Water Gun', 'The target is blasted with a forceful shot of water.'),
('FR', 56, 'Hydrocanon', 'Un puissant jet d’eau est dirigé sur l’ennemi.'),
('EN', 56, 'Hydro Pump', 'The target is blasted by a huge volume of water launched under great pressure.'),
('FR', 57, 'Surf', 'Une énorme vague s’abat sur le champ de bataille. Permet aussi de voyager sur l’eau.'),
('EN', 57, 'Surf', 'The user attacks everything around it by swamping its surroundings with a giant wave. This can also be used for crossing water.'),
('FR', 58, 'Laser Glace', 'Un rayon de glace frappe l’ennemi. Peut aussi le geler.'),
('EN', 58, 'Ice Beam', 'The target is struck with an icy-cold beam of energy. This may also leave the target frozen.'),
('FR', 59, 'Blizzard', 'Une violente tempête de neige est déclenchée sur l’ennemi. Peut aussi le geler.'),
('EN', 59, 'Blizzard', 'A howling blizzard is summoned to strike opposing Pokémon. This may also leave the opposing Pokémon frozen.'),
('FR', 60, 'Rafale Psy', 'Un étrange rayon frappe l’ennemi. Peut aussi le rendre confus.'),
('EN', 60, 'Psybeam', 'The target is attacked with a peculiar ray. This may also leave the target confused.'),
('FR', 61, 'Bulles d\'O', 'Des bulles sont envoyées avec puissance sur l’ennemi. Peut aussi baisser sa Vitesse.'),
('EN', 61, 'Bubble Beam', 'A spray of bubbles is forcefully ejected at the target. This may also lower its Speed stat.'),
('FR', 62, 'Onde Boréale', 'Envoie un rayon arc-en-ciel sur l’ennemi. Peut aussi baisser son Attaque.'),
('EN', 62, 'Aurora Beam', 'The target is hit with a rainbow-colored beam. This may also lower the target’s Attack stat.'),
('FR', 63, 'Ultralaser', 'Projette un puissant rayon sur l’ennemi. Le lanceur doit se reposer au tour suivant.'),
('EN', 63, 'Hyper Beam', 'The target is attacked with a powerful beam. The user can’t move on the next turn.'),
('FR', 64, 'Picpic', 'Frappe l’ennemi d’un bec pointu ou d’une corne pour infliger des dégâts.'),
('EN', 64, 'Peck', 'The target is jabbed with a sharply pointed beak or horn.'),
('FR', 65, 'Bec Vrille', 'Une attaque utilisant le bec comme une perceuse.'),
('EN', 65, 'Drill Peck', 'A corkscrewing attack with a sharp beak acting as a drill.'),
('FR', 66, 'Sacrifice', 'Le lanceur agrippe l’ennemi et l’écrase au sol. Blesse aussi légèrement le lanceur.'),
('EN', 66, 'Submission', 'The user grabs the target and recklessly dives for the ground. This also damages the user a little.'),
('FR', 67, 'Balayage', 'Un puissant coup de pied bas qui fauche l’ennemi. Il est plus efficace contre les ennemis lourds.'),
('EN', 67, 'Low Kick', 'A powerful low kick that makes the target fall over. The heavier the target, the greater the move’s power.'),
('FR', 68, 'Riposte', 'Une riposte qui répond à toute attaque physique en infligeant le double de dégâts.'),
('EN', 68, 'Counter', 'A retaliation move that counters any physical attack, inflicting double the damage taken.'),
('FR', 69, 'Frappe Atlas', 'L’ennemi est projeté grâce au pouvoir de la gravité. Inflige des dégâts équivalents au niveau du lanceur.'),
('EN', 69, 'Seismic Toss', 'The target is thrown using the power of gravity. It inflicts damage equal to the user’s level.'),
('FR', 70, 'Force', 'Le lanceur cogne l’ennemi de toutes ses forces. Permet aussi de déplacer des rochers.'),
('EN', 70, 'Strength', 'The target is slugged with a punch thrown at maximum power. This can also be used to move heavy boulders.'),
('FR', 71, 'Vol-Vie', 'Une attaque qui convertit la moitié des dégâts infligés en PV pour le lanceur.'),
('EN', 71, 'Absorb', 'A nutrient-draining attack. The user’s HP is restored by half the damage taken by the target.'),
('FR', 72, 'Méga-Sangsue', 'Une attaque qui convertit la moitié des dégâts infligés en PV pour le lanceur.'),
('EN', 72, 'Mega Drain', 'A nutrient-draining attack. The user’s HP is restored by half the damage taken by the target.'),
('FR', 73, 'Vampigraine', 'Une graine est semée sur l’ennemi. À chaque tour, elle lui dérobe des PV que le lanceur récupère.'),
('EN', 73, 'Leech Seed', 'A seed is planted on the target. It steals some HP from the target every turn.'),
('FR', 74, 'Croissance', 'Le corps du lanceur se développe. Augmente l’Attaque et l’Attaque Spéciale.'),
('EN', 74, 'Growth', 'The user’s body grows all at once, raising the Attack and Sp. Atk stats.'),
('FR', 75, 'Tranch\'Herbe', 'Des feuilles aiguisées comme des rasoirs entaillent l’ennemi. Taux de critiques élevé.'),
('EN', 75, 'Razor Leaf', 'Sharp-edged leaves are launched to slash at the opposing Pokémon. Critical hits land more easily.'),
('FR', 76, 'Lance-Soleil', 'Absorbe la lumière au premier tour et envoie un rayon puissant au tour suivant.'),
('EN', 76, 'Solar Beam', 'A two-turn attack. The user gathers light, then blasts a bundled beam on the next turn.'),
('FR', 77, 'Poudre Toxik', 'Une poudre toxique empoisonne l’ennemi.'),
('EN', 77, 'Poison Powder', 'The user scatters a cloud of poisonous dust on the target. This may also poison the target.'),
('FR', 78, 'Para-Spore', 'Le lanceur répand sur l’ennemi une poudre qui le paralyse.'),
('EN', 78, 'Stun Spore', 'The user scatters a cloud of numbing powder that paralyzes the target.'),
('FR', 79, 'Poudre Dodo', 'Le lanceur répand une poudre soporifique qui endort la cible.'),
('EN', 79, 'Sleep Powder', 'The user scatters a big cloud of sleep-inducing dust around the target.'),
('FR', 80, 'Danse-Fleur', 'Le lanceur attaque en projetant des pétales pendant deux à trois tours avant de céder à la confusion.'),
('EN', 80, 'Petal Dance', 'The user attacks the target by scattering petals for two to three turns. The user then becomes confused.'),
('FR', 81, 'Sécrétion', 'Le lanceur crache de la soie pour ligoter l’ennemi et baisser sensiblement sa Vitesse.'),
('EN', 81, 'String Shot', 'The opposing Pokémon are bound with silk blown from the user’s mouth that harshly lowers the Speed stat.'),
('FR', 82, 'Draco-Rage', 'La colère du lanceur déclenche une onde de choc destructrice qui inflige toujours 40 PV de dégâts.'),
('EN', 82, 'Dragon Rage', 'This attack hits the target with a shock wave of pure rage. This attack always inflicts 40 HP damage.'),
('FR', 83, 'Danse Flamme', 'Un tourbillon de flammes emprisonne l’ennemi pendant quatre à cinq tours.'),
('EN', 83, 'Fire Spin', 'The target becomes trapped within a fierce vortex of fire that rages for four to five turns.'),
('FR', 84, 'Éclair', 'Une décharge électrique tombe sur l’ennemi. Peut aussi le paralyser.'),
('EN', 84, 'Thunder Shock', 'A jolt of electricity crashes down on the target to inflict damage. This may also leave the target with paralysis.'),
('FR', 85, 'Tonnerre', 'Une grosse décharge électrique tombe sur l’ennemi. Peut aussi le paralyser.'),
('EN', 85, 'Thunderbolt', 'A strong electric blast crashes down on the target. This may also leave the target with paralysis.'),
('FR', 86, 'Cage-Éclair', 'Un faible choc électrique frappe l’ennemi. Si l’attaque le touche, celui-ci est paralysé.'),
('EN', 86, 'Thunder Wave', 'The user launches a weak jolt of electricity that paralyzes the target.'),
('FR', 87, 'Fatal-Foudre', 'La foudre tombe sur l’ennemi pour lui infliger des dégâts. Peut aussi le paralyser.'),
('EN', 87, 'Thunder', 'A wicked thunderbolt is dropped on the target to inflict damage. This may also leave the target with paralysis.'),
('FR', 88, 'Jet-Pierres', 'Le lanceur lâche une pierre sur l’ennemi.'),
('EN', 88, 'Rock Throw', 'The user picks up and throws a small rock at the target to attack.'),
('FR', 89, 'Séisme', 'Le lanceur provoque un tremblement de terre touchant tous les Pokémon autour de lui.'),
('EN', 89, 'Earthquake', 'The user sets off an earthquake that strikes every Pokémon around it. '),
('FR', 90, 'Abîme', 'Le lanceur fait tomber l’ennemi dans une crevasse. Si cette attaque réussit, elle met K.O. sur le coup.'),
('EN', 90, 'Fissure', 'The user opens up a fissure in the ground and drops the target in. The target faints instantly if this attack hits.'),
('FR', 91, 'Tunnel', 'Le lanceur creuse au premier tour et frappe au second. Permet aussi de sortir des grottes.'),
('EN', 91, 'Dig', 'The user burrows, then attacks on the next turn. It can also be used to exit dungeons.'),
('FR', 92, 'Toxik', 'Empoisonne gravement l’ennemi. Les dégâts dus au poison augmentent à chaque tour.'),
('EN', 92, 'Toxic', 'A move that leaves the target badly poisoned. Its poison damage worsens every turn.'),
('FR', 93, 'Choc Mental', 'Une faible vague télékinétique frappe l’ennemi. Peut aussi le plonger dans la confusion.'),
('EN', 93, 'Confusion', 'The target is hit by a weak telekinetic force. This may also confuse the target.'),
('FR', 94, 'Psyko', 'Une puissante force télékinétique frappe l’ennemi. Peut aussi faire baisser sa Défense Spéciale.'),
('EN', 94, 'Psychic', 'The target is hit by a strong telekinetic force. This may also lower the target’s Sp. Def stat.'),
('FR', 95, 'Hypnose', 'Le lanceur hypnotise l’ennemi pour le plonger dans un profond sommeil.'),
('EN', 95, 'Hypnosis', 'The user employs hypnotic suggestion to make the target fall into a deep sleep.'),
('FR', 96, 'Yoga', 'Le lanceur médite pour éveiller son pouvoir latent et augmenter son Attaque.'),
('EN', 96, 'Meditate', 'The user meditates to awaken the power deep within its body and raise its Attack stat.'),
('FR', 97, 'Hâte', 'Le lanceur se relaxe et allège son corps pour augmenter considérablement sa Vitesse.'),
('EN', 97, 'Agility', 'The user relaxes and lightens its body to move faster. This sharply raises the Speed stat.'),
('FR', 98, 'Vive-Attaque', 'Le lanceur fonce sur l’ennemi si rapidement qu’on parvient à peine à le discerner. Frappe toujours en premier.'),
('EN', 98, 'Quick Attack', 'The user lunges at the target at a speed that makes it almost invisible. This move always goes first.'),
('FR', 99, 'Frénésie', 'Une fois activée, cette capacité augmente l’Attaque du lanceur à mesure que celui-ci subit des attaques.'),
('EN', 99, 'Rage', 'As long as this move is in use, the power of rage raises the Attack stat each time the user is hit in battle.'),
('FR', 100, 'Téléport', 'Permet de fuir un Pokémon sauvage. Permet aussi de revenir au dernier Centre Pokémon visité.'),
('EN', 100, 'Teleport', 'Use it to flee from any wild Pokémon. It can also warp to the last Pokémon Center visited.'),
('FR', 101, 'Ombre Nocturne', 'Le lanceur invoque un mirage. Inflige des dégâts équivalents au niveau du lanceur.'),
('EN', 101, 'Night Shade', 'The user makes the target see a frightening mirage. It inflicts damage equal to the user’s level.'),
('FR', 102, 'Copie', 'Le lanceur copie la dernière capacité utilisée par la cible et la conserve tant qu’il reste au combat.'),
('EN', 102, 'Mimic', 'The user copies the target’s last move. The move can be used during battle until the Pokémon is switched out.'),
('FR', 103, 'Grincement', 'Cri strident qui baisse beaucoup la Défense de l’ennemi.'),
('EN', 103, 'Screech', 'An earsplitting screech harshly lowers the target’s Defense stat.'),
('FR', 104, 'Reflet', 'Le lanceur se déplace si vite qu’il crée des copies illusoires de lui, augmentant son Esquive.'),
('EN', 104, 'Double Team', 'By moving rapidly, the user makes illusory copies of itself to raise its evasiveness.'),
('FR', 105, 'Soin', 'Un soin qui permet au lanceur de récupérer jusqu’à la moitié de ses PV max.'),
('EN', 105, 'Recover', 'Restoring its own cells, the user restores its own HP by half of its max HP.'),
('FR', 106, 'Armure', 'Le lanceur contracte tous ses muscles pour augmenter sa Défense.'),
('EN', 106, 'Harden', 'The user stiffens all the muscles in its body to raise its Defense stat.'),
('FR', 107, 'Lilliput', 'Le lanceur comprime son corps pour se faire tout petit et augmenter fortement son Esquive.'),
('EN', 107, 'Minimize', 'The user compresses its body to make itself look smaller, which sharply raises its evasiveness.'),
('FR', 108, 'Brouillard', 'Le lanceur disperse un nuage d’encre ou de fumée. Réduit la Précision de l’ennemi.'),
('EN', 108, 'Smokescreen', 'The user releases an obscuring cloud of smoke or ink. This lowers the target’s accuracy.'),
('FR', 109, 'Onde Folie', 'Un rayon sinistre qui plonge l’ennemi dans un état de confusion.'),
('EN', 109, 'Confuse Ray', 'The target is exposed to a sinister ray that triggers confusion.'),
('FR', 110, 'Repli', 'Le lanceur se recroqueville dans sa carapace, ce qui augmente sa Défense.'),
('EN', 110, 'Withdraw', 'The user withdraws its body into its hard shell, raising its Defense stat.'),
('FR', 111, 'Boul\'Armure', 'Le lanceur s’enroule pour cacher ses points faibles, ce qui augmente sa Défense.'),
('EN', 111, 'Defense Curl', 'The user curls up to conceal weak spots and raise its Defense stat.'),
('FR', 112, 'Bouclier', 'Le lanceur érige un mur solide qui augmente fortement sa Défense.'),
('EN', 112, 'Barrier', 'The user throws up a sturdy wall that sharply raises its Defense stat.'),
('FR', 113, 'Mur Lumière', 'Un fabuleux mur de lumière qui réduit les dégâts causés par les attaques spéciales durant cinq tours. '),
('EN', 113, 'Light Screen', 'A wondrous wall of light is put up to reduce damage from special attacks for five turns.'),
('FR', 114, 'Buée Noire', 'Un brouillard qui annule les changements de stats de tous les Pokémon au combat.'),
('EN', 114, 'Haze', 'The user creates a haze that eliminates every stat change among all the Pokémon engaged in battle.'),
('FR', 115, 'Protection', 'Un fabuleux mur de lumière qui réduit les dégâts causés par les attaques physiques durant cinq tours.'),
('EN', 115, 'Reflect', 'A wondrous wall of light is put up to reduce damage from physical attacks for five turns.'),
('FR', 116, 'Puissance', 'Le lanceur prend une profonde inspiration et se concentre pour augmenter son taux de critiques.'),
('EN', 116, 'Focus Energy', 'The user takes a deep breath and focuses so that critical hits land more easily.'),
('FR', 117, 'Patience', 'Le lanceur encaisse les coups durant deux tours et réplique en infligeant le double des dégâts subis.'),
('EN', 117, 'Bide', 'The user endures attacks for two turns, then strikes back to cause double the damage taken.'),
('FR', 118, 'Métronome', 'Agite un doigt et stimule le cerveau pour utiliser presque n’importe quelle capacité au hasard.'),
('EN', 118, 'Metronome', 'The user waggles a finger and stimulates its brain into randomly using nearly any move.'),
('FR', 119, 'Mimique', 'Le lanceur riposte à l’attaque de l’ennemi avec la même attaque.'),
('EN', 119, 'Mirror Move', 'The user counters the target by mimicking the target’s last move.'),
('FR', 120, 'Destruction', 'Le lanceur explose en blessant tous les Pokémon autour de lui. Le lanceur tombe K.O.'),
('EN', 120, 'Self-Destruct', 'The user attacks everything around it by causing an explosion. The user faints upon using this move.'),
('FR', 121, 'Bomb\'Œuf', 'De toutes ses forces, le lanceur jette un gros œuf sur l’ennemi pour lui infliger des dégâts.'),
('EN', 121, 'Egg Bomb', 'A large egg is hurled at the target with maximum force to inflict damage.'),
('FR', 122, 'Léchouille', 'Un grand coup de langue qui inflige des dégâts à l’ennemi. Peut aussi le paralyser.'),
('EN', 122, 'Lick', 'The target is licked with a long tongue, causing damage. This may also leave the target with paralysis.'),
('FR', 123, 'Purédpois', 'Le lanceur attaque à l’aide d’une éruption de gaz répugnants. Peut aussi empoisonner l’ennemi.'),
('EN', 123, 'Smog', 'The target is attacked with a discharge of filthy gases. This may also poison the target.'),
('FR', 124, 'Détritus', 'Des détritus toxiques sont projetés sur l’ennemi. Peut aussi l’empoisonner.'),
('EN', 124, 'Sludge', 'Unsanitary sludge is hurled at the target. This may also poison the target.'),
('FR', 125, 'Massd\'Os', 'Le lanceur frappe l’ennemi à grands coups d’os. Peut aussi l’apeurer.'),
('EN', 125, 'Bone Club', 'The user clubs the target with a bone. This may also make the target flinch.'),
('FR', 126, 'Déflagration', 'Un déluge de flammes ardentes submerge l’ennemi. Peut aussi le brûler.'),
('EN', 126, 'Fire Blast', 'The target is attacked with an intense blast of all-consuming fire. This may also leave the target with a burn.'),
('FR', 127, 'Cascade', 'Le lanceur charge l’ennemi à une vitesse remarquable, ce qui peut l’apeurer. Permet aussi de franchir une cascade.'),
('EN', 127, 'Waterfall', 'The user charges at the target and may make it flinch. This can also be used to climb a waterfall.'),
('FR', 128, 'Claquoir', 'Le lanceur piège l’ennemi dans sa dure coquille et l’écrase pendant quatre à cinq tours.'),
('EN', 128, 'Clamp', 'The target is clamped and squeezed by the user’s very thick and sturdy shell for four to five turns.'),
('FR', 129, 'Météores', 'Le lanceur envoie des rayons d’étoiles. Touche toujours l’ennemi.'),
('EN', 129, 'Swift', 'Star-shaped rays are shot at the opposing Pokémon. This attack never misses.'),
('FR', 130, 'Coud\'Krâne', 'Le lanceur baisse la tête pour augmenter sa Défense au premier tour et percuter l’ennemi au second.'),
('EN', 130, 'Skull Bash', 'The user tucks in its head to raise its Defense in the first turn, then rams the target on the next turn.'),
('FR', 131, 'Picanon', 'Envoie une rafale de dards. Peut toucher de deux à cinq fois.'),
('EN', 131, 'Spike Cannon', 'Sharp spikes are shot at the target in rapid succession. They hit two to five times in a row.'),
('FR', 132, 'Constriction', 'De longs tentacules ou lianes attaquent l’ennemi. Peut aussi baisser sa Vitesse.'),
('EN', 132, 'Constrict', 'The target is attacked with long, creeping tentacles or vines. This may also lower the target’s Speed stat.'),
('FR', 133, 'Amnésie', 'Le lanceur fait le vide dans son esprit pour oublier ses soucis. Augmente fortement sa Défense Spéciale.'),
('EN', 133, 'Amnesia', 'The user temporarily empties its mind to forget its concerns. This sharply raises the user’s Sp. Def stat.'),
('FR', 134, 'Télékinésie', 'Le lanceur distrait l’ennemi en pliant une cuiller, ce qui baisse sa Précision.'),
('EN', 134, 'Kinesis', 'The user distracts the target by bending a spoon. This lowers the target’s accuracy.'),
('FR', 135, 'E-Coque', 'Le lanceur récupère jusqu’à la moitié de ses PV max. En dehors des combats, permet de transférer des PV du lanceur à un allié.'),
('EN', 135, 'Soft-Boiled', 'The user restores its own HP by up to half of its max HP. May also be used in the field to heal HP.'),
('FR', 136, 'Pied Voltige', 'Le lanceur s’élance pour effectuer un coup de genou sauté. S’il échoue, le lanceur se blesse.'),
('EN', 136, 'High Jump Kick', 'The target is attacked with a knee kick from a jump. If it misses, the user is hurt instead.'),
('FR', 137, 'Regard Médusant', 'Le lanceur intimide l’ennemi grâce à son regard terrifiant pour le paralyser.'),
('EN', 137, 'Glare', 'The user intimidates the target with the pattern on its belly to cause paralysis.'),
('FR', 138, 'Dévorêve', 'Le lanceur mange le rêve de l’ennemi endormi. Il récupère en PV la moitié des dégâts infligés.'),
('EN', 138, 'Dream Eater', 'The user eats the dreams of a sleeping target. It absorbs half the damage caused to heal its own HP.'),
('FR', 139, 'Gaz Toxik', 'Un nuage de gaz toxique est projeté au visage de l’ennemi pour l’empoisonner.'),
('EN', 139, 'Poison Gas', 'A cloud of poison gas is sprayed in the face of opposing Pokémon. This may also poison those hit.'),
('FR', 140, 'Pilonnage', 'Projette de deux à cinq grosses boules sur l’ennemi.'),
('EN', 140, 'Barrage', 'Round objects are hurled at the target to strike two to five times in a row.'),
('FR', 141, 'Vampirisme', 'Une attaque qui aspire le sang de l’ennemi. La moitié des dégâts sont convertis en PV pour le lanceur.'),
('EN', 141, 'Leech Life', 'The user drains the target’s blood. The user’s HP is restored by half the damage taken by the target.'),
('FR', 142, 'Grobisou', 'Le lanceur fait un bisou à l’ennemi en prenant une mine effrayante. Endort l’ennemi.'),
('EN', 142, 'Lovely Kiss', 'With a scary face, the user tries to force a kiss on the target. If it succeeds, the target falls asleep.'),
('FR', 143, 'Piqué', 'Une attaque en deux tours au taux de critiques élevé. Peut aussi apeurer l’ennemi.'),
('EN', 143, 'Sky Attack', 'A second-turn attack move where critical hits land more easily. This may also make the target flinch.'),
('FR', 144, 'Morphing', 'Le lanceur devient une copie de sa cible et obtient la même palette de capacités.'),
('EN', 144, 'Transform', 'The user transforms into a copy of the target right down to having the same move set.'),
('FR', 145, 'Écume', 'Des bulles frappent l’ennemi. Peut réduire sa Vitesse.'),
('EN', 145, 'Bubble', 'A spray of countless bubbles is jetted at the opposing Pokémon. This may also lower their Speed stats.'),
('FR', 146, 'Uppercut', 'Un enchaînement de coups de poing cadencés frappe l’ennemi. Peut aussi le rendre confus.'),
('EN', 146, 'Dizzy Punch', 'The target is hit with rhythmically launched punches. This may also leave the target confused.'),
('FR', 147, 'Spore', 'Le lanceur répand un nuage de spores qui endort.'),
('EN', 147, 'Spore', 'The user scatters bursts of spores that induce sleep.'),
('FR', 148, 'Flash', 'Explosion lumineuse qui fait baisser la Précision de l’ennemi. Illumine les endroits sombres.'),
('EN', 148, 'Flash', 'The user flashes a bright light that cuts the target’s accuracy. It can also be used to illuminate dark caves.'),
('FR', 149, 'Vague Psy', 'Une étrange onde d’énergie chaude frappe l’ennemi. Cette attaque est d’intensité variable.'),
('EN', 149, 'Psywave', 'The target is attacked with an odd psychic wave. The attack varies in intensity.'),
('FR', 150, 'Trempette', 'Le lanceur barbote et éclabousse les environs. Cette capacité n’a aucun effet.'),
('EN', 150, 'Splash', 'The user just flops and splashes around to no effect at all...'),
('FR', 151, 'Acidarmure', 'Le lanceur modifie sa structure moléculaire pour se liquéfier et augmenter fortement sa Défense.'),
('EN', 151, 'Acid Armor', 'The user alters its cellular structure to liquefy itself, sharply raising its Defense stat.'),
('FR', 152, 'Pince-Masse', 'Une grande pince martèle l’ennemi. Taux de critiques élevé.'),
('EN', 152, 'Crabhammer', 'The target is hammered with a large pincer. Critical hits land more easily.'),
('FR', 153, 'Explosion', 'Le lanceur explose et inflige des dégâts à tous les Pokémon autour de lui. Met K.O. le lanceur.'),
('EN', 153, 'Explosion', 'The user attacks everything around it by causing a tremendous explosion. The user faints upon using this move.'),
('FR', 154, 'Combo-Griffe', 'L’ennemi est lacéré par des faux ou des griffes de deux à cinq fois d’affilée.'),
('EN', 154, 'Fury Swipes', 'The target is raked with sharp claws or scythes quickly two to five times in a row.'),
('FR', 155, 'Osmerang', 'Le lanceur projette son os comme un boomerang. Cette attaque frappe à l’aller et au retour.'),
('EN', 155, 'Bonemerang', 'The user throws the bone it holds. The bone loops to hit the target twice, coming and going.'),
('FR', 156, 'Repos', 'Le lanceur regagne tous ses PV et soigne ses problèmes de statut, puis il dort pendant deux tours.'),
('EN', 156, 'Rest', 'The user goes to sleep for two turns. This fully restores the user’s HP and heals any status conditions.'),
('FR', 157, 'Éboulement', 'Envoie de gros rochers sur l’ennemi pour infliger des dégâts. Peut aussi l’apeurer.'),
('EN', 157, 'Rock Slide', 'Large boulders are hurled at the opposing Pokémon to inflict damage. This may also make the opposing Pokémon flinch.'),
('FR', 158, 'Croc de Mort', 'Le lanceur mord l’ennemi à l’aide de ses incisives aiguisées. Peut aussi l’apeurer.'),
('EN', 158, 'Hyper Fang', 'The user bites hard on the target with its sharp front fangs. This may also make the target flinch.'),
('FR', 159, 'Affûtage', 'Le lanceur réduit son nombre de polygones pour accentuer ses angles et augmenter son Attaque.'),
('EN', 159, 'Sharpen', 'The user lowers its polygon count to make itself more jagged, raising the Attack stat.'),
('FR', 160, 'Conversion', 'Le lanceur change de type pour prendre celui de la première capacité de sa liste.'),
('EN', 160, 'Conversion', 'The user changes its type to become the same type as the move at the top of the list of moves it knows.'),
('FR', 161, 'Triplattaque', 'Le lanceur envoie trois boules d’énergie simultanément. Peut aussi paralyser, brûler ou geler l’ennemi.'),
('EN', 161, 'Tri Attack', 'The user strikes with a simultaneous three-beam attack. May also burn, freeze, or leave the target with paralysis.'),
('FR', 162, 'Croc Fatal', 'Une vilaine morsure d’incisives qui réduit de moitié les PV de l’ennemi.'),
('EN', 162, 'Super Fang', 'The user chomps hard on the target with its sharp front fangs. This cuts the target’s HP in half.'),
('FR', 163, 'Tranche', 'Un coup de griffe ou autre tranche l’ennemi. Taux de critiques élevé.'),
('EN', 163, 'Slash', 'The target is attacked with a slash of claws or blades. Critical hits land more easily.'),
('FR', 164, 'Clonage', 'Le lanceur fait une copie de lui-même en sacrifiant quelques PV. La copie sert de leurre.'),
('EN', 164, 'Substitute', 'The user makes a copy of itself using some of its HP. The copy serves as the user’s decoy.'),
('FR', 165, 'Lutte', 'Une attaque désespérée, utilisée quand le lanceur n’a plus de PP. Le blesse aussi légèrement.'),
('EN', 165, 'Struggle', 'An attack that is used in desperation only if the user has no PP. This also damages the user a little.'),
('FR', 166, 'Gribouille', 'Le lanceur apprend la dernière capacité utilisée par la  cible. Gribouille disparaît après utilisation.'),
('EN', 166, 'Sketch', 'It enables the user to permanently learn the move last used by the target. Once used, Sketch disappears.'),
('FR', 167, 'Triple Pied', 'Une salve de un à trois coups de pied dont la puissance augmente à chaque coup porté.'),
('EN', 167, 'Triple Kick', 'A consecutive three-kick attack that becomes more powerful with each successive hit.'),
('FR', 168, 'Larcin', 'Le lanceur attaque la cible et vole son objet. Le lanceur ne peut rien voler s’il tient déjà un objet.'),
('EN', 168, 'Thief', 'The user attacks and steals the target’s held item simultaneously. The user can’t steal anything if it already holds an item.'),
('FR', 169, 'Toile', 'Le lanceur enserre l’ennemi à l’aide d’une fine soie gluante pour l’empêcher de fuir le combat.'),
('EN', 169, 'Spider Web', 'The user ensnares the target with thin, gooey silk so it can’t flee from battle.'),
('FR', 170, 'Lire-Esprit', 'Le lanceur analyse les mouvements de l’ennemi pour être sûr de toucher au coup suivant.'),
('EN', 170, 'Mind Reader', 'The user senses the target’s movements with its mind to ensure its next attack does not miss the target.'),
('FR', 171, 'Cauchemar', 'Un cauchemar qui inflige des dégâts à chaque tour à un ennemi endormi.'),
('EN', 171, 'Nightmare', 'A sleeping target sees a nightmare that inflicts some damage every turn.'),
('FR', 172, 'Roue de Feu', 'Le lanceur s’entoure de feu et charge l’ennemi. Peut aussi le brûler.'),
('EN', 172, 'Flame Wheel', 'The user cloaks itself in fire and charges at the target. This may also leave the target with a burn.'),
('FR', 173, 'Ronflement', 'Une attaque qui ne fonctionne que si le lanceur est endormi. Le boucan peut aussi apeurer l’ennemi.'),
('EN', 173, 'Snore', 'An attack that can be used only if the user is asleep. The harsh noise may also make the target flinch.'),
('FR', 174, 'Malédiction', 'Une capacité à l’effet différent selon que le lanceur est un Pokémon Spectre ou non.'),
('EN', 174, 'Curse', 'A move that works differently for the Ghost type than for all other types.'),
('FR', 175, 'Fléau', 'Le lanceur fait tournoyer son fléau. Plus ses PV sont bas, plus l’attaque est puissante.'),
('EN', 175, 'Flail', 'The user flails about aimlessly to attack. The less HP the user has, the greater the move’s power.'),
('FR', 176, 'Conversion 2', 'Le lanceur change de type pour être résistant au type de la dernière attaque lancée par sa cible.'),
('EN', 176, 'Conversion 2', 'The user changes its type to make itself resistant to the type of the attack the opponent used last.'),
('FR', 177, 'Aéroblast', 'Le lanceur projette une tornade sur l’ennemi pour infliger des dégâts. Taux de critiques élevé.'),
('EN', 177, 'Aeroblast', 'A vortex of air is shot at the target to inflict damage. Critical hits land more easily.'),
('FR', 178, 'Spore Coton', 'Le lanceur libère des spores cotonneuses qui collent à l’ennemi et baissent fortement sa Vitesse.'),
('EN', 178, 'Cotton Spore', 'The user releases cotton-like spores that cling to the opposing Pokémon, which harshly lowers their Speed stats.'),
('FR', 179, 'Contre', 'Le lanceur ne retient plus ses coups. Plus ses PV sont bas et plus l’attaque est puissante.'),
('EN', 179, 'Reversal', 'An all-out attack that becomes more powerful the less HP the user has.'),
('FR', 180, 'Dépit', 'Le lanceur exprime son ressentiment en retirant 4 PP de la dernière attaque de l’ennemi.'),
('EN', 180, 'Spite', 'The user unleashes its grudge on the move last used by the target by cutting 4 PP from it.'),
('FR', 181, 'Poudreuse', 'Le lanceur projette de la neige poudreuse. Peut aussi geler l’ennemi.'),
('EN', 181, 'Powder Snow', 'The user attacks with a chilling gust of powdery snow. This may also freeze the opposing Pokémon.'),
('FR', 182, 'Abri', 'Le lanceur esquive toutes les attaques. Le risque d’échec augmente lorsque la capacité est utilisée plusieurs fois de suite.'),
('EN', 182, 'Protect', 'Enables the user to evade all attacks. Its chance of failing rises if it is used in succession.'),
('FR', 183, 'Mach Punch', 'Coup de poing fulgurant. Frappe toujours en premier.'),
('EN', 183, 'Mach Punch', 'The user throws a punch at blinding speed. This move always goes first.'),
('FR', 184, 'Grimace', 'Une grimace qui effraie l’ennemi et réduit fortement sa Vitesse.'),
('EN', 184, 'Scary Face', 'The user frightens the target with a scary face to harshly lower its Speed stat.'),
('FR', 185, 'Feinte', 'Le lanceur s’approche l’air de rien avant de frapper par surprise. N’échoue jamais.'),
('EN', 185, 'Feint Attack', 'The user approaches the target disarmingly, then throws a sucker punch. This attack never misses.'),
('FR', 186, 'Doux Baiser', 'Le lanceur envoie un bisou si mignon et désarmant qu’il plonge l’ennemi dans la confusion.'),
('EN', 186, 'Sweet Kiss', 'The user kisses the target with a sweet, angelic cuteness that causes confusion.'),
('FR', 187, 'Cognobidon', 'Améliore l’Attaque au maximum en sacrifiant la moitié des PV max.'),
('EN', 187, 'Belly Drum', 'The user maximizes its Attack stat in exchange for HP equal to half its max HP.'),
('FR', 188, 'Bomb-Beurk', 'Des détritus toxiques sont projetés sur l’ennemi. Peut aussi l’empoisonner.'),
('EN', 188, 'Sludge Bomb', 'Unsanitary sludge is hurled at the target. This may also poison the target.'),
('FR', 189, 'Coud\'Boue', 'Le lanceur envoie de la boue au visage de l’ennemi pour infliger des dégâts et baisser sa Précision.'),
('EN', 189, 'Mud-Slap', 'The user hurls mud in the target’s face to inflict damage and lower its accuracy.'),
('FR', 190, 'Octazooka', 'Le lanceur attaque en projetant de l’encre au visage de l’ennemi. Peut aussi baisser sa Précision. '),
('EN', 190, 'Octazooka', 'The user attacks by spraying ink in the target’s face or eyes. This may also lower the target’s accuracy.'),
('FR', 191, 'Picots', 'Le lanceur disperse des piquants sur le sol pour blesser tout ennemi qui entre au combat.'),
('EN', 191, 'Spikes', 'The user lays a trap of spikes at the opposing team’s feet. The trap hurts Pokémon that switch into battle.'),
('FR', 192, 'Élecanon', 'Un boulet de canon électrifié qui inflige des dégâts et paralyse l’ennemi.'),
('EN', 192, 'Zap Cannon', 'The user fires an electric blast like a cannon to inflict damage and cause paralysis.'),
('FR', 193, 'Clairvoyance', 'Permet de toucher un Pokémon Spectre avec n’importe quelle capacité ou de toucher un ennemi insaisissable.'),
('EN', 193, 'Foresight', 'Enables a Ghost-type target to be hit by Normal- and Fighting-type attacks. This also enables an evasive target to be hit.'),
('FR', 194, 'Prélèvem. Destin', 'Quand cette capacité est activée, elle met K.O. un ennemi qui porte un coup fatal au lanceur.'),
('EN', 194, 'Destiny Bond', 'When this move is used, if the user faints, the Pokémon that landed the knockout hit also faints.'),
('FR', 195, 'Requiem', 'Tout Pokémon qui entend ce requiem est K.O. dans trois tours à moins qu’il ne soit remplacé.'),
('EN', 195, 'Perish Song', 'Any Pokémon that hears this song faints in three turns, unless it switches out of battle.'),
('FR', 196, 'Vent Glace', 'Une bourrasque de vent froid blesse l’ennemi. Réduit aussi sa Vitesse.'),
('EN', 196, 'Icy Wind', 'The user attacks with a gust of chilled air. This also lowers the opposing Pokémon’s Speed stats.'),
('FR', 197, 'Détection', 'Le lanceur esquive toutes les attaques. Le risque d’échec augmente lorsque la capacité est utilisée plusieurs fois de suite.'),
('EN', 197, 'Detect', 'Enables the user to evade all attacks. Its chance of failing rises if it is used in succession.'),
('FR', 198, 'Charge-Os', 'Le lanceur frappe l’ennemi avec un os de deux à cinq fois d’affilée.'),
('EN', 198, 'Bone Rush', 'The user strikes the target with a hard bone two to five times in a row.'),
('FR', 199, 'Verrouillage', 'Verrouille l’ennemi pour ne pas le rater au tour suivant.'),
('EN', 199, 'Lock-On', 'The user takes sure aim at the target. This ensures the next attack does not miss the target.'),
('FR', 200, 'Colère', 'Le lanceur laisse éclater sa rage et attaque pendant deux à trois tours avant de céder à la confusion.'),
('EN', 200, 'Outrage', 'The user rampages and attacks for two to three turns. The user then becomes confused.'),
('FR', 201, 'Tempête de Sable', 'Une tempête de sable qui blesse tous les Pokémon durant cinq tours, sauf ceux de types Roche, Sol et Acier.'),
('EN', 201, 'Sandstorm', 'A five-turn sandstorm is summoned to hurt all combatants except the Rock, Ground, and Steel types.'),
('FR', 202, 'Giga-Sangsue', 'Une attaque qui convertit la moitié des dégâts infligés en PV pour le lanceur.'),
('EN', 202, 'Giga Drain', 'A nutrient-draining attack. The user’s HP is restored by half the damage taken by the target.'),
('FR', 203, 'Ténacité', 'Le lanceur résiste aux attaques avec 1 PV. Peut échouer si utilisée plusieurs fois de suite.'),
('EN', 203, 'Endure', 'The user endures any attack with at least 1 HP. Its chance of failing rises if it is used in succession.'),
('FR', 204, 'Charme', 'Le lanceur fait les yeux doux pour berner l’ennemi et réduire considérablement son Attaque.'),
('EN', 204, 'Charm', 'The user gazes at the target rather charmingly, making it less wary. This harshly lowers its Attack stat.'),
('FR', 205, 'Roulade', 'Un rocher roule sur l’ennemi pendant cinq tours. L’attaque gagne en puissance à chaque coup.'),
('EN', 205, 'Rollout', 'The user continually rolls into the target over five turns. It becomes more powerful each time it hits.'),
('FR', 206, 'Faux-Chage', 'Le lanceur retient ses coups pour que l’ennemi garde au moins 1 PV et ne tombe pas K.O.'),
('EN', 206, 'False Swipe', 'A restrained attack that prevents the target from fainting. The target is left with at least 1 HP.'),
('FR', 207, 'Vantardise', 'Fait enrager la cible et la plonge dans la confusion, mais augmente fortement son Attaque.'),
('EN', 207, 'Swagger', 'The user enrages and confuses the target. However, this also sharply raises the target’s Attack stat.'),
('FR', 208, 'Lait à Boire', 'Le lanceur récupère jusqu’à la moitié de ses PV max. En dehors des combats, permet de transférer des PV du lanceur à un allié.'),
('EN', 208, 'Milk Drink', 'The user restores its own HP by up to half of its max HP. May also be used in the field to heal HP.'),
('FR', 209, 'Étincelle', 'Lance une charge électrique sur l’ennemi. Peut aussi le paralyser.'),
('EN', 209, 'Spark', 'The user throws an electrically charged tackle at the target. This may also leave the target with paralysis.'),
('FR', 210, 'Taillade', 'Un coup de faux ou de griffe dont la force augmente quand il touche plusieurs fois d’affilée.'),
('EN', 210, 'Fury Cutter', 'The target is slashed with scythes or claws. This attack becomes more powerful if it hits in succession.'),
('FR', 211, 'Aile d\'Acier', 'Le lanceur frappe l’ennemi avec des ailes d’acier. Peut aussi augmenter la Défense du lanceur.'),
('EN', 211, 'Steel Wing', 'The target is hit with wings of steel. This may also raise the user’s Defense stat.'),
('FR', 212, 'Regard Noir', 'Le lanceur pétrifie l’ennemi en lui lançant un regard noir. Il devient incapable de s’enfuir.'),
('EN', 212, 'Mean Look', 'The user pins the target with a dark, arresting look. The target becomes unable to flee.'),
('FR', 213, 'Attraction', 'Si l’ennemi est du sexe opposé, il tombe amoureux et rechigne alors à attaquer.'),
('EN', 213, 'Attract', 'If it is the opposite gender of the user, the target becomes infatuated and less likely to attack.'),
('FR', 214, 'Blabla Dodo', 'Le lanceur utilise une de ses capacités au hasard pendant qu’il dort.'),
('EN', 214, 'Sleep Talk', 'While it is asleep, the user randomly uses one of the moves it knows.'),
('FR', 215, 'Glas de Soin', 'Carillon apaisant qui soigne les problèmes de statut de tous les Pokémon de l’équipe.'),
('EN', 215, 'Heal Bell', 'The user makes a soothing bell chime to heal the status conditions of all the party Pokémon.'),
('FR', 216, 'Retour', 'Plus le Pokémon apprécie son Dresseur, plus la puissance de cette attaque furieuse augmente.'),
('EN', 216, 'Return', 'A full-power attack that grows more powerful the more the user likes its Trainer.'),
('FR', 217, 'Cadeau', 'Le lanceur attaque en offrant un cadeau piégé à la cible. Peut cependant restaurer certains de ses PV.'),
('EN', 217, 'Present', 'The user attacks by giving the target a gift with a hidden trap. It restores HP sometimes, however.'),
('FR', 218, 'Frustration', 'Moins le Pokémon aime son Dresseur, plus cette attaque est puissante.'),
('EN', 218, 'Frustration', 'A full-power attack that grows more powerful the less the user likes its Trainer.'),
('FR', 219, 'Rune Protect', 'Crée un champ protecteur qui empêche tous les problèmes de statut pendant cinq tours.'),
('EN', 219, 'Safeguard', 'The user creates a protective field that prevents status conditions for five turns.'),
('FR', 220, 'Balance', 'Le lanceur ajoute ses PV à ceux de sa cible et les répartit équitablement.'),
('EN', 220, 'Pain Split', 'The user adds its HP to the target’s HP, then equally shares the combined HP with the target.'),
('FR', 221, 'Feu Sacré', 'Le lanceur génère un feu mystique d’une intensité redoutable pour attaquer l’ennemi. Peut aussi le brûler.'),
('EN', 221, 'Sacred Fire', 'The target is razed with a mystical fire of great intensity. This may also leave the target with a burn.'),
('FR', 222, 'Ampleur', 'Provoque un tremblement de terre d’intensité variable qui affecte tous les Pokémon alentour. L’efficacité varie.'),
('EN', 222, 'Magnitude', 'The user attacks everything around it with a ground-shaking quake. Its power varies.'),
('FR', 223, 'Dynamopoing', 'Le lanceur rassemble ses forces et envoie un coup de poing à l’ennemi. Rend ce dernier confus.'),
('EN', 223, 'Dynamic Punch', 'The user punches the target with full, concentrated power. This confuses the target if it hits.'),
('FR', 224, 'Mégacorne', 'Le lanceur utilise ses gigantesques cornes pour charger l’ennemi.'),
('EN', 224, 'Megahorn', 'Using its tough and impressive horn, the user rams into the target with no letup.'),
('FR', 225, 'Dracosouffle', 'Le lanceur souffle fort sur l’ennemi pour lui infliger des dégâts. Peut aussi le paralyser.'),
('EN', 225, 'Dragon Breath', 'The user exhales a mighty gust that inflicts damage. This may also leave the target with paralysis.'),
('FR', 226, 'Relais', 'Le lanceur échange sa place et tout changement de stat avec un Pokémon de l’équipe.'),
('EN', 226, 'Baton Pass', 'The user switches places with a party Pokémon in waiting and passes along any stat changes.'),
('FR', 227, 'Encore', 'Oblige l’ennemi à répéter durant trois tours la dernière capacité utilisée.'),
('EN', 227, 'Encore', 'The user compels the target to keep using only the move it last used for three turns.'),
('FR', 228, 'Poursuite', 'Une attaque qui inflige deux fois plus de dégâts à un ennemi qui s’apprête à être remplacé.'),
('EN', 228, 'Pursuit', 'An attack move that inflicts double damage if used on a target that is switching out of battle.'),
('FR', 229, 'Tour Rapide', 'Une attaque tournoyante pouvant aussi annuler par exemple Étreinte, Ligotage, Vampigraine ou Picots.'),
('EN', 229, 'Rapid Spin', 'A spin attack that can also eliminate such moves as Bind, Wrap, Leech Seed, and Spikes.'),
('FR', 230, 'Doux Parfum', 'Un doux parfum qui réduit sensiblement l’Esquive de l’ennemi. Utilisé hors d’un combat, attire aussi les Pokémon sauvages.'),
('EN', 230, 'Sweet Scent', 'A sweet scent that harshly lowers opposing Pokémon’s evasiveness. This also lures wild Pokémon if used in places such as tall grass.'),
('FR', 231, 'Queue de Fer', 'Attaque l’ennemi avec une queue de fer. Peut aussi baisser sa Défense.'),
('EN', 231, 'Iron Tail', 'The target is slammed with a steel-hard tail. This may also lower the target’s Defense stat.'),
('FR', 232, 'Griffe Acier', 'Attaque avec des griffes d’acier. Peut aussi augmenter l’Attaque du lanceur.'),
('EN', 232, 'Metal Claw', 'The target is raked with steel claws. This may also raise the user’s Attack stat.'),
('FR', 233, 'Corps Perdu', 'Le lanceur porte son coup en dernier. En échange, cette capacité n’échoue jamais.'),
('EN', 233, 'Vital Throw', 'The user attacks last. In return, this throw move never misses.'),
('FR', 234, 'Aurore', 'Un soin qui restaure des PV au lanceur. Son efficacité varie en fonction de la météo.'),
('EN', 234, 'Morning Sun', 'The user restores its own HP. The amount of HP regained varies with the weather.'),
('FR', 235, 'Synthèse', 'Un soin qui restaure des PV au lanceur. Son efficacité varie en fonction de la météo.'),
('EN', 235, 'Synthesis', 'The user restores its own HP. The amount of HP regained varies with the weather.'),
('FR', 236, 'Rayon Lune', 'Un soin qui restaure des PV au lanceur. Son efficacité varie en fonction de la météo.'),
('EN', 236, 'Moonlight', 'The user restores its own HP. The amount of HP regained varies with the weather.'),
('FR', 237, 'Puissance Cachée', 'Attaque dont le type dépend du Pokémon qui l’utilise.'),
('EN', 237, 'Hidden Power', 'A unique attack that varies in type depending on the Pokémon using it. '),
('FR', 238, 'Coup-Croix', 'Le lanceur délivre un coup double en croisant les avant-bras. Taux de critiques élevé.'),
('EN', 238, 'Cross Chop', 'The user delivers a double chop with its forearms crossed. Critical hits land more easily.'),
('FR', 239, 'Ouragan', 'Déclenche un terrible ouragan sur l’ennemi. Peut aussi l’apeurer.'),
('EN', 239, 'Twister', 'The user whips up a vicious tornado to tear at the opposing Pokémon. This may also make them flinch.'),
('FR', 240, 'Danse Pluie', 'Invoque de fortes pluies qui durent cinq tours et augmentent la puissance des capacités de type Eau.'),
('EN', 240, 'Rain Dance', 'The user summons a heavy rain that falls for five turns, powering up Water-type moves.'),
('FR', 241, 'Zénith', 'Fait briller le soleil pendant cinq tours, augmentant la puissance des capacités de type Feu.'),
('EN', 241, 'Sunny Day', 'The user intensifies the sun for five turns, powering up Fire-type moves.'),
('FR', 242, 'Mâchouille', 'Le lanceur mord l’ennemi de ses crocs pointus. Peut aussi baisser sa Défense.'),
('EN', 242, 'Crunch', 'The user crunches up the target with sharp fangs. This may also lower the target’s Defense stat.'),
('FR', 243, 'Voile Miroir', 'Une riposte qui contre n’importe quelle attaque spéciale en infligeant le double des dégâts subis.'),
('EN', 243, 'Mirror Coat', 'A retaliation move that counters any special attack, inflicting double the damage taken.'),
('FR', 244, 'Boost', 'Une autohypnose qui permet au lanceur de copier les changements de stats de la cible.'),
('EN', 244, 'Psych Up', 'The user hypnotizes itself into copying any stat change made by the target.'),
('FR', 245, 'Vitesse Extrême', 'Le lanceur charge à une vitesse renversante. Frappe toujours en premier.'),
('EN', 245, 'Extreme Speed', 'The user charges the target at blinding speed. This move always goes first.'),
('FR', 246, 'Pouvoir Antique', 'Une attaque préhistorique qui peut augmenter toutes les stats du lanceur d’un seul coup.'),
('EN', 246, 'Ancient Power', 'The user attacks with a prehistoric power. This may also raise all the user’s stats at once.'),
('FR', 247, 'Ball\'Ombre', 'Projette une grande ombre sur l’ennemi. Peut aussi faire baisser sa Défense Spéciale.'),
('EN', 247, 'Shadow Ball', 'The user hurls a shadowy blob at the target. This may also lower the target’s Sp. Def stat.'),
('FR', 248, 'Prescience', 'De l’énergie psychique vient frapper l’ennemi deux tours après l’utilisation de cette capacité.'),
('EN', 248, 'Future Sight', 'Two turns after this move is used, a hunk of psychic energy attacks the target.'),
('FR', 249, 'Éclate-Roc', 'Porte un coup de poing à l’ennemi qui peut baisser sa Défense. Peut aussi briser des rochers fissurés.'),
('EN', 249, 'Rock Smash', 'The user attacks with a punch. This may also lower the target’s Defense stat. This move can also shatter rocks in the field.'),
('FR', 250, 'Siphon', 'Piège l’ennemi dans une trombe d’eau pendant quatre à cinq tours.'),
('EN', 250, 'Whirlpool', 'The user traps the target in a violent swirling whirlpool for four to five turns.'),
('FR', 251, 'Baston', 'Le lanceur appelle tous les Pokémon de son équipe à attaquer. Plus ils sont nombreux, plus il y a d’attaques.'),
('EN', 251, 'Beat Up', 'The user gets all party Pokémon to attack the target. The more party Pokémon, the greater the number of attacks.'),
('FR', 252, 'Bluff', 'Permet de frapper en premier et apeure l’ennemi. Ne fonctionne qu’au premier tour.'),
('EN', 252, 'Fake Out', 'An attack that hits first and makes the target flinch. It only works the first turn the user is in battle.'),
('FR', 253, 'Brouhaha', 'Le lanceur attaque en rugissant durant trois tours. Pendant ce temps, aucun Pokémon ne peut s’endormir.'),
('EN', 253, 'Uproar', 'The user attacks in an uproar for three turns. During that time, no one can fall asleep.'),
('FR', 254, 'Stockage', 'Le lanceur accumule de la puissance et augmente sa Défense et sa Défense Spéciale. Peut être utilisé trois fois.'),
('EN', 254, 'Stockpile', 'The user charges up power and raises both its Defense and Sp. Def stats. The move can be used three times.'),
('FR', 255, 'Relâche', 'Libère dans une attaque la puissance précédemment accumulée avec Stockage.'),
('EN', 255, 'Spit Up', 'The power stored using the move Stockpile is released at once in an attack. The more power is stored, the greater the move’s power.'),
('FR', 256, 'Avale', 'Le lanceur absorbe la puissance accumulée avec Stockage pour restaurer ses PV.'),
('EN', 256, 'Swallow', 'The power stored using the move Stockpile is absorbed by the user to heal its HP. Storing more power heals more HP.'),
('FR', 257, 'Canicule', 'Le lanceur provoque une vague de chaleur. Peut aussi brûler l’ennemi.'),
('EN', 257, 'Heat Wave', 'The user attacks by exhaling hot breath on the opposing Pokémon. This may also leave those Pokémon with a burn.'),
('FR', 258, 'Grêle', 'Invoque une tempête de grêle qui dure cinq tours. Ne blesse pas les Pokémon de type Glace.'),
('EN', 258, 'Hail', 'The user summons a hailstorm lasting five turns. It damages all Pokémon except the Ice type.'),
('FR', 259, 'Tourmente', 'Le lanceur irrite l’ennemi pour l’empêcher d’utiliser la même capacité deux fois de suite.'),
('EN', 259, 'Torment', 'The user torments and enrages the target, making it incapable of using the same move twice in a row.'),
('FR', 260, 'Flatterie', 'Rend la cible confuse, mais augmente son Attaque Spéciale.'),
('EN', 260, 'Flatter', 'Flattery is used to confuse the target. However, this also raises the target’s Sp. Atk stat.'),
('FR', 261, 'Feu Follet', 'Lance une sinistre flamme violette à l’ennemi pour lui infliger une brûlure.'),
('EN', 261, 'Will-O-Wisp', 'The user shoots a sinister, bluish-white flame at the target to inflict a burn.'),
('FR', 262, 'Souvenir', 'Le lanceur est mis K.O., mais l’Attaque et l’Attaque Spéciale de l’ennemi baissent beaucoup.'),
('EN', 262, 'Memento', 'The user faints when using this move. In return, this harshly lowers the target’s Attack and Sp. Atk stats.'),
('FR', 263, 'Façade', 'Une attaque dont la puissance double lorsque le lanceur est empoisonné, paralysé ou brûlé.'),
('EN', 263, 'Facade', 'An attack move that doubles its power if the user is poisoned, burned, or has paralysis.'),
('FR', 264, 'Mitra-Poing', 'Le lanceur se concentre avant d’attaquer. Échoue s’il est touché avant d’avoir frappé.'),
('EN', 264, 'Focus Punch', 'The user focuses its mind before launching a punch. This move fails if the user is hit before it is used.'),
('FR', 265, 'Stimulant', 'Cette attaque est doublement efficace sur les Pokémon paralysés, mais elle soigne leur paralysie. '),
('EN', 265, 'Smelling Salts', 'This attack inflicts double damage on a target with paralysis. This also cures the target’s paralysis, however.'),
('FR', 266, 'Par Ici', 'Attire l’attention des ennemis pour les forcer à n’attaquer que le lanceur.'),
('EN', 266, 'Follow Me', 'The user draws attention to itself, making all targets take aim only at the user.'),
('FR', 267, 'Force-Nature', 'Une attaque qui tire sa force de la nature. Son type varie selon le terrain.'),
('EN', 267, 'Nature Power', 'An attack that makes use of nature’s power. Its effects vary depending on the user’s environment.'),
('FR', 268, 'Chargeur', 'Le lanceur concentre sa puissance pour sa prochaine attaque Électrik. Augmente sa Défense Spéciale.'),
('EN', 268, 'Charge', 'The user boosts the power of the Electric move it uses on the next turn. This also raises the user’s Sp. Def stat.'),
('FR', 269, 'Provoc', 'Provoque l’ennemi. L’oblige à n’utiliser que des attaques pendant trois tours.'),
('EN', 269, 'Taunt', 'The target is taunted into a rage that allows it to use only attack moves for three turns.'),
('FR', 270, 'Coup d\'Main', 'Une capacité qui augmente la puissance d’attaque d’un allié.'),
('EN', 270, 'Helping Hand', 'The user assists an ally by boosting the power of that ally’s attack.'),
('FR', 271, 'Tourmagik', 'Le lanceur prend la cible au dépourvu et l’oblige à échanger son objet contre le sien.'),
('EN', 271, 'Trick', 'The user catches the target off guard and swaps its held item with its own.'),
('FR', 272, 'Imitation', 'Imite la cible et copie son talent.'),
('EN', 272, 'Role Play', 'The user mimics the target completely, copying the target’s natural Ability.'),
('FR', 273, 'Vœu', 'Un vœu qui permet de récupérer la moitié des PV max au tour suivant.'),
('EN', 273, 'Wish', 'One turn after this move is used, the target’s HP is restored by half the user’s max HP.'),
('FR', 274, 'Assistance', 'Le lanceur se dépêche d’utiliser une capacité au hasard parmi celles des Pokémon de l’équipe.'),
('EN', 274, 'Assist', 'The user hurriedly and randomly uses a move among those known by other Pokémon in the party.'),
('FR', 275, 'Racines', 'Le lanceur plante ses racines et récupère des PV à chaque tour. Une fois enraciné, il ne peut plus fuir.'),
('EN', 275, 'Ingrain', 'The user lays roots that restore its HP on every turn. Because it is rooted, it can’t switch out.'),
('FR', 276, 'Surpuissance', 'Une attaque puissante, mais qui baisse l’Attaque et la Défense du lanceur.'),
('EN', 276, 'Superpower', 'The user attacks the target with great power. However, this also lowers the user’s Attack and Defense stats.'),
('FR', 277, 'Reflet Magik', 'Une barrière qui renvoie les capacités comme Vampigraine et celles affectant le statut et les stats.'),
('EN', 277, 'Magic Coat', 'A barrier reflects back to the target moves like Leech Seed and moves that damage status.'),
('FR', 278, 'Recyclage', 'Recycle un objet tenu à usage unique déjà utilisé lors du combat pour pouvoir l’utiliser à nouveau.'),
('EN', 278, 'Recycle', 'The user recycles a held item that has been used in battle so it can be used again.'),
('FR', 279, 'Vendetta', 'Une attaque deux fois plus puissante si le lanceur a été blessé par l’ennemi durant ce tour.'),
('EN', 279, 'Revenge', 'An attack move that inflicts double the damage if the user has been hurt by the opponent in the same turn.'),
('FR', 280, 'Casse-Brique', 'Une attaque avec le tranchant de la main. Permet aussi de briser les barrières comme Mur Lumière et Protection.'),
('EN', 280, 'Brick Break', 'The user attacks with a swift chop. It can also break barriers, such as Light Screen and Reflect.'),
('FR', 281, 'Bâillement', 'Fait bâiller l’ennemi qui s’endort au tour suivant.'),
('EN', 281, 'Yawn', 'The user lets loose a huge yawn that lulls the target into falling asleep on the next turn.'),
('FR', 282, 'Sabotage', 'Fait plus de dégâts aux cibles qui tiennent un objet. De plus, fait tomber cet objet et empêche la cible de l’utiliser jusqu’à la fin du combat.'),
('EN', 282, 'Knock Off', 'The user slaps down the target’s held item, and that item can’t be used in that battle. The move does more damage if the target has a held item.'),
('FR', 283, 'Effort', 'Une attaque qui réduit les PV de l’ennemi au niveau des PV du lanceur.'),
('EN', 283, 'Endeavor', 'An attack move that cuts down the target’s HP to equal the user’s HP.'),
('FR', 284, 'Éruption', 'Le lanceur laisse exploser sa colère. Plus ses PV sont bas et moins l’attaque est puissante.'),
('EN', 284, 'Eruption', 'The user attacks opposing Pokémon with explosive fury. The lower the user’s HP, the lower the move’s power.'),
('FR', 285, 'Échange', 'Le lanceur utilise ses pouvoirs psychiques pour échanger son talent avec la cible.'),
('EN', 285, 'Skill Swap', 'The user employs its psychic power to exchange Abilities with the target.'),
('FR', 286, 'Possessif', 'Si l’ennemi et le lanceur ont des capacités en commun, l’ennemi ne pourra pas les utiliser.'),
('EN', 286, 'Imprison', 'If opposing Pokémon know any move also known by the user, they are prevented from using it.'),
('FR', 287, 'Régénération', 'Le lanceur se repose pour guérir d’un empoisonnement, d’une brûlure ou d’une paralysie.'),
('EN', 287, 'Refresh', 'The user rests to cure itself of a poisoning, burn, or paralysis.'),
('FR', 288, 'Rancune', 'Si le lanceur est mis K.O., sa rancune épuise les PP de la capacité utilisée par l’ennemi pour le mettre K.O.'),
('EN', 288, 'Grudge', 'If the user faints, the user’s grudge fully depletes the PP of the opponent’s move that knocked it out.'),
('FR', 289, 'Saisie', 'Lorsqu’une capacité de soin ou de changement de stats est utilisée, le lanceur vole ses effets.'),
('EN', 289, 'Snatch', 'The user steals the effects of any attempts to use a healing or stat-changing move.'),
('FR', 290, 'Force Cachée', 'Les effets de cette attaque varient en fonction de l’environnement. Permet aussi de créer sa propre  Base Secrète.'),
('EN', 290, 'Secret Power', 'An attack whose additional effects depend upon where it was used. Can also be used to make a Secret Base.'),
('FR', 291, 'Plongée', 'Le lanceur plonge sous l’eau au premier tour et frappe au second. Permet aussi de plonger dans l’eau profonde.'),
('EN', 291, 'Dive', 'Diving on the first turn, the user floats up and attacks on the next turn. It can be used to dive deep in the ocean.'),
('FR', 292, 'Cogne', 'Un déluge de coups adressés avec la paume qui frappe de deux à cinq fois d’affilée.'),
('EN', 292, 'Arm Thrust', 'The user lets loose a flurry of open-palmed arm thrusts that hit two to five times in a row.'),
('FR', 293, 'Camouflage', 'Modifie le type du lanceur en fonction du terrain, comme une berge, une grotte, l’herbe, etc.'),
('EN', 293, 'Camouflage', 'The user’s type is changed depending on its environment, such as at water’s edge, in grass, or in a cave.'),
('FR', 294, 'Lumiqueue', 'Le lanceur regarde un flash lumineux fixement. Augmente considérablement son Attaque Spéciale.'),
('EN', 294, 'Tail Glow', 'The user stares at flashing lights to focus its mind, drastically raising its Sp. Atk stat.'),
('FR', 295, 'Lumi-Éclat', 'Le lanceur libère un éclair lumineux. Peut aussi baisser la Défense Spéciale de l’ennemi.'),
('EN', 295, 'Luster Purge', 'The user lets loose a damaging burst of light. This may also lower the target’s Sp. Def stat.'),
('FR', 296, 'Ball\'Brume', 'Une bulle de brume inflige des dégâts à l’ennemi. Peut aussi réduire son Attaque Spéciale.'),
('EN', 296, 'Mist Ball', 'A mist-like flurry of down envelops and damages the target. This may also lower the target’s Sp. Atk stat.'),
('FR', 297, 'Danse-Plume', 'Une montagne de plumes ensevelit l’ennemi et réduit considérablement son Attaque.'),
('EN', 297, 'Feather Dance', 'The user covers the target’s body with a mass of down that harshly lowers its Attack stat.'),
('FR', 298, 'Danse-Folle', 'Danse qui rend confus tous les Pokémon autour du lanceur.'),
('EN', 298, 'Teeter Dance', 'The user performs a wobbly dance that confuses the Pokémon around it.'),
('FR', 299, 'Pied Brûleur', 'Le lanceur envoie un coup de pied au taux de critiques élevé. Peut aussi brûler la cible.'),
('EN', 299, 'Blaze Kick', 'The user launches a kick that lands a critical hit more easily. This may also leave the target with a burn.'),
('FR', 300, 'Lance-Boue', 'Asperge les alentours de boue. Affaiblit les capacités Électrik pendant cinq tours.'),
('EN', 300, 'Mud Sport', 'The user covers itself with mud. This weakens Electric-type moves for five turns.'),
('FR', 301, 'Ball\'Glace', 'Envoie une balle de glace pendant cinq tours. L’attaque gagne en puissance à chaque coup.'),
('EN', 301, 'Ice Ball', 'The user continually rolls into the target over five turns. It becomes stronger each time it hits.'),
('FR', 302, 'Poing Dard', 'Le lanceur attaque en fouettant l’ennemi de ses bras épineux. Peut aussi l’apeurer.'),
('EN', 302, 'Needle Arm', 'The user attacks by wildly swinging its thorny arms. This may also make the target flinch.'),
('FR', 303, 'Paresse', 'Le lanceur se tourne les pouces et récupère jusqu’à la moitié de ses PV max.'),
('EN', 303, 'Slack Off', 'The user slacks off, restoring its own HP by up to half of its max HP.'),
('FR', 304, 'Mégaphone', 'Le lanceur pousse un cri dont l’écho terrifiant a le pouvoir d’infliger des dégâts.'),
('EN', 304, 'Hyper Voice', 'The user lets loose a horribly echoing shout with the power to inflict damage.'),
('FR', 305, 'Crochet Venin', 'Le lanceur mord l’ennemi de ses crocs toxiques. Peut aussi l’empoisonner gravement.'),
('EN', 305, 'Poison Fang', 'The user bites the target with toxic fangs. This may also leave the target badly poisoned.'),
('FR', 306, 'Éclate Griffe', 'Lacère l’ennemi avec des griffes solides et aiguisées. Peut aussi baisser sa Défense.'),
('EN', 306, 'Crush Claw', 'The user slashes the target with hard and sharp claws. This may also lower the target’s Defense stat.'),
('FR', 307, 'Rafale Feu', 'Une explosion ardente souffle l’adversaire. Le lanceur doit se reposer au tour suivant.'),
('EN', 307, 'Blast Burn', 'The target is razed by a fiery explosion. The user can’t move on the next turn.'),
('FR', 308, 'Hydroblast', 'Une trombe d’eau heurte l’ennemi. Le lanceur doit se reposer au tour suivant.'),
('EN', 308, 'Hydro Cannon', 'The target is hit with a watery blast. The user can’t move on the next turn.'),
('FR', 309, 'Poing Météor', 'Un coup de poing lancé à la vitesse d’un météore. Peut aussi augmenter l’Attaque du lanceur.'),
('EN', 309, 'Meteor Mash', 'The target is hit with a hard punch fired like a meteor. This may also raise the user’s Attack stat.'),
('FR', 310, 'Étonnement', 'Le lanceur attaque l’ennemi en poussant un cri terrifiant. Peut aussi l’apeurer.'),
('EN', 310, 'Astonish', 'The user attacks the target while shouting in a startling fashion. This may also make the target flinch.'),
('FR', 311, 'Ball\'Météo', 'Une attaque dont la puissance et le type varient en fonction du temps qu’il fait.'),
('EN', 311, 'Weather Ball', 'An attack move that varies in power and type depending on the weather.'),
('FR', 312, 'Aromathérapie', 'Le lanceur libère un parfum apaisant qui guérit tous les problèmes de statut de l’équipe.'),
('EN', 312, 'Aromatherapy', 'The user releases a soothing scent that heals all status conditions affecting the user’s party.'),
('FR', 313, 'Croco Larme', 'Le lanceur fait semblant de pleurer pour troubler l’ennemi et baisser fortement sa Défense Spéciale.'),
('EN', 313, 'Fake Tears', 'The user feigns crying to fluster the target, harshly lowering its Sp. Def stat.'),
('FR', 314, 'Tranch\'Air', 'Le lanceur appelle des vents tranchants qui lacèrent l’ennemi. Taux de critiques élevé.'),
('EN', 314, 'Air Cutter', 'The user launches razor-like wind to slash the opposing Pokémon. Critical hits land more easily.'),
('FR', 315, 'Surchauffe', 'Attaque l’ennemi à pleine puissance. Le contrecoup baisse fortement l’Attaque Spéciale du lanceur.'),
('EN', 315, 'Overheat', 'The user attacks the target at full power. The attack’s recoil harshly lowers the user’s Sp. Atk stat.'),
('FR', 316, 'Flair', 'Permet de toucher un Pokémon Spectre avec n’importe quelle capacité ou de toucher un ennemi insaisissable.'),
('EN', 316, 'Odor Sleuth', 'Enables a Ghost-type target to be hit by Normal- and Fighting-type attacks. This also enables an evasive target to be hit.'),
('FR', 317, 'Tomberoche', 'Des rochers frappent l’ennemi. Réduit aussi sa Vitesse.'),
('EN', 317, 'Rock Tomb', 'Boulders are hurled at the target. This also lowers the target’s Speed stat by preventing its movement.'),
('FR', 318, 'Vent Argenté', 'Vent qui projette des écailles poudreuses sur l’ennemi. Peut aussi monter toutes les stats du lanceur.'),
('EN', 318, 'Silver Wind', 'The target is attacked with powdery scales blown by wind. This may also raise all the user’s stats.'),
('FR', 319, 'Strido-Son', 'Un cri horrible tel un crissement métallique qui réduit fortement la Défense Spéciale de l’ennemi.'),
('EN', 319, 'Metal Sound', 'A horrible sound like scraping metal harshly lowers the target’s Sp. Def stat.'),
('FR', 320, 'Siffl\'Herbe', 'Le lanceur joue une douce mélodie qui plonge l’ennemi dans un profond sommeil.'),
('EN', 320, 'Grass Whistle', 'The user plays a pleasant melody that lulls the target into a deep sleep.'),
('FR', 321, 'Chatouille', 'Le lanceur chatouille l’ennemi, ce qui baisse son Attaque et sa Défense.'),
('EN', 321, 'Tickle', 'The user tickles the target into laughing, reducing its Attack and Defense stats.'),
('FR', 322, 'Force Cosmik', 'Le lanceur absorbe un pouvoir mystique spatial qui augmente sa Défense et sa Défense Spéciale.'),
('EN', 322, 'Cosmic Power', 'The user absorbs a mystical power from space to raise its Defense and Sp. Def stats.'),
('FR', 323, 'Giclédo', 'Le lanceur attaque avec un jet d’eau. Moins il a de PV et moins l’attaque est puissante.'),
('EN', 323, 'Water Spout', 'The user spouts water to damage opposing Pokémon. The lower the user’s HP, the lower the move’s power.'),
('FR', 324, 'Rayon Signal', 'Le lanceur projette un rayon de lumière sinistre. Peut aussi rendre l’ennemi confus.'),
('EN', 324, 'Signal Beam', 'The user attacks with a sinister beam of light. This may also confuse the target.'),
('FR', 325, 'Poing Ombre', 'Le lanceur surgit des ombres et donne un coup de poing. N’échoue jamais.'),
('EN', 325, 'Shadow Punch', 'The user throws a punch from the shadows. This attack never misses.'),
('FR', 326, 'Extrasenseur', 'Le lanceur attaque avec un pouvoir étrange et invisible. Peut aussi apeurer l’ennemi.'),
('EN', 326, 'Extrasensory', 'The user attacks with an odd, unseeable power. This may also make the target flinch.'),
('FR', 327, 'Stratopercut', 'Le lanceur attaque avec un uppercut. Il envoie son poing vers le ciel de toutes ses forces.'),
('EN', 327, 'Sky Uppercut', 'The user attacks the target with an uppercut thrown skyward with force.'),
('FR', 328, 'Tourbi-Sable', 'Le lanceur emprisonne l’ennemi dans une tempête de sable terrifiante qui dure de quatre à cinq tours.'),
('EN', 328, 'Sand Tomb', 'The user traps the target inside a harshly raging sandstorm for four to five turns.'),
('FR', 329, 'Glaciation', 'Une vague de froid glacial frappe l’ennemi. S’il est touché, il est mis K.O. sur le coup.'),
('EN', 329, 'Sheer Cold', 'The target is attacked with a blast of absolute-zero cold. The target faints instantly if this attack hits.'),
('FR', 330, 'Ocroupi', 'Le lanceur attaque en projetant de l’eau boueuse. Peut aussi réduire la Précision de l’ennemi.'),
('EN', 330, 'Muddy Water', 'The user attacks by shooting muddy water at the opposing Pokémon. This may also lower their accuracy.'),
('FR', 331, 'Balle Graine', 'Le lanceur mitraille l’ennemi avec une rafale de graines. De deux à cinq rafales sont lancées à la suite.'),
('EN', 331, 'Bullet Seed', 'The user forcefully shoots seeds at the target two to five times in a row.'),
('FR', 332, 'Aéropique', 'Le lanceur prend l’ennemi de vitesse et le lacère. N’échoue jamais.'),
('EN', 332, 'Aerial Ace', 'The user confounds the target with speed, then slashes. This attack never misses.'),
('FR', 333, 'Stalagtite', 'Le lanceur jette des pics de glace sur l’ennemi, de deux à cinq fois de suite.'),
('EN', 333, 'Icicle Spear', 'The user launches sharp icicles at the target two to five times in a row.'),
('FR', 334, 'Mur de Fer', 'L’épiderme du lanceur devient dur comme du fer, ce qui augmente considérablement sa Défense.'),
('EN', 334, 'Iron Defense', 'The user hardens its body’s surface like iron, sharply raising its Defense stat.'),
('FR', 335, 'Barrage', 'Le lanceur bloque la route de l’ennemi pour empêcher sa fuite.'),
('EN', 335, 'Block', 'The user blocks the target’s way with arms spread wide to prevent escape.'),
('FR', 336, 'Grondement', 'Le lanceur hurle pour se rassurer, ce qui augmente son Attaque.'),
('EN', 336, 'Howl', 'The user howls loudly to raise its spirit, which raises its Attack stat.'),
('FR', 337, 'Dracogriffe', 'Le lanceur lacère l’ennemi de ses grandes griffes aiguisées.'),
('EN', 337, 'Dragon Claw', 'The user slashes the target with huge, sharp claws.'),
('FR', 338, 'Végé-Attak', 'Un violent coup de racines s’abat sur l’ennemi. Immobilise le lanceur au tour suivant.'),
('EN', 338, 'Frenzy Plant', 'The user slams the target with an enormous tree. The user can’t move on the next turn.'),
('FR', 339, 'Gonflette', 'Le lanceur tend ses muscles pour se gonfler, ce qui booste son Attaque et sa Défense.'),
('EN', 339, 'Bulk Up', 'The user tenses its muscles to bulk up its body, raising both its Attack and Defense stats.'),
('FR', 340, 'Rebond', 'Le lanceur bondit très haut et plonge sur l’ennemi au second tour. Peut aussi le paralyser.'),
('EN', 340, 'Bounce', 'The user bounces up high, then drops on the target on the second turn. This may also leave the target with paralysis.'),
('FR', 341, 'Tir de Boue', 'Le lanceur attaque en projetant de la boue sur l’ennemi. Réduit aussi la Vitesse de la cible.'),
('EN', 341, 'Mud Shot', 'The user attacks by hurling a blob of mud at the target. This also lowers the target’s Speed stat.'),
('FR', 342, 'Queue-Poison', 'Attaque à taux de critiques élevé. Peut aussi empoisonner l’ennemi.'),
('EN', 342, 'Poison Tail', 'The user hits the target with its tail. This may also poison the target. Critical hits land more easily.'),
('FR', 343, 'Implore', 'Le lanceur s’approche de la cible avec un air angélique afin de dérober l’objet qu’elle tient.'),
('EN', 343, 'Covet', 'The user endearingly approaches the target, then steals the target’s held item.'),
('FR', 344, 'Électacle', 'Le lanceur électrifie son corps avant de charger. Le choc blesse aussi gravement le lanceur et peut paralyser l’ennemi.'),
('EN', 344, 'Volt Tackle', 'The user electrifies itself, then charges. This also damages the user quite a lot. This may leave the target with paralysis.'),
('FR', 345, 'Feuille Magik', 'Le lanceur disperse d’étranges feuilles qui poursuivent l’ennemi. N’échoue jamais.'),
('EN', 345, 'Magical Leaf', 'The user scatters curious leaves that chase the target. This attack never misses.'),
('FR', 346, 'Tourniquet', 'Asperge d’eau les alentours. Affaiblit les capacités Feu pendant cinq tours.'),
('EN', 346, 'Water Sport', 'The user soaks itself with water. This weakens Fire-type moves for five turns.'),
('FR', 347, 'Plénitude', 'Le lanceur se concentre et fait le vide dans son esprit pour augmenter son Attaque Spéciale et sa Défense Spéciale.'),
('EN', 347, 'Calm Mind', 'The user quietly focuses its mind and calms its spirit to raise its Sp. Atk and Sp. Def stats.'),
('FR', 348, 'Lame-Feuille', 'Une feuille coupante comme une lame entaille l’ennemi. Taux de critiques élevé.'),
('EN', 348, 'Leaf Blade', 'The user handles a sharp leaf like a sword and attacks by cutting its target. Critical hits land more easily.'),
('FR', 349, 'Danse Draco', 'Une danse mystique dont le rythme effréné augmente l’Attaque et la Vitesse du lanceur.'),
('EN', 349, 'Dragon Dance', 'The user vigorously performs a mystic, powerful dance that raises its Attack and Speed stats.'),
('FR', 350, 'Boule Roc', 'Le lanceur projette un rocher sur l’ennemi de deux à cinq fois d’affilée.'),
('EN', 350, 'Rock Blast', 'The user hurls hard rocks at the target. Two to five rocks are launched in a row.'),
('FR', 351, 'Onde de Choc', 'Le lanceur envoie un choc électrique rapide à l’ennemi. Impossible à esquiver.'),
('EN', 351, 'Shock Wave', 'The user strikes the target with a quick jolt of electricity. This attack never misses.'),
('FR', 352, 'Vibraqua', 'Le lanceur envoie un puissant jet d’eau sur l’ennemi. Peut le rendre confus.'),
('EN', 352, 'Water Pulse', 'The user attacks the target with a pulsing blast of water. This may also confuse the target.'),
('FR', 353, 'Carnareket', 'Le lanceur génère une sphère lumineuse qu’il projette sur l’ennemi deux tours plus tard.'),
('EN', 353, 'Doom Desire', 'Two turns after this move is used, the user blasts the target with a concentrated bundle of light.'),
('FR', 354, 'Psycho Boost', 'Attaque l’ennemi à pleine puissance. Le contrecoup baisse fortement l’Attaque Spéciale du lanceur.'),
('EN', 354, 'Psycho Boost', 'The user attacks the target at full power. The attack’s recoil harshly lowers the user’s Sp. Atk stat.'),
('FR', 355, 'Atterrissage', 'Le lanceur atterrit et se repose. Restaure jusqu’à la moitié de ses PV max.'),
('EN', 355, 'Roost', 'The user lands and rests its body. It restores the user’s HP by up to half of its max HP.'),
('FR', 356, 'Gravité', 'La gravité augmente pendant cinq tours, ce qui empêche les capacités volantes et annule Lévitation.'),
('EN', 356, 'Gravity', 'Gravity is intensified for five turns, making moves involving flying unusable and negating Levitate.'),
('FR', 357, 'Œil Miracle', 'Permet de toucher un Pokémon Ténèbres avec les capacités de type Psy ou de toucher un ennemi  ayant beaucoup d’esquive.'),
('EN', 357, 'Miracle Eye', 'Enables a Dark-type target to be hit by Psychic-type attacks. This also enables an evasive target to be hit.'),
('FR', 358, 'Réveil Forcé', 'Cette attaque inflige d’importants dégâts à un Pokémon endormi. Mais elle le réveille également.'),
('EN', 358, 'Wake-Up Slap', 'This attack inflicts big damage on a sleeping target. This also wakes the target up, however.'),
('FR', 359, 'Marto-Poing', 'Le lanceur lâche un puissant coup de poing sur l’ennemi. Réduit la vitesse du lanceur.'),
('EN', 359, 'Hammer Arm', 'The user swings and hits with its strong and heavy fist. It lowers the user’s Speed, however.'),
('FR', 360, 'Gyroballe', 'Le lanceur effectue une rotation et frappe l’ennemi. Plus la Vitesse du lanceur est basse, plus il fait de dégâts.'),
('EN', 360, 'Gyro Ball', 'The user tackles the target with a high-speed spin. The slower the user compared to the target, the greater the move’s power.'),
('FR', 361, 'Vœu Soin', 'Le lanceur tombe K.O. pour soigner les PV et le statut du Pokémon qui passe après lui.'),
('EN', 361, 'Healing Wish', 'The user faints. In return, the Pokémon taking its place will have its HP restored and status conditions cured.'),
('FR', 362, 'Saumure', 'Cette attaque est deux fois plus puissante lorsque l’ennemi a moins de la moitié de ses PV.'),
('EN', 362, 'Brine', 'If the target’s HP is half or less, this attack will hit with double the power.'),
('FR', 363, 'Don Naturel', 'Avant d’attaquer, le lanceur rassemble ses forces grâce à sa Baie. Elle détermine le type et la puissance de l’attaque.'),
('EN', 363, 'Natural Gift', 'The user draws power to attack by using its held Berry. The Berry determines the move’s type and power.'),
('FR', 364, 'Ruse', 'Une attaque capable de toucher un ennemi qui utilise Détection ou Abri. Annule aussi leur effet.'),
('EN', 364, 'Feint', 'An attack that hits a target using Protect or Detect. This also lifts the effects of those moves.'),
('FR', 365, 'Picore', 'Le lanceur picore la cible. Si cette dernière tient une Baie, le lanceur la mange  et profite de ses effets.'),
('EN', 365, 'Pluck', 'The user pecks the target. If the target is holding a Berry, the user eats it and gains its effect.'),
('FR', 366, 'Vent Arrière', 'Génère une rafale de vent qui augmente la Vitesse des Pokémon de l’équipe pendant 4 tours.'),
('EN', 366, 'Tailwind', 'The user whips up a turbulent whirlwind that ups the Speed stat of the user and its allies for four turns.'),
('FR', 367, 'Acupression', 'Le lanceur utilise sa connaissance des points de pression pour augmenter fortement une stat.'),
('EN', 367, 'Acupressure', 'The user applies pressure to stress points, sharply boosting one of its or its allies’ stats.'),
('FR', 368, 'Fulmifer', 'Le lanceur attaque le dernier ennemi l’ayant blessé durant le même tour en frappant plus fort.'),
('EN', 368, 'Metal Burst', 'The user retaliates with much greater power against the opponent that last inflicted damage on it.'),
('FR', 369, 'Demi-Tour', 'Après son attaque, le lanceur revient à toute vitesse et change de place avec un Pokémon de l’équipe prêt au combat.'),
('EN', 369, 'U-turn', 'After making its attack, the user rushes back to switch places with a party Pokémon in waiting.'),
('FR', 370, 'Close Combat', 'Le lanceur combat au corps à corps sans se protéger. Baisse aussi sa Défense et sa Défense Spéciale.'),
('EN', 370, 'Close Combat', 'The user fights the target up close without guarding itself. This also lowers the user’s Defense and Sp. Def stats.'),
('FR', 371, 'Représailles', 'Le lanceur charge son énergie, puis attaque. La puissance est doublée si le lanceur agit après l’ennemi.'),
('EN', 371, 'Payback', 'The user stores power, then attacks. If the user moves after the target, this attack’s power will be doubled.'),
('FR', 372, 'Assurance', 'Cette attaque est deux fois plus efficace si l’ennemi a déjà été blessé durant ce tour.'),
('EN', 372, 'Assurance', 'If the target has already taken some damage in the same turn, this attack’s power is doubled.'),
('FR', 373, 'Embargo', 'Empêche l’ennemi d’utiliser un objet tenu et son Dresseur d’utiliser un objet sur lui.'),
('EN', 373, 'Embargo', 'This move prevents the target from using its held item. Its Trainer is also prevented from using items on it.'),
('FR', 374, 'Dégommage', 'Le lanceur envoie l’objet qu’il tient sur l’ennemi. La puissance et les effets dépendent de l’objet.'),
('EN', 374, 'Fling', 'The user flings its held item at the target to attack. This move’s power and effects depend on the item.'),
('FR', 375, 'Échange Psy', 'Le lanceur transfère ses problèmes de statut à l’ennemi grâce à son pouvoir de suggestion.'),
('EN', 375, 'Psycho Shift', 'Using its psychic power of suggestion, the user transfers its status conditions to the target.'),
('FR', 376, 'Atout', 'Moins cette capacité possède de PP, plus elle est puissante.'),
('EN', 376, 'Trump Card', 'The fewer PP this move has, the greater its power.'),
('FR', 377, 'Anti-Soin', 'Le lanceur empêche l’ennemi de récupérer des PV à l’aide de capacités, talents ou objets tenus, pendant cinq tours.'),
('EN', 377, 'Heal Block', 'For five turns, the user prevents the opposing team from using any moves, Abilities, or held items that recover HP.'),
('FR', 378, 'Essorage', 'Le lanceur essore l’ennemi. Plus l’ennemi a de PV, plus cette attaque est puissante.'),
('EN', 378, 'Wring Out', 'The user powerfully wrings the target. The more HP the target has, the greater the move’s power.'),
('FR', 379, 'Astuce Force', 'Le lanceur utilise ses pouvoirs psychiques pour échanger sa Défense et son Attaque.'),
('EN', 379, 'Power Trick', 'The user employs its psychic power to switch its Attack with its Defense stat.'),
('FR', 380, 'Suc Digestif', 'Le lanceur répand ses sucs digestifs sur l’ennemi. Le fluide neutralise le talent de l’ennemi.'),
('EN', 380, 'Gastro Acid', 'The user hurls up its stomach acids on the target. The fluid eliminates the effect of the target’s Ability.'),
('FR', 381, 'Air Veinard', 'Le lanceur envoie une incantation vers le ciel et protège l’équipe des coups critiques.'),
('EN', 381, 'Lucky Chant', 'The user chants an incantation toward the sky, preventing opposing Pokémon from landing critical hits.'),
('FR', 382, 'Moi d\'Abord', 'Le lanceur vole la capacité prévue par l’ennemi et l’utilise en faisant plus de dégâts. Il doit frapper en premier.'),
('EN', 382, 'Me First', 'The user cuts ahead of the target to steal and use the target’s intended move with greater power. This move fails if it isn’t used first.'),
('FR', 383, 'Photocopie', 'Le lanceur imite la dernière capacité employée. Échoue si aucune capacité n’a été utilisée.'),
('EN', 383, 'Copycat', 'The user mimics the move used immediately before it. The move fails if no other move has been used yet.'),
('FR', 384, 'Permuforce', 'Pouvoir qui échange les modifications de l’Attaque Spéciale et de l’Attaque du lanceur avec la cible.'),
('EN', 384, 'Power Swap', 'The user employs its psychic power to switch changes to its Attack and Sp. Atk stats with the target.'),
('FR', 385, 'Permugarde', 'Pouvoir qui échange les modifications de la Défense Spéciale et de la Défense avec la cible.'),
('EN', 385, 'Guard Swap', 'The user employs its psychic power to switch changes to its Defense and Sp. Def stats with the target.'),
('FR', 386, 'Punition', 'Plus l’ennemi a utilisé d’augmentations de stats et plus cette capacité est puissante.'),
('EN', 386, 'Punishment', 'The more the target has powered up with stat changes, the greater the move’s power.'),
('FR', 387, 'Dernier Recours', 'Cette capacité ne peut être utilisée qu’après que le lanceur a utilisé toutes les autres.'),
('EN', 387, 'Last Resort', 'This move can be used only after the user has used all the other moves it knows in the battle.'),
('FR', 388, 'Soucigraine', 'Plante sur la cible une graine qui la rend soucieuse et remplace son talent par Insomnia, l’empêchant ainsi de dormir.'),
('EN', 388, 'Worry Seed', 'A seed that causes worry is planted on the target. It prevents sleep by making the target’s Ability Insomnia.'),
('FR', 389, 'Coup Bas', 'Permet au lanceur de frapper en premier. Échoue si l’ennemi ne prépare pas une attaque.'),
('EN', 389, 'Sucker Punch', 'This move enables the user to attack first. This move fails if the target is not readying an attack.'),
('FR', 390, 'Pics Toxik', 'Lance des pics autour de l’ennemi. Ils empoisonnent les ennemis qui entrent au combat.'),
('EN', 390, 'Toxic Spikes', 'The user lays a trap of poison spikes at the opposing team’s feet. They poison opposing Pokémon that switch into battle.'),
('FR', 391, 'Permucœur', 'Le lanceur utilise un pouvoir psychique pour échanger ses changements de stats avec la cible.'),
('EN', 391, 'Heart Swap', 'The user employs its psychic power to switch stat changes with the target.'),
('FR', 392, 'Anneau Hydro', 'Un voile liquide enveloppe le lanceur. Il récupère des PV à chaque tour.'),
('EN', 392, 'Aqua Ring', 'The user envelops itself in a veil made of water. It regains some HP every turn.'),
('FR', 393, 'Vol Magnétik', 'Le lanceur utilise l’électricité pour générer un champ magnétique et léviter durant cinq tours.'),
('EN', 393, 'Magnet Rise', 'The user levitates using electrically generated magnetism for five turns.'),
('FR', 394, 'Boutefeu', 'Le lanceur s’embrase avant de charger l’ennemi. Le choc blesse aussi gravement le lanceur. Peut brûler l’ennemi.'),
('EN', 394, 'Flare Blitz', 'The user cloaks itself in fire and charges the target. This also damages the user quite a lot. This may leave the target with a burn.'),
('FR', 395, 'Forte-Paume', 'Une onde de choc frappe l’ennemi. Peut aussi paralyser la cible.'),
('EN', 395, 'Force Palm', 'The target is attacked with a shock wave. This may also leave the target with paralysis.'),
('FR', 396, 'Aurasphère', 'Le lanceur dégage une aura et projette de l’énergie. N’échoue jamais.'),
('EN', 396, 'Aura Sphere', 'The user lets loose a blast of aura power from deep within its body at the target. This attack never misses.'),
('FR', 397, 'Poliroche', 'Le lanceur polit son corps pour diminuer sa résistance au vent. Augmente fortement la Vitesse.'),
('EN', 397, 'Rock Polish', 'The user polishes its body to reduce drag. This can sharply raise the Speed stat.'),
('FR', 398, 'Direct Toxik', 'Attaque l’ennemi avec un tentacule ou un bras plein de poison. Peut aussi l’empoisonner.'),
('EN', 398, 'Poison Jab', 'The target is stabbed with a tentacle or arm steeped in poison. This may also poison the target.'),
('FR', 399, 'Vibrobscur', 'Le lanceur dégage une horrible aura chargée d’idées noires. Peut aussi apeurer l’ennemi.'),
('EN', 399, 'Dark Pulse', 'The user releases a horrible aura imbued with dark thoughts. This may also make the target flinch.'),
('FR', 400, 'Tranche-Nuit', 'Le lanceur lacère l’ennemi à la première occasion. Taux de critiques élevé.'),
('EN', 400, 'Night Slash', 'The user slashes the target the instant an opportunity arises. Critical hits land more easily.'),
('FR', 401, 'Hydroqueue', 'Le lanceur attaque en balançant sa queue comme une lame de fond en pleine tempête.'),
('EN', 401, 'Aqua Tail', 'The user attacks by swinging its tail as if it were a vicious wave in a raging storm.'),
('FR', 402, 'Canon Graine', 'Le lanceur fait pleuvoir un déluge de graines explosives sur l’ennemi.'),
('EN', 402, 'Seed Bomb', 'The user slams a barrage of hard-shelled seeds down on the target from above.'),
('FR', 403, 'Lame d\'Air', 'Le lanceur attaque avec une lame d’air qui fend tout. Peut aussi apeurer l’ennemi.'),
('EN', 403, 'Air Slash', 'The user attacks with a blade of air that slices even the sky. This may also make the target flinch.'),
('FR', 404, 'Plaie-Croix', 'Le lanceur taillade l’ennemi en utilisant ses faux ou ses griffes comme une paire de ciseaux.'),
('EN', 404, 'X-Scissor', 'The user slashes at the target by crossing its scythes or claws as if they were a pair of scissors.'),
('FR', 405, 'Bourdon', 'Le lanceur fait vibrer son corps pour lancer une vague sonique. Peut aussi baisser la Défense Spéciale de l’ennemi.'),
('EN', 405, 'Bug Buzz', 'The user vibrates its wings to generate a damaging sound wave. This may also lower the target’s Sp. Def stat.'),
('FR', 406, 'Dracochoc', 'Le lanceur ouvre la bouche pour envoyer une onde de choc qui frappe l’ennemi.'),
('EN', 406, 'Dragon Pulse', 'The target is attacked with a shock wave generated by the user’s gaping mouth.'),
('FR', 407, 'Dracocharge', 'Le lanceur frappe l’ennemi en prenant un air menaçant. Peut aussi l’apeurer.'),
('EN', 407, 'Dragon Rush', 'The user tackles the target while exhibiting overwhelming menace. This may also make the target flinch.'),
('FR', 408, 'Rayon Gemme', 'Le lanceur attaque avec un rayon de lumière qui scintille comme s’il était composé de gemmes.'),
('EN', 408, 'Power Gem', 'The user attacks with a ray of light that sparkles as if it were made of gemstones.'),
('FR', 409, 'Vampipoing', 'Un coup de poing qui draine l’énergie. Convertit la moitié des dégâts infligés en PV pour le lanceur.'),
('EN', 409, 'Drain Punch', 'An energy-draining punch. The user’s HP is restored by half the damage taken by the target.'),
('FR', 410, 'Onde Vide', 'Le lanceur agite son poing pour projeter une onde de vide. Frappe toujours en premier.'),
('EN', 410, 'Vacuum Wave', 'The user whirls its fists to send a wave of pure vacuum at the target. This move always goes first.'),
('FR', 411, 'Exploforce', 'Le lanceur rassemble ses forces et laisse éclater son pouvoir. Peut aussi baisser la Défense Spéciale  de l’ennemi.'),
('EN', 411, 'Focus Blast', 'The user heightens its mental focus and unleashes its power. This may also lower the target’s Sp. Def.'),
('FR', 412, 'Éco-Sphère', 'Utilise les pouvoirs de la nature pour attaquer l’ennemi. Peut aussi baisser sa Défense Spéciale.'),
('EN', 412, 'Energy Ball', 'The user draws power from nature and fires it at the target. This may also lower the target’s Sp. Def.'),
('FR', 413, 'Rapace', 'Le lanceur replie ses ailes et charge en rase-mottes. Blesse gravement le lanceur.'),
('EN', 413, 'Brave Bird', 'The user tucks in its wings and charges from a low altitude. This also damages the user quite a lot.'),
('FR', 414, 'Telluriforce', 'Des éruptions volcaniques ont lieu sous l’ennemi. Peut aussi baisser sa Défense Spéciale.'),
('EN', 414, 'Earth Power', 'The user makes the ground under the target erupt with power. This may also lower the target’s Sp. Def.'),
('FR', 415, 'Passe-Passe', 'Le lanceur échange son objet avec celui de la cible à une vitesse que l’œil a du mal à suivre.'),
('EN', 415, 'Switcheroo', 'The user trades held items with the target faster than the eye can follow.'),
('FR', 416, 'Giga Impact', 'Le lanceur charge l’ennemi de toute sa puissance et doit ensuite se reposer au tour suivant.'),
('EN', 416, 'Giga Impact', 'The user charges at the target using every bit of its power. The user can’t move on the next turn.'),
('FR', 417, 'Machination', 'Stimule l’esprit par de mauvaises pensées. Augmente fortement l’Attaque Spéciale du lanceur.'),
('EN', 417, 'Nasty Plot', 'The user stimulates its brain by thinking bad thoughts. This sharply raises the user’s Sp. Atk.'),
('FR', 418, 'Pisto-Poing', 'Le lanceur envoie des coups de poing aussi rapides que des balles de revolver. Frappe toujours en premier.'),
('EN', 418, 'Bullet Punch', 'The user strikes the target with tough punches as fast as bullets. This move always goes first.'),
('FR', 419, 'Avalanche', 'Une attaque deux fois plus puissante si le lanceur a été blessé par l’ennemi durant le tour.'),
('EN', 419, 'Avalanche', 'An attack move that inflicts double the damage if the user has been hurt by the target in the same turn.'),
('FR', 420, 'Éclats Glace', 'Le lanceur crée des éclats de glace qu’il envoie sur l’ennemi. Frappe toujours en premier.'),
('EN', 420, 'Ice Shard', 'The user flash-freezes chunks of ice and hurls them at the target. This move always goes first.'),
('FR', 421, 'Griffe Ombre', 'Attaque avec une griffe puissante faite d’ombres. Taux de critiques élevé.'),
('EN', 421, 'Shadow Claw', 'The user slashes with a sharp claw made from shadows. Critical hits land more easily.'),
('FR', 422, 'Crocs Éclair', 'Le lanceur utilise une morsure électrifiée. Peut aussi paralyser ou apeurer l’ennemi.'),
('EN', 422, 'Thunder Fang', 'The user bites with electrified fangs. This may also make the target flinch or leave it with paralysis.'),
('FR', 423, 'Crocs Givre', 'Le lanceur utilise une morsure glaciale. Peut aussi geler ou apeurer l’ennemi.'),
('EN', 423, 'Ice Fang', 'The user bites with cold-infused fangs. This may also make the target flinch or leave it frozen.'),
('FR', 424, 'Crocs Feu', 'Le lanceur utilise une morsure enflammée. Peut aussi brûler ou apeurer l’ennemi.'),
('EN', 424, 'Fire Fang', 'The user bites with flame-cloaked fangs. This may also make the target flinch or leave it with a burn.'),
('FR', 425, 'Ombre Portée', 'Le lanceur étend son ombre pour frapper par-derrière. Frappe toujours en premier.'),
('EN', 425, 'Shadow Sneak', 'The user extends its shadow and attacks the target from behind. This move always goes first.'),
('FR', 426, 'Boue-Bombe', 'Le lanceur attaque à l’aide d’une boule de boue solidifiée. Peut aussi baisser la Précision de l’ennemi.'),
('EN', 426, 'Mud Bomb', 'The user launches a hard-packed mud ball to attack. This may also lower the target’s accuracy.'),
('FR', 427, 'Coupe Psycho', 'Le lanceur entaille l’ennemi grâce à des lames faites de pouvoir psychique. Taux de critiques élevé.'),
('EN', 427, 'Psycho Cut', 'The user tears at the target with blades formed by psychic power. Critical hits land more easily.'),
('FR', 428, 'Psykoud\'Boul', 'Le lanceur concentre sa volonté et donne un coup de tête. Peut aussi apeurer l’ennemi.'),
('EN', 428, 'Zen Headbutt', 'The user focuses its willpower to its head and attacks the target. This may also make the target flinch.'),
('FR', 429, 'Miroi-Tir', 'Le corps poli du lanceur libère un éclair d’énergie. Peut aussi baisser la Précision de l’ennemi.'),
('EN', 429, 'Mirror Shot', 'The user lets loose a flash of energy at the target from its polished body. This may also lower the target’s accuracy.'),
('FR', 430, 'Luminocanon', 'Le lanceur concentre son énergie lumineuse et la fait exploser. Peut aussi baisser la Défense Spéciale de l’ennemi.'),
('EN', 430, 'Flash Cannon', 'The user gathers all its light energy and releases it at once. This may also lower the target’s Sp. Def stat.'),
('FR', 431, 'Escalade', 'Le lanceur se jette violemment sur l’ennemi. Peut aussi le rendre confus.'),
('EN', 431, 'Rock Climb', 'The user attacks the target by smashing into it with incredible force. This may also confuse the target.'),
('FR', 432, 'Anti-Brume', 'Un grand coup de vent qui disperse la Protection ou le Mur Lumière de l’ennemi. Diminue aussi son Esquive.'),
('EN', 432, 'Defog', 'A strong wind blows away the target’s barriers such as Reflect or Light Screen. This also lowers the target’s evasiveness.'),
('FR', 433, 'Distorsion', 'Le lanceur crée une zone étrange où les Pokémon les plus lents frappent en premier pendant cinq tours.'),
('EN', 433, 'Trick Room', 'The user creates a bizarre area in which slower Pokémon get to move first for five turns.'),
('FR', 434, 'Draco Météor', 'Le lanceur invoque des comètes. Le contrecoup réduit fortement son Attaque Spéciale.'),
('EN', 434, 'Draco Meteor', 'Comets are summoned down from the sky onto the target. The attack’s recoil harshly lowers the user’s Sp. Atk stat.'),
('FR', 435, 'Coup d\'Jus', 'Un flamboiement d’électricité frappe tous les Pokémon autour du lanceur. Peut aussi les paralyser. '),
('EN', 435, 'Discharge', 'The user strikes everything around it by letting loose a flare of electricity. This may also cause paralysis.'),
('FR', 436, 'Ébullilave', 'Des boules de feu s’abattent sur tous les Pokémon  autour du lanceur. Peut aussi les brûler.'),
('EN', 436, 'Lava Plume', 'The user torches everything around it with an inferno of scarlet flames. This may also leave those hit with a burn.'),
('FR', 437, 'Tempête Verte', 'Invoque une tempête de feuilles acérées. Le contrecoup réduit fortement l’Attaque Spéciale du lanceur.'),
('EN', 437, 'Leaf Storm', 'The user whips up a storm of leaves around the target. The attack’s recoil harshly lowers the user’s Sp. Atk stat.'),
('FR', 438, 'Mégafouet', 'Le lanceur fait virevolter violemment ses lianes ou ses tentacules pour fouetter l’ennemi.'),
('EN', 438, 'Power Whip', 'The user violently whirls its vines or tentacles to harshly lash the target.'),
('FR', 439, 'Roc-Boulet', 'Le lanceur attaque en projetant un gros rocher sur l’ennemi. Il doit se reposer au tour suivant.'),
('EN', 439, 'Rock Wrecker', 'The user launches a huge boulder at the target to attack. The user can’t move on the next turn.'),
('FR', 440, 'Poison-Croix', 'Un coup tranchant qui peut empoisonner l’ennemi. Taux de critiques élevé.'),
('EN', 440, 'Cross Poison', 'A slashing attack with a poisonous blade that may also poison the target. Critical hits land more easily.'),
('FR', 441, 'Détricanon', 'Le lanceur envoie des détritus sur l’ennemi. Peut aussi l’empoisonner.'),
('EN', 441, 'Gunk Shot', 'The user shoots filthy garbage at the target to attack. This may also poison the target.'),
('FR', 442, 'Tête de Fer', 'Le lanceur heurte l’ennemi avec sa tête dure comme de l’acier. Peut aussi l’apeurer.'),
('EN', 442, 'Iron Head', 'The user slams the target with its steel-hard head. This may also make the target flinch.'),
('FR', 443, 'Bombaimant', 'Le lanceur projette des bombes d’acier qui collent à l’ennemi. N’échoue jamais.'),
('EN', 443, 'Magnet Bomb', 'The user launches steel bombs that stick to the target. This attack never misses.'),
('FR', 444, 'Lame de Roc', 'Fait surgir des pierres aiguisées sous l’ennemi. Taux de critiques élevé.'),
('EN', 444, 'Stone Edge', 'The user stabs the target with sharpened stones from below. Critical hits land more easily.'),
('FR', 445, 'Séduction', 'Si l’ennemi est de sexe opposé au lanceur, il est séduit et son Attaque Spéciale baisse fortement.'),
('EN', 445, 'Captivate', 'If any opposing Pokémon is the opposite gender of the user, it is charmed, which harshly lowers its Sp. Atk stat.'),
('FR', 446, 'Piège de Roc', 'Lance des pierres flottantes autour de l’ennemi, qui blessent tout adversaire entrant au combat.'),
('EN', 446, 'Stealth Rock', 'The user lays a trap of levitating stones around the opposing team. The trap hurts opposing Pokémon that switch into battle.'),
('FR', 447, 'Nœud Herbe', 'L’ennemi est piégé dans de l’herbe qui le fait trébucher. Plus il est lourd, plus il subit de dégâts.'),
('EN', 447, 'Grass Knot', 'The user snares the target with grass and trips it. The heavier the target, the greater the move’s power.'),
('FR', 448, 'Babil', 'Attaque avec les ondes sonores assourdissantes qu’il émet en prononçant des mots au hasard. Rend l’ennemi confus.'),
('EN', 448, 'Chatter', 'The user attacks the target with sound waves of deafening chatter. This confuses the target.'),
('FR', 449, 'Jugement', 'Le lanceur libère une myriade de rayons de lumière. Le type varie selon la Plaque que tient le lanceur.'),
('EN', 449, 'Judgment', 'The user releases countless shots of light at the target. This move’s type varies depending on the kind of Plate the user is holding.'),
('FR', 450, 'Piqûre', 'Le lanceur pique l’ennemi. Si ce dernier tient une Baie, le lanceur la dévore et obtient son effet.'),
('EN', 450, 'Bug Bite', 'The user bites the target. If the target is holding a Berry, the user eats it and gains its effect.'),
('FR', 451, 'Rayon Chargé', 'Le lanceur tire un rayon chargé d’électricité. Peut aussi augmenter son Attaque Spéciale.'),
('EN', 451, 'Charge Beam', 'The user attacks with an electric charge. The user may use any remaining electricity to raise its Sp. Atk stat.'),
('FR', 452, 'Martobois', 'Le lanceur heurte l’ennemi de son corps robuste. Blesse aussi gravement le lanceur.'),
('EN', 452, 'Wood Hammer', 'The user slams its rugged body into the target to attack. This also damages the user quite a lot.'),
('FR', 453, 'Aqua-Jet', 'Le lanceur fonce sur l’ennemi si rapidement qu’on parvient à peine à le discerner. Frappe toujours en premier.'),
('EN', 453, 'Aqua Jet', 'The user lunges at the target at a speed that makes it almost invisible. This move always goes first.'),
('FR', 454, 'Appel Attak', 'Le lanceur appelle ses sous-fifres pour frapper l’ennemi. Taux de critiques élevé.'),
('EN', 454, 'Attack Order', 'The user calls out its underlings to pummel the target. Critical hits land more easily.'),
('FR', 455, 'Appel Défense', 'Le lanceur appelle ses sous-fifres pour former un bouclier qui augmente sa Défense et sa Défense Spéciale.'),
('EN', 455, 'Defend Order', 'The user calls out its underlings to shield its body, raising its Defense and Sp. Def stats.'),
('FR', 456, 'Appel Soins', 'Le lanceur appelle ses sous-fifres pour le soigner. Il récupère jusqu’à la moitié de ses PV max.'),
('EN', 456, 'Heal Order', 'The user calls out its underlings to heal it. The user regains up to half of its max HP.'),
('FR', 457, 'Fracass\'Tête', 'Le lanceur assène un coup de tête désespéré. Blesse aussi gravement le lanceur.'),
('EN', 457, 'Head Smash', 'The user attacks the target with a hazardous, full-power headbutt. This also damages the user terribly.'),
('FR', 458, 'Coup Double', 'Un coup de queue ou de liane qui frappe l’ennemi deux fois d’affilée.'),
('EN', 458, 'Double Hit', 'The user slams the target with a long tail, vines, or a tentacle. The target is hit twice in a row.'),
('FR', 459, 'Hurle-Temps', 'Le lanceur frappe si fort qu’il affecte le cours du temps. Il se repose au tour suivant.'),
('EN', 459, 'Roar of Time', 'The user blasts the target with power that distorts even time. The user can’t move on the next turn.'),
('FR', 460, 'Spatio-Rift', 'Le lanceur déchire l’ennemi et l’espace autour de lui. Taux de critiques élevé.'),
('EN', 460, 'Spacial Rend', 'The user tears the target along with the space around it. Critical hits land more easily.'),
('FR', 461, 'Danse-Lune', 'Le lanceur tombe K.O. pour soigner le statut et les PV du Pokémon qui prendra sa place au combat.'),
('EN', 461, 'Lunar Dance', 'The user faints. In return, the Pokémon taking its place will have its status and HP fully restored.'),
('FR', 462, 'Presse', 'Une force puissante écrase l’ennemi. Plus il lui reste de PV et plus l’attaque est puissante.'),
('EN', 462, 'Crush Grip', 'The target is crushed with great force. The more HP the target has left, the greater this move’s power.'),
('FR', 463, 'Vortex Magma', 'L’ennemi est pris dans un tourbillon de feu qui dure de quatre à cinq tours.'),
('EN', 463, 'Magma Storm', 'The target becomes trapped within a maelstrom of fire that rages for four to five turns.'),
('FR', 464, 'Trou Noir', 'L’ennemi est plongé dans les ténèbres. Il tombe dans un profond sommeil.'),
('EN', 464, 'Dark Void', 'Opposing Pokémon are dragged into a world of total darkness that makes them sleep.'),
('FR', 465, 'Fulmigraine', 'Le corps du lanceur émet une onde de choc. Peut aussi baisser fortement la Défense Spéciale de la cible.'),
('EN', 465, 'Seed Flare', 'The user emits a shock wave from its body to attack its target. This may also harshly lower the target’s Sp. Def.'),
('FR', 466, 'Vent Mauvais', 'Le lanceur crée une violente bourrasque. Peut aussi augmenter toutes ses stats.'),
('EN', 466, 'Ominous Wind', 'The user blasts the target with a gust of repulsive wind. This may also raise all the user’s stats at once.'),
('FR', 467, 'Revenant', 'Le lanceur disparaît et frappe l’ennemi au second tour. Fonctionne même si l’ennemi se protège.'),
('EN', 467, 'Shadow Force', 'The user disappears, then strikes the target on the next turn. This move hits even if the target protects itself.'),
('FR', 468, 'Aiguisage', 'Le lanceur s’aiguise les griffes. Augmente l’Attaque et la Précision.'),
('EN', 468, 'Hone Claws', 'The user sharpens its claws to boost its Attack stat and accuracy.'),
('FR', 469, 'Garde Large', 'Annule les attaques visant toute l’équipe pendant un tour.'),
('EN', 469, 'Wide Guard', 'The user and its allies are protected from wide-ranging attacks for one turn.'),
('FR', 470, 'Partage Garde', 'Additionne la Défense et la Défense Spéciale du lanceur et de sa cible et les redistribue équitablement entre les deux.'),
('EN', 470, 'Guard Split', 'The user employs its psychic power to average its Defense and Sp. Def stats with those of the target.'),
('FR', 471, 'Partage Force', 'Additionne l’Attaque Spéciale et l’Attaque du lanceur et de sa cible et les redistribue équitablement entre les deux.'),
('EN', 471, 'Power Split', 'The user employs its psychic power to average its Attack and Sp. Atk stats with those of the target.'),
('FR', 472, 'Zone Étrange', 'Crée une zone étrange où pendant cinq tours, la Défense et la Défense Spéciale de tous les Pokémon sont inversées.'),
('EN', 472, 'Wonder Room', 'The user creates a bizarre area in which Pokémon’s Defense and Sp. Def stats are swapped for five turns.'),
('FR', 473, 'Choc Psy', 'Le lanceur matérialise des ondes mystérieuses qu’il projette sur l’ennemi. Inflige des dégâts physiques.'),
('EN', 473, 'Psyshock', 'The user materializes an odd psychic wave to attack the target. This attack does physical damage.'),
('FR', 474, 'Choc Venin', 'Le lanceur inocule un poison spécial à l’ennemi. L’effet est doublé si l’ennemi est déjà empoisonné. '),
('EN', 474, 'Venoshock', 'The user drenches the target in a special poisonous liquid. This move’s power is doubled if the target is poisoned.'),
('FR', 475, 'Allègement', 'Le lanceur se débarrasse des parties inutiles de son corps. Son poids diminue et sa Vitesse augmente fortement.'),
('EN', 475, 'Autotomize', 'The user sheds part of its body to make itself lighter and sharply raise its Speed stat.'),
('FR', 476, 'Poudre Fureur', 'Le lanceur s’asperge d’une poudre irritante pour attirer l’attention. Il attire toutes les attaques ennemies.'),
('EN', 476, 'Rage Powder', 'The user scatters a cloud of irritating powder to draw attention to itself. Opponents aim only at the user.'),
('FR', 477, 'Lévikinésie', 'Un pouvoir qui fait flotter l’ennemi dans les airs. Pendant trois tours, il devient plus facile à atteindre.'),
('EN', 477, 'Telekinesis', 'The user makes the target float with its psychic power. The target is easier to hit for three turns.'),
('FR', 478, 'Zone Magique', 'Le lanceur crée une zone étrange. Pendant cinq tours, les objets tenus par tous les Pokémon n’ont plus aucun effet.'),
('EN', 478, 'Magic Room', 'The user creates a bizarre area in which Pokémon’s held items lose their effects for five turns.'),
('FR', 479, 'Anti-Air', 'Le lanceur jette toutes sortes de projectiles à un ennemi. Si ce dernier vole, il tombe au sol.'),
('EN', 479, 'Smack Down', 'The user throws a stone or similar projectile to attack an opponent. A flying Pokémon will fall to the ground when it’s hit.'),
('FR', 480, 'Yama Arashi', 'Un coup très puissant dont l’effet est toujours critique.'),
('EN', 480, 'Storm Throw', 'The user strikes the target with a fierce blow. This attack always results in a critical hit.'),
('FR', 481, 'Rebondifeu', 'Quand l’attaque atteint sa cible, elle projette des flammes qui touchent tout ennemi situé à côté.'),
('EN', 481, 'Flame Burst', 'The user attacks the target with a bursting flame. The bursting flame damages Pokémon next to the target as well.'),
('FR', 482, 'Cradovague', 'Une vague de détritus attaque tous les Pokémon autour du lanceur. Peut aussi empoisonner.'),
('EN', 482, 'Sludge Wave', 'The user strikes everything around it by swamping the area with a giant sludge wave. This may also poison those hit.'),
('FR', 483, 'Papillodanse', 'Une danse mystique dont le rythme parfait augmente l’Attaque Spéciale, la Défense Spéciale et la Vitesse du lanceur.'),
('EN', 483, 'Quiver Dance', 'The user lightly performs a beautiful, mystic dance. This boosts the user’s Sp. Atk, Sp. Def, and Speed stats.'),
('FR', 484, 'Tacle Lourd', 'Le lanceur se jette sur l’ennemi de tout son poids. S’il est plus lourd que l’ennemi, l’effet augmente en conséquence.'),
('EN', 484, 'Heavy Slam', 'The user slams into the target with its heavy body. The more the user outweighs the target, the greater the move’s power.'),
('FR', 485, 'Synchropeine', 'Des ondes mystérieuses blessent tous les Pokémon alentour qui sont du même type que le lanceur.'),
('EN', 485, 'Synchronoise', 'Using an odd shock wave, the user inflicts damage on any Pokémon of the same type in the area around it.'),
('FR', 486, 'Boule Élek', 'Le lanceur envoie une boule d’électricité. Si sa Vitesse est plus grande que celle de l’ennemi, les dégâts augmentent d’autant.'),
('EN', 486, 'Electro Ball', 'The user hurls an electric orb at the target. The faster the user is than the target, the greater the move’s power.'),
('FR', 487, 'Détrempage', 'Le lanceur projette beaucoup d’eau sur sa cible, qui devient de type Eau.'),
('EN', 487, 'Soak', 'The user shoots a torrent of water at the target and changes the target’s type to Water.'),
('FR', 488, 'Nitrocharge', 'Le lanceur s’entoure de flammes pour attaquer l’ennemi. Il se concentre et sa Vitesse augmente.'),
('EN', 488, 'Flame Charge', 'Cloaking itself in flame, the user attacks. Then, building up more power, the user raises its Speed stat.'),
('FR', 489, 'Enroulement', 'Le lanceur s’enroule sur lui-même et se concentre. Son Attaque, sa Défense et sa Précision augmentent.'),
('EN', 489, 'Coil', 'The user coils up and concentrates. This raises its Attack and Defense stats as well as its accuracy.'),
('FR', 490, 'Balayette', 'Un coup rapide qui vise les pieds de l’ennemi et diminue sa Vitesse.'),
('EN', 490, 'Low Sweep', 'The user makes a swift attack on the target’s legs, which lowers the target’s Speed stat.'),
('FR', 491, 'Bombe Acide', 'Projette un liquide acide qui fait fondre l’ennemi. Sa Défense Spéciale diminue beaucoup.'),
('EN', 491, 'Acid Spray', 'The user spits fluid that works to melt the target. This harshly lowers the target’s Sp. Def stat.'),
('FR', 492, 'Tricherie', 'Le lanceur utilise la force de l’ennemi. Plus l’Attaque de l’ennemi est élevée, plus le lanceur inflige de dégâts.'),
('EN', 492, 'Foul Play', 'The user turns the target’s power against it. The higher the target’s Attack stat, the greater the move’s power.'),
('FR', 493, 'Rayon Simple', 'Le lanceur envoie des ondes mystérieuses à l’ennemi. Son talent devient Simple.'),
('EN', 493, 'Simple Beam', 'The user’s mysterious psychic wave changes the target’s Ability to Simple.'),
('FR', 494, 'Ten-danse', 'Le lanceur danse sur un rythme étrange. Il force sa cible à l’imiter, ce qui lui fait adopter son talent.'),
('EN', 494, 'Entrainment', 'The user dances with an odd rhythm that compels the target to mimic it, making the target’s Ability the same as the user’s.'),
('FR', 495, 'Après Vous', 'S’il est le premier à agir, le lanceur permet à sa cible d’utiliser une capacité juste après lui.'),
('EN', 495, 'After You', 'The user helps the target and makes it use its move right after the user.'),
('FR', 496, 'Chant Canon', 'Le lanceur attaque l’ennemi en chantant. Si plusieurs Pokémon déclenchent cette attaque à la suite, l’effet augmente.'),
('EN', 496, 'Round', 'The user attacks the target with a song. Others can join in the Round and make the attack do greater damage.'),
('FR', 497, 'Écho', 'Un cri retentissant blesse l’ennemi. Si le lanceur ou d’autres Pokémon l’utilisent à chaque tour, l’effet augmente.'),
('EN', 497, 'Echoed Voice', 'The user attacks the target with an echoing voice. If this move is used every turn, it does greater damage.'),
('FR', 498, 'Attrition', 'Une attaque puissante quand l’ennemi baisse sa garde. Inflige des dégâts sans tenir compte des changements de stats.'),
('EN', 498, 'Chip Away', 'Looking for an opening, the user strikes consistently. The target’s stat changes don’t affect this attack’s damage.'),
('FR', 499, 'Bain de Smog', 'Le lanceur jette un tas de détritus spéciaux sur la cible. Les changements de stats de la cible sont annulés.'),
('EN', 499, 'Clear Smog', 'The user attacks by throwing a clump of special mud. All stat changes are returned to normal.'),
('FR', 500, 'Force Ajoutée', 'Le lanceur attaque l’ennemi avec une force accumulée. Plus les stats du lanceur sont augmentées, plus le coup est efficace.'),
('EN', 500, 'Stored Power', 'The user attacks the target with stored power. The more the user’s stats are raised, the greater the move’s power.'),
('FR', 501, 'Prévention', 'Le lanceur et son équipe sont protégés contre les attaques prioritaires.'),
('EN', 501, 'Quick Guard', 'The user protects itself and its allies from priority moves.'),
('FR', 502, 'Interversion', 'Le lanceur se téléporte à l’aide d’un pouvoir mystérieux. Il échange sa place avec celle d’un allié sur le terrain.'),
('EN', 502, 'Ally Switch', 'The user teleports using a strange power and switches places with one of its allies.'),
('FR', 503, 'Ébullition', 'L’ennemi est attaqué par un jet d’eau bouillante. Peut aussi le brûler.'),
('EN', 503, 'Scald', 'The user shoots boiling hot water at its target. This may also leave the target with a burn.'),
('FR', 504, 'Exuviation', 'Le lanceur brise sa coquille. Il baisse sa Défense et Défense Spéciale, mais augmente fortement son Attaque, Attaque Spéciale et Vitesse.'),
('EN', 504, 'Shell Smash', 'The user breaks its shell, which lowers Defense and Sp. Def stats but sharply raises its Attack, Sp. Atk, and Speed stats.'),
('FR', 505, 'Vibra Soin', 'Une aura de bien-être fait récupérer la moitié de ses PV max à la cible.'),
('EN', 505, 'Heal Pulse', 'The user emits a healing pulse which restores the target’s HP by up to half of its max HP.'),
('FR', 506, 'Châtiment', 'Attaque acharnée qui cause davantage de dégâts à l’ennemi s’il a un problème de statut.'),
('EN', 506, 'Hex', 'This relentless attack does massive damage to a target affected by status conditions.'),
('FR', 507, 'Chute Libre', 'Le lanceur emmène l’ennemi dans les airs au premier tour et le lâche dans le vide au second. L’ennemi saisi ne peut pas attaquer.'),
('EN', 507, 'Sky Drop', 'The user takes the target into the sky, then drops it during the next turn. The target cannot attack while in the sky.'),
('FR', 508, 'Chgt Vitesse', 'Le lanceur fait tourner ses engrenages. Ceci augmente son Attaque et augmente fortement sa Vitesse.'),
('EN', 508, 'Shift Gear', 'The user rotates its gears, raising its Attack and sharply raising its Speed.'),
('FR', 509, 'Projection', 'Projette le Pokémon ennemi et le remplace par un autre. Dans la nature, met fin au combat.'),
('EN', 509, 'Circle Throw', 'The target is thrown, and a different Pokémon is dragged out. In the wild, this ends a battle against a single Pokémon.'),
('FR', 510, 'Calcination', 'Des flammes calcinent l’ennemi. S’il tient un objet, une Baie par exemple, celui-ci est brûlé et devient inutilisable.'),
('EN', 510, 'Incinerate', 'The user attacks opposing Pokémon with fire. If a Pokémon is holding a certain item, such as a Berry, the item becomes burned up and unusable.'),
('FR', 511, 'À la Queue', 'Retient la cible de force, l’obligeant à agir en dernier.'),
('EN', 511, 'Quash', 'The user suppresses the target and makes its move go last.'),
('FR', 512, 'Acrobatie', 'Attaque agile. Si le lanceur ne tient pas d’objet, l’attaque inflige davantage de dégâts.'),
('EN', 512, 'Acrobatics', 'The user nimbly strikes the target. If the user is not holding an item, this attack inflicts massive damage.'),
('FR', 513, 'Copie Type', 'Le lanceur copie le type de la cible et devient du même type.'),
('EN', 513, 'Reflect Type', 'The user reflects the target’s type, making it the same type as the target.'),
('FR', 514, 'Vengeance', 'Venge un Pokémon de l’équipe mis K.O. Si un Pokémon de l’équipe a été mis K.O. au tour d’avant, l’effet augmente.'),
('EN', 514, 'Retaliate', 'The user gets revenge for a fainted ally. If an ally fainted in the previous turn, this move becomes more powerful.'),
('FR', 515, 'Tout ou Rien', 'Une attaque très risquée. Le lanceur perd tous ses PV restants et inflige autant de dégâts à l’ennemi.'),
('EN', 515, 'Final Gambit', 'The user risks everything to attack its target. The user faints but does damage equal to its HP.'),
('FR', 516, 'Passe-Cadeau', 'Si la cible ne tient pas d’objet, le lanceur lui donne l’objet qu’il tient.'),
('EN', 516, 'Bestow', 'The user passes its held item to the target when the target isn’t holding an item.'),
('FR', 517, 'Feu d\'Enfer', 'L’ennemi est entouré d’un torrent de flammes ardentes qui le brûlent.'),
('EN', 517, 'Inferno', 'The user attacks by engulfing the target in an intense fire. This leaves the target with a burn.'),
('FR', 518, 'Aire d\'Eau', 'Une masse d’eau s’abat sur l’ennemi. En l’utilisant avec Aire de Feu, l’effet augmente et un arc-en-ciel apparaît.'),
('EN', 518, 'Water Pledge', 'A column of water strikes the target. When combined with its fire equivalent, the damage increases and a rainbow appears.'),
('FR', 519, 'Aire de Feu', 'Une masse de feu s’abat sur l’ennemi. En l’utilisant avec Aire d’Herbe, l’effet augmente et une mer de feu apparaît.'),
('EN', 519, 'Fire Pledge', 'A column of fire hits the target. When used with its grass equivalent, its damage increases and a vast sea of fire appears.'),
('FR', 520, 'Aire d\'Herbe', 'Une masse végétale s’abat sur l’ennemi. En l’utilisant avec Aire d’Eau, l’effet augmente et un marécage apparaît.'),
('EN', 520, 'Grass Pledge', 'A column of grass hits the target. When used with its water equivalent, its damage increases and a vast swamp appears.'),
('FR', 521, 'Change Éclair', 'Après son attaque, le lanceur revient à toute vitesse et change de place avec un Pokémon de l’équipe prêt au combat.'),
('EN', 521, 'Volt Switch', 'After making its attack, the user rushes back to switch places with a party Pokémon in waiting.'),
('FR', 522, 'Survinsecte', 'Le lanceur se débat de toutes ses forces, et baisse l’Attaque Spéciale de l’ennemi.'),
('EN', 522, 'Struggle Bug', 'While resisting, the user attacks the opposing Pokémon. This lowers the Sp. Atk stat of those hit.'),
('FR', 523, 'Piétisol', 'Le lanceur piétine le sol et inflige des dégâts à tous les Pokémon autour de lui. Baisse aussi leur Vitesse.'),
('EN', 523, 'Bulldoze', 'The user strikes everything around it by stomping down on the ground. This lowers the Speed stat of those hit.'),
('FR', 524, 'Souffle Glacé', 'Un souffle froid blesse l’ennemi. L’effet est toujours critique.'),
('EN', 524, 'Frost Breath', 'The user blows its cold breath on the target. This attack always results in a critical hit.'),
('FR', 525, 'Draco-Queue', 'Un coup puissant qui blesse la cible et l’envoie au loin. Dans la nature, met fin au combat.'),
('EN', 525, 'Dragon Tail', 'The target is knocked away, and a different Pokémon is dragged out. In the wild, this ends a battle against a single Pokémon.'),
('FR', 526, 'Rengorgement', 'Le lanceur se rengorge. Augmente l’Attaque et l’Attaque Spéciale.'),
('EN', 526, 'Work Up', 'The user is roused, and its Attack and Sp. Atk stats increase.'),
('FR', 527, 'Toile Élek', 'Attrape l’ennemi dans un filet électrique. Baisse aussi la Vitesse de l’ennemi.'),
('EN', 527, 'Electroweb', 'The user attacks and captures opposing Pokémon using an electric net. This lowers their Speed stat.'),
('FR', 528, 'Éclair Fou', 'Une charge électrique violente qui blesse aussi légèrement le lanceur.'),
('EN', 528, 'Wild Charge', 'The user shrouds itself in electricity and smashes into its target. This also damages the user a little.'),
('FR', 529, 'Tunnelier', 'Le lanceur tourne sur lui-même comme une perceuse et se jette sur l’ennemi. Taux de critiques élevé.'),
('EN', 529, 'Drill Run', 'The user crashes into its target while rotating its body like a drill. Critical hits land more easily.'),
('FR', 530, 'Double Baffe', 'Le lanceur frappe l’ennemi deux fois d’affilée avec les parties les plus robustes de son corps.'),
('EN', 530, 'Dual Chop', 'The user attacks its target by hitting it with brutal strikes. The target is hit twice in a row.'),
('FR', 531, 'Crève-Cœur', 'Déconcentre l’ennemi avec des mouvements mignons avant de le frapper violemment. Peut aussi l’apeurer.'),
('EN', 531, 'Heart Stamp', 'The user unleashes a vicious blow after its cute act makes the target less wary. This may also make the target flinch.'),
('FR', 532, 'Encornebois', 'Un coup de corne qui draine l’énergie de l’ennemi. Convertit la moitié des dégâts infligés en PV pour le lanceur.'),
('EN', 532, 'Horn Leech', 'The user drains the target’s energy with its horns. The user’s HP is restored by half the damage taken by the target.'),
('FR', 533, 'Lame Sainte', 'Un coup de corne violent qui lacère l’ennemi et lui inflige des dégâts quels que soient ses changements de stats.'),
('EN', 533, 'Sacred Sword', 'The user attacks by slicing with a long horn. The target’s stat changes don’t affect this attack’s damage.'),
('FR', 534, 'Coquilame', 'Un coquillage aiguisé lacère l’ennemi. Peut aussi baisser sa Défense.'),
('EN', 534, 'Razor Shell', 'The user cuts its target with sharp shells. This may also lower the target’s Defense stat.'),
('FR', 535, 'Tacle Feu', 'Le lanceur projette son corps enflammé contre l’ennemi. S’il est plus lourd que l’ennemi, l’effet augmente en conséquence.'),
('EN', 535, 'Heat Crash', 'The user slams its target with its flame- covered body. The more the user outweighs the target, the greater the move’s power.'),
('FR', 536, 'Phytomixeur', 'L’ennemi est pris dans un tourbillon de feuilles acérées. Peut aussi baisser sa Précision.'),
('EN', 536, 'Leaf Tornado', 'The user attacks its target by encircling it in sharp leaves. This attack may also lower the target’s accuracy.'),
('FR', 537, 'Bulldoboule', 'Le lanceur se roule en boule et écrase son ennemi. Peut aussi l’apeurer.'),
('EN', 537, 'Steamroller', 'The user crushes its targets by rolling over them with its rolled-up body. This may also make the target flinch.'),
('FR', 538, 'Cotogarde', 'Le lanceur se protège en s’emmitouflant dans du coton. Sa Défense augmente énormément.'),
('EN', 538, 'Cotton Guard', 'The user protects itself by wrapping its body in soft cotton, which drastically raises the user’s Defense stat.'),
('FR', 539, 'Explonuit', 'Le lanceur attaque l’ennemi avec une onde de choc ténébreuse. Peut aussi baisser sa Précision.'),
('EN', 539, 'Night Daze', 'The user lets loose a pitch-black shock wave at its target. This may also lower the target’s accuracy.'),
('FR', 540, 'Frappe Psy', 'Le lanceur matérialise des ondes mystérieuses qu’il projette sur l’ennemi. Inflige des dégâts physiques.'),
('EN', 540, 'Psystrike', 'The user materializes an odd psychic wave to attack the target. This attack does physical damage.'),
('FR', 541, 'Plumo-Queue', 'Le lanceur frappe l’ennemi de deux à cinq fois d’affilée avec sa queue robuste.'),
('EN', 541, 'Tail Slap', 'The user attacks by striking the target with its hard tail. It hits the target two to five times in a row.'),
('FR', 542, 'Vent Violent', 'Le lanceur déclenche une tempête de vents violents qui s’abat sur l’ennemi. Peut aussi le rendre confus.'),
('EN', 542, 'Hurricane', 'The user attacks by wrapping its opponent in a fierce wind that flies up into the sky. This may also confuse the target.'),
('FR', 543, 'Peignée', 'Le lanceur donne un coup avec sa tête couronnée d’une fière crinière. Blesse aussi légèrement le lanceur.'),
('EN', 543, 'Head Charge', 'The user charges its head into its target, using its powerful guard hair. This also damages the user a little.'),
('FR', 544, 'Lancécrou', 'Le lanceur jette deux écrous d’acier qui frappent l’ennemi deux fois d’affilée.'),
('EN', 544, 'Gear Grind', 'The user attacks by throwing steel gears at its target twice.'),
('FR', 545, 'Incendie', 'Des boules de feu s’abattent sur tous les Pokémon  autour du lanceur. Peut aussi les brûler.'),
('EN', 545, 'Searing Shot', 'The user torches everything around it with an inferno of scarlet flames. This may also leave those hit with a burn.'),
('FR', 546, 'TechnoBuster', 'Le lanceur projette un rayon lumineux sur l’ennemi.  Le type varie selon le Module que tient le lanceur.'),
('EN', 546, 'Techno Blast', 'The user fires a beam of light at its target. The move’s type changes depending on the Drive the user holds.'),
('FR', 547, 'Chant Antique', 'Le lanceur attaque l’ennemi en lui chantant une chanson d’un autre temps. Peut l’endormir.'),
('EN', 547, 'Relic Song', 'The user sings an ancient song and attacks by appealing to the hearts of the listening opposing Pokémon. This may also induce sleep.'),
('FR', 548, 'Lame Ointe', 'L’ennemi est lacéré par une longue corne. Son pouvoir mystérieux inflige des dégâts physiques.'),
('EN', 548, 'Secret Sword', 'The user cuts with its long horn. The odd power contained in the horn does physical damage to the target.'),
('FR', 549, 'Ère Glaciaire', 'Un souffle de vent qui congèle tout sur son passage s’abat sur l’ennemi. Réduit aussi sa Vitesse.'),
('EN', 549, 'Glaciate', 'The user attacks by blowing freezing cold air at opposing Pokémon. This lowers their Speed stat.'),
('FR', 550, 'Charge Foudre', 'Le lanceur s’enveloppe d’une charge électrique surpuissante et se jette sur l’ennemi. Peut aussi le paralyser.'),
('EN', 550, 'Bolt Strike', 'The user surrounds itself with a great amount of electricity and charges its target. This may also leave the target with paralysis.'),
('FR', 551, 'Flamme Bleue', 'De magnifiques et redoutables flammes bleues fondent sur l’ennemi. Peut aussi le brûler.'),
('EN', 551, 'Blue Flare', 'The user attacks by engulfing the target in an intense, yet beautiful, blue flame. This may also leave the target with a burn.'),
('FR', 552, 'Danse du Feu', 'Le lanceur enveloppe l’ennemi de flammes. Peut aussi augmenter l’Attaque Spéciale du lanceur.'),
('EN', 552, 'Fiery Dance', 'Cloaked in flames, the user dances and flaps its wings. This may also raise the user’s Sp. Atk stat.'),
('FR', 553, 'Éclair Gelé', 'Projette un bloc de glace électrifié sur l’ennemi au second tour. Peut aussi le paralyser.'),
('EN', 553, 'Freeze Shock', 'On the second turn, the user hits the target with electrically charged ice. This may also leave the target with paralysis.'),
('FR', 554, 'Feu Glacé', 'Au second tour, le lanceur projette un souffle de vent glacial dévastateur sur l’ennemi. Peut aussi le brûler.'),
('EN', 554, 'Ice Burn', 'On the second turn, an ultracold, freezing wind surrounds the target. This may leave the target with a burn.'),
('FR', 555, 'Aboiement', 'Le lanceur hurle sur l’ennemi. Baisse l’Attaque Spéciale de l’ennemi.'),
('EN', 555, 'Snarl', 'The user yells as if it’s ranting about something, which lowers the Sp. Atk stat of opposing Pokémon.'),
('FR', 556, 'Chute Glace', 'Envoie de gros blocs de glace sur l’ennemi pour lui infliger des dégâts. Peut aussi l’apeurer.'),
('EN', 556, 'Icicle Crash', 'The user attacks by harshly dropping large icicles onto the target. This may also make the target flinch.'),
('FR', 557, 'Coup Victoire', 'Le lanceur projette une flamme ardente de son front et se jette sur l’ennemi. Baisse la Défense, la Défense Spéciale et la Vitesse.'),
('EN', 557, 'V-create', 'With a hot flame on its forehead, the user hurls itself at its target. This lowers the user’s Defense, Sp. Def, and Speed stats.'),
('FR', 558, 'Flamme Croix', 'Projette une boule de feu gigantesque. L’effet augmente sous l’influence d’Éclair Croix.'),
('EN', 558, 'Fusion Flare', 'The user brings down a giant flame. This move is more powerful when influenced by an enormous thunderbolt.'),
('FR', 559, 'Éclair Croix', 'Projette un orbe électrique gigantesque. L’effet augmente sous l’influence de Flamme Croix.'),
('EN', 559, 'Fusion Bolt', 'The user throws down a giant thunderbolt. This move is more powerful when influenced by an enormous flame.'),
('FR', 560, 'Flying Press', 'Une attaque en piqué depuis le ciel, à la fois de type Combat et de type Vol.'),
('EN', 560, 'Flying Press', 'The user dives down onto the target from the sky. This move is Fighting and Flying type simultaneously.'),
('FR', 561, 'Tatamigaeshi', 'Retourne un tatami pour bloquer, comme avec un bouclier, les capacités visant le lanceur ou ses alliés. N’a pas d’effet sur les attaques de statut.'),
('EN', 561, 'Mat Block', 'Using a pulled-up mat as a shield, the user protects itself and its allies from damaging moves. This does not stop status moves.'),
('FR', 562, 'Éructation', 'Le lanceur se tourne vers l’ennemi et lui éructe dessus, infligeant des dégâts. Ne fonctionne que si le lanceur consomme la Baie qu’il tient.'),
('EN', 562, 'Belch', 'The user lets out a damaging belch at the target. The user must eat a held Berry to use this move.'),
('FR', 563, 'Fertilisation', 'Laboure le sol et le rend plus fertile. Augmente l’Attaque et l’Attaque Spéciale des Pokémon de type Plante.'),
('EN', 563, 'Rototiller', 'Tilling the soil, the user makes it easier for plants to grow. This raises the Attack and Sp. Atk stats of Grass-type Pokémon.'),
('FR', 564, 'Toile Gluante', 'Déploie une toile visqueuse autour de l’ennemi qui ralentit la Vitesse de tout adversaire entrant au combat.'),
('EN', 564, 'Sticky Web', 'The user weaves a sticky net around the opposing team, which lowers their Speed stat upon switching into battle.'),
('FR', 565, 'Dard Mortel', 'Augmente fortement l’Attaque du lanceur si un ennemi est mis K.O. avec cette capacité.'),
('EN', 565, 'Fell Stinger', 'When the user knocks out a target with this move, the user’s Attack stat rises sharply.'),
('FR', 566, 'Hantise', 'Le lanceur disparaît au premier tour et frappe au second. Cette attaque passe outre les protections.'),
('EN', 566, 'Phantom Force', 'The user vanishes somewhere, then strikes the target on the next turn. This move hits even if the target protects itself.'),
('FR', 567, 'Halloween', 'Insuffle à la cible l’esprit d’Halloween, et ajoute le type Spectre à ses types actuels.'),
('EN', 567, 'Trick-or-Treat', 'The user takes the target trick-or-treating. This adds Ghost type to the target’s type.'),
('FR', 568, 'Râle Mâle', 'Le lanceur pousse un rugissement qui intimide l’ennemi et diminue son Attaque et son Attaque Spéciale.'),
('EN', 568, 'Noble Roar', 'Letting out a noble roar, the user intimidates the target and lowers its Attack and Sp. Atk stats.'),
('FR', 569, 'DélugePlasmique', 'Diffuse des particules saturées d’électricité qui transforment les capacités de type Normal en capacités de type Électrik.'),
('EN', 569, 'Ion Deluge', 'The user disperses electrically charged particles, which changes Normal-type moves to Electric-type moves.'),
('FR', 570, 'Parabocharge', 'Inflige des dégâts à tous les Pokémon autour du lanceur. Il récupère en PV la moitié des dégâts infligés.'),
('EN', 570, 'Parabolic Charge', 'The user attacks everything around it. The user’s HP is restored by half the damage taken by those hit.'),
('FR', 571, 'Maléfice Sylvain', 'La cible est charmée par l’esprit de la forêt. Le type Plante est ajouté à ses types actuels.'),
('EN', 571, 'Forest\'s Curse', 'The user puts a forest curse on the target. Afflicted targets are now Grass type as well.'),
('FR', 572, 'Tempête Florale', 'Déclenche une violente tempête de fleurs qui inflige des dégâts à tous les Pokémon alentour.'),
('EN', 572, 'Petal Blizzard', 'The user stirs up a violent petal blizzard and attacks everything around it.'),
('FR', 573, 'Lyophilisation', 'Refroidit violemment l’ennemi et peut le geler. Super efficace sur les Pokémon de type Eau.'),
('EN', 573, 'Freeze-Dry', 'The user rapidly cools the target. This may also leave the target frozen. This move is super effective on Water types.'),
('FR', 574, 'Voix Enjôleuse', 'Laisse s’échapper une voix enchanteresse qui inflige des dégâts psychiques à l’ennemi. Touche à coup sûr.'),
('EN', 574, 'Disarming Voice', 'Letting out a charming cry, the user does emotional damage to opposing Pokémon. This attack never misses.'),
('FR', 575, 'Dernier Mot', 'Menace l’ennemi dans une ultime tirade avant de changer de place avec un autre Pokémon. Réduit l’Attaque et l’Attaque Spéciale de l’ennemi.'),
('EN', 575, 'Parting Shot', 'With a parting threat, the user lowers the target’s Attack and Sp. Atk stats. Then it switches with a party Pokémon.'),
('FR', 576, 'Renversement', 'Inverse tous les changements de stats de la cible.'),
('EN', 576, 'Topsy-Turvy', 'All stat changes affecting the target turn topsy-turvy and become the opposite of what they were.'),
('FR', 577, 'Vampibaiser', 'Aspire la force vitale de l’ennemi par un baiser. Rend au lanceur un nombre de PV supérieur ou égal à la moitié des dégâts infligés.'),
('EN', 577, 'Draining Kiss', 'The user steals the target’s energy with a kiss. The user’s HP is restored by over half of the damage taken by the target.'),
('FR', 578, 'Vigilance', 'Utilise une force mystérieuse pour protéger l’équipe des attaques de statut. Ne protège pas des autres capacités.'),
('EN', 578, 'Crafty Shield', 'The user protects itself and its allies from status moves with a mysterious power. This does not stop moves that do damage.'),
('FR', 579, 'Garde Florale', 'Grâce à une force mystérieuse, la Défense de tous les Pokémon Plante au combat augmente.'),
('EN', 579, 'Flower Shield', 'The user raises the Defense stat of all Grass-type Pokémon in battle with a mysterious power.'),
('FR', 580, 'Champ Herbu', 'Pendant cinq tours, le terrain aux pieds du lanceur se transforme en gazon. Les Pokémon au sol récupèrent des PV à chaque tour.'),
('EN', 580, 'Grassy Terrain', 'The user turns the ground under everyone’s feet to grass for five turns. This restores the HP of Pokémon on the ground a little every turn.'),
('FR', 581, 'Champ Brumeux', 'Pendant cinq tours, le terrain aux pieds du lanceur se couvre de brume. Les Pokémon au sol ne peuvent pas subir d’altération de statut.'),
('EN', 581, 'Misty Terrain', 'The user covers the ground under everyone’s feet with mist for five turns. This protects Pokémon on the ground from status conditions.'),
('FR', 582, 'Électrisation', 'Si le lanceur attaque avant la cible, les capacités de celle-ci seront de type Électrik jusqu’à la fin du tour.'),
('EN', 582, 'Electrify', 'If the target is electrified before it uses a move during that turn, the target’s move becomes Electric type.'),
('FR', 583, 'Câlinerie', 'Attaque l’ennemi avec un câlin. Peut diminuer son Attaque.'),
('EN', 583, 'Play Rough', 'The user plays rough with the target and attacks it. This may also lower the target’s Attack stat.'),
('FR', 584, 'Vent Féérique', 'Déchaîne un vent magique qui cingle l’ennemi.'),
('EN', 584, 'Fairy Wind', 'The user stirs up a fairy wind and strikes the target with it.'),
('FR', 585, 'Pouvoir Lunaire', 'Attaque l’ennemi grâce au pouvoir de la lune. Peut diminuer son Attaque Spéciale.'),
('EN', 585, 'Moonblast', 'Borrowing the power of the moon, the user attacks the target. This may also lower the target’s Sp. Atk stat.'),
('FR', 586, 'Bang Sonique', 'Attaque les Pokémon alentour grâce à une onde sonore assourdissante qui détruit tout sur son passage.'),
('EN', 586, 'Boomburst', 'The user attacks everything around it with the destructive power of a terrible, explosive sound.'),
('FR', 587, 'Verrou Enchanté', 'Des chaînes entourent la zone de combat, empêchant tous les Pokémon de fuir au prochain tour.'),
('EN', 587, 'Fairy Lock', 'By locking down the battlefield, the user keeps all Pokémon from fleeing during the next turn.'),
('FR', 588, 'Bouclier Royal', 'Prend une posture défensive pour bloquer les dégâts. Diminue fortement l’Attaque de tout Pokémon qui entre en contact avec le lanceur.'),
('EN', 588, 'King\'s Shield', 'The user takes a defensive stance while it protects itself from damage. It also harshly lowers the Attack stat of any attacker who makes direct contact.'),
('FR', 589, 'Camaraderie', 'L’ennemi se lie d’amitié avec le lanceur et perd sa combativité, diminuant son Attaque.'),
('EN', 589, 'Play Nice', 'The user and the target become friends, and the target loses its will to fight. This lowers the target’s Attack stat.'),
('FR', 590, 'Confidence', 'Dévoile des secrets à l’ennemi qui perd sa concentration et voit son Attaque Spéciale diminuer.'),
('EN', 590, 'Confide', 'The user tells the target a secret, and the target loses its ability to concentrate. This lowers the target’s Sp. Atk stat.'),
('FR', 591, 'Orage Adamantin', 'Provoque une tempête de diamants qui inflige des dégâts. Peut augmenter la Défense du lanceur.'),
('EN', 591, 'Diamond Storm', 'The user whips up a storm of diamonds to damage opposing Pokémon. This may also raise the user’s Defense stat.'),
('FR', 592, 'Jet de Vapeur', 'Plonge l’ennemi dans une chaleur étouffante. Peut le brûler.'),
('EN', 592, 'Steam Eruption', 'The user immerses the target in superheated steam. This may also leave the target with a burn.'),
('FR', 594, 'Sheauriken', 'Attaque l’ennemi avec des shuriken de mucus. Frappe deux à cinq fois d’affilée en un tour, et toujours en premier.'),
('EN', 594, 'Water Shuriken', 'The user hits the target with throwing stars two to five times in a row. This move always goes first.'),
('FR', 595, 'Feu Ensorcelé', 'Attaque avec des flammes brûlantes soufflées de la bouche du lanceur. Diminue l’Attaque Spéciale de l’ennemi.'),
('EN', 595, 'Mystical Fire', 'The user attacks by breathing a special, hot fire. This also lowers the target’s Sp. Atk stat.'),
('FR', 596, 'Pico-Défense', 'Protège des attaques, et diminue les PV de tout attaquant qui entre en contact avec le lanceur.'),
('EN', 596, 'Spiky Shield', 'In addition to protecting the user from attacks, this move also damages any attacker who makes direct contact.'),
('FR', 597, 'Brume Capiteuse', 'Grâce à un parfum mystérieux, augmente la Défense Spéciale d’un allié.'),
('EN', 597, 'Aromatic Mist', 'The user raises the Sp. Def stat of an ally Pokémon by using a mysterious aroma.'),
('FR', 598, 'Ondes Étranges', 'Le corps du lanceur produit des ondes anormales qui enveloppent l’ennemi et diminuent fortement son Attaque Spéciale.'),
('EN', 598, 'Eerie Impulse', 'The user’s body generates an eerie impulse. Exposing the target to it harshly lowers the target’s Sp. Atk stat.'),
('FR', 599, 'Piège de Venin', 'Sécrète un liquide empoisonné. Diminue l’Attaque, l’Attaque Spéciale et la Vitesse de l’ennemi empoisonné.'),
('EN', 599, 'Venom Drench', 'Opposing Pokémon are drenched in an odd poisonous liquid. This lowers the Attack, Sp. Atk, and Speed stats of a poisoned target.'),
('FR', 600, 'Nuée de Poudre', 'L’ennemi est pris dans un nuage de poudre. S’il utilise une capacité de type Feu lors du même tour, le nuage explose et lui inflige des dégâts.'),
('EN', 600, 'Powder', 'The user covers the target in a powder that explodes and damages the target if it uses a Fire-type move.'),
('FR', 601, 'Géo-Contrôle', 'Le lanceur absorbe de l’énergie au premier tour et augmente fortement son Attaque Spéciale, sa Défense Spéciale et sa Vitesse au second.'),
('EN', 601, 'Geomancy', 'The user absorbs energy and sharply raises its Sp. Atk, Sp. Def, and Speed stats on the next turn.'),
('FR', 602, 'Magné-Contrôle', 'Manipule les champs magnétiques pour augmenter la Défense et la Défense Spéciale des Pokémon alliés dotés du talent Plus ou du talent Minus.'),
('EN', 602, 'Magnetic Flux', 'The user manipulates magnetic fields, which raises the Defense and Sp. Def stats of ally Pokémon with the Plus or Minus Ability.'),
('FR', 603, 'Étrennes', 'Utilisé pendant un combat, multiplie par deux l’argent gagné à la fin.'),
('EN', 603, 'Happy Hour', 'Using Happy Hour doubles the amount of prize money received after battle.'),
('FR', 604, 'Champ Électrifié', 'Pendant cinq tours, le terrain aux pieds du lanceur se charge d’électricité. Les Pokémon au sol ne peuvent pas s’endormir.'),
('EN', 604, 'Electric Terrain', 'The user electrifies the ground under everyone’s feet for five turns. Pokémon on the ground no longer fall asleep.'),
('FR', 605, 'Éclat Magique', 'Libère une puissante décharge lumineuse qui inflige des dégâts à l’ennemi.'),
('EN', 605, 'Dazzling Gleam', 'The user damages opposing Pokémon by emitting a powerful flash.'),
('FR', 608, 'Regard Touchant', 'Fixe l’ennemi d’un air très attendrissant qui le touche et diminue son Attaque. Agit toujours en premier.'),
('EN', 608, 'Baby-Doll Eyes', 'The user stares at the target with its baby-doll eyes, which lowers its Attack stat. This move always goes first.'),
('FR', 609, 'Frotte-Frimousse', 'Le lanceur attaque en frottant ses bajoues chargées d’électricité. Paralyse l’ennemi.'),
('EN', 609, 'Nuzzle', 'The user attacks by nuzzling its electrified cheeks against the target. This also leaves the target with paralysis.'),
('FR', 610, 'Retenue', 'Le lanceur attaque avec retenue, et laisse l’ennemi à 1 PV.'),
('EN', 610, 'Hold Back', 'The user holds back when it attacks and the target is left with at least 1 HP.'),
('FR', 611, 'Harcèlement', 'Cette attaque perdure pendant quatre à cinq tours. L’ennemi ne peut pas fuir au cours de cette période.'),
('EN', 611, 'Infestation', 'The target is infested and attacked for four to five turns. The target can’t flee during this time.'),
('FR', 612, 'Poing Boost', 'À force de frapper, les poings deviennent plus durs. Augmente l’Attaque du lanceur si l’ennemi est touché.'),
('EN', 612, 'Power-Up Punch', 'Striking opponents over and over makes the user’s fists harder. Hitting a target raises the Attack stat.'),
('FR', 613, 'Mort-Ailes', 'Vole l’énergie de la cible. Rend au lanceur un nombre de PV supérieur ou égal à la moitié des dégâts infligés.'),
('EN', 613, 'Oblivion Wing', 'The user absorbs its target’s HP. The user’s HP is restored by over half of the damage taken by the target.'),
('FR', 616, 'Force Chtonienne', 'Utilise la puissance du sol et la concentre sur l’ennemi pour infliger des dégâts.'),
('EN', 616, 'Land\'s Wrath', 'The user gathers the energy of the land and focuses that power on opposing Pokémon to damage them.'),
('FR', 617, 'Lumière du Néant', 'Utilise la puissance de la Fleur Éternelle pour lancer un formidable rayon d’énergie. Blesse aussi gravement le lanceur.'),
('EN', 617, 'Light of Ruin', 'Drawing power from the Eternal Flower, the user fires a powerful beam of light. This also damages the user quite a lot.'),
('FR', 618, 'Onde Originelle', 'D’innombrables rayons lumineux d’un bleu étincelant  s’abattent sur la cible.'),
('EN', 618, 'Origin Pulse', 'The user attacks opposing Pokémon with countless beams of light that glow a deep and brilliant blue.'),
('FR', 619, 'Lame Pangéenne', 'Le Pokémon transforme la puissance de la terre et attaque la cible avec une lame acérée.'),
('EN', 619, 'Precipice Blades', 'The user attacks opposing Pokémon by manifesting the power of the land in fearsome blades of stone.'),
('FR', 620, 'Draco Ascension', 'Le Pokémon s’abat à toute vitesse sur la cible depuis les hautes couches de l’atmosphère. Baisse la Défense et la Défense Spéciale du lanceur. '),
('EN', 620, 'Dragon Ascent', 'After soaring upward, the user attacks its target by dropping out of the sky at high speeds, although it lowers its own Defense and Sp. Def in the process.'),
('FR', 621, 'Furie Dimension', 'Le Pokémon utilise sa multitude de bras pour infliger  une nuée de coups qui ignorent les capacités telles  qu’Abri ou Détection. Baisse la Défense du lanceur. '),
('EN', 621, 'Hyperspace Fury', 'Using its many arms, the user unleashes a barrage of attacks that ignore the effects of moves like Protect and Detect. This attack lowers the user’s Defense.');

COMMIT;
