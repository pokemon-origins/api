START TRANSACTION;

-- Language table used as base for translation
CREATE TABLE IF NOT EXISTS `languages` (
  `id` char(2) NOT NULL PRIMARY KEY COMMENT 'Id of the language (Primary key)',
  `name` varchar(20) NOT NULL COMMENT 'Name of the language (in the specified language)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `languages` (`id`, `name`) VALUES ('EN', 'English'), ('FR', 'Français');

COMMIT;
