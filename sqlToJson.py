import mysql.connector
from mysql.connector import Error
import re
import json
from pathlib import Path

def getContentFromDatabase(request, enableDictionary):
  """Do a SQL request and get the result

  Keyword arguments:
    request -- (string) SQL request
    enableDictionary -- (bool) Use dictionary mode (column name will be used to identify values)

    return: (array) All fetch results of the SQL request
    """
  try:
    connection = mysql.connector.connect(host='mariadb',
                                         database='db',
                                         user='root',
                                         password='root')

    cursor = connection.cursor(dictionary=enableDictionary)
    cursor.execute(request)
    records = cursor.fetchall()
  except Error as e:
    print("Error reading data from MySQL table using request {0}".format(request), e)
    raise
  finally:
    if (connection.is_connected()):
      connection.close()
      cursor.close()

  return records


def main():
  all_paths = []
  all_tables = [''.join(elems) for elems in getContentFromDatabase("SHOW tables;", False)]
  all_language_ids = [item for sublist in getContentFromDatabase("SELECT id FROM languages", False) for item in sublist]

  for table_name in all_tables:
    if ((not '_translation' in table_name) and (not 'Tables_in_db' in table_name)):
      # Create the base of the table
      #table_content = getContentFromDatabase("SELECT id FROM {0}".format(table_name), True)
      table_content = getContentFromDatabase("SELECT * FROM {0}".format(table_name), True)
      Path("{0}".format(table_name)).mkdir(parents=True, exist_ok=True)
      with open("{0}/index.html".format(table_name), "w") as file_write:
        json.dump(table_content, file_write)
        all_paths.append("{0}/".format(table_name))
        #list_ids = [item for sublist in table_content for item in sublist]
        #json.dump(list_ids, file_write)
      if (not ('{0}_translation'.format(table_name) in all_tables)):
        # Dump all tables which have no translation
        all_content = getContentFromDatabase("SELECT * FROM {0}".format(table_name), True)
        has_id = False
        for content in all_content:
          if "id" in content:
            has_id = True
            Path("{0}/{1}".format(table_name, content['id'])).mkdir(parents=True, exist_ok=True)
            with open("{0}/{1}/index.html".format(table_name, content['id']), "w") as file_write:
              json.dump(content, file_write)
          else:
            print("No id in table {0}".format(table_name))
            break
        if has_id:
          all_paths.append("{0}/{{{0} id}}/".format(table_name))

      else:
        # Dump all tables which have translation
        has_id = False
        for language in all_language_ids:
          all_content = getContentFromDatabase("SELECT * FROM {0} C INNER JOIN {0}_translation T ON C.id = T.{0}_id WHERE T.languages_id = '{1}'".format(table_name, language), True)
          Path("{0}/{1}".format(table_name, language)).mkdir(parents=True, exist_ok=True)
          with open("{0}/{1}/index.html".format(table_name, language), "w") as file_write:
            json.dump(all_content, file_write)
          for content in all_content:
            if "id" in content:
              has_id = True
              content.pop("{0}_id".format(table_name), None)
              Path("{0}/{1}/{2}".format(table_name, language, content['id'])).mkdir(parents=True, exist_ok=True)
              with open("{0}/{1}/{2}/index.html".format(table_name, language, content['id']), "w") as file_write:
                json.dump(content, file_write)
            else:
              break
        all_paths.append("{0}/{{language id}}/".format(table_name))
        if has_id:
          all_paths.append("{0}/{{language id}}/{{{0} id}}/".format(table_name))

  with open("index.html", "a") as file_write:
    # Display the top of the index file
    with open("top.html", "r") as top_file:
      file_write.write(top_file.read())

    # Display available languages
    file_write.write("<h3>List of available language ID: {0}</h3>".format("{0}".format(", ".join(all_language_ids))))

    # Display a list of all available API request
    new_path = False
    for path in all_paths:
      example = path.replace("{language id}", "EN")
      example = re.sub("{(.*)}", "1", example)
      if path.count('/') == 1:
        content_name = path[:-1].replace('_', ' ')

        if new_path:
          file_write.write("</tbody></table>")

        file_write.write("<br/><br/><h3>{0}</h3>".format(content_name.capitalize()))
        file_write.write("<table class=\"table table-striped table-bordered\">")
        file_write.write("<thead><tr><th scope=\"col\">Description</th><th scope=\"col\">API call</th><th scope=\"col\">Example</th></tr></thead>")
        file_write.write("<tbody>")

        file_write.write("<tr><th scope=\"row\">List of all {0}</th><td><i>{1}</i></td><td><a target=_blank href='{2}'>{2}</a></td></tr>".format(content_name, path, example))

        new_path = True
      elif ("{language id}" in path) and (path.count('/') == 2):
        file_write.write("<tr><th scope=\"row\">List of all {0} with readable information</th><td><i>{1}</i></td><td><a target=_blank href='{2}'>{2}</a></td></tr>".format(content_name, path, example))
      elif (not "{language id}" in path) and (path.count('/') == 2):
        file_write.write("<tr><th scope=\"row\">Get specific {0}</th><td><i>{1}</i></td><td><a target=_blank href='{2}'>{2}</a> (warning: Example ID may not exist)</td></tr>".format(content_name, path, example))
      elif (path.count('/') == 3):
        file_write.write("<tr><th scope=\"row\">Get specific {0} with readable information</th><td><i>{1}</i></td><td><a target=_blank href='{2}'>{2}</a> (warning: Example ID may not exist)</td></tr>".format(content_name, path, example))
      else:
        raise NameError("Failed to understand API call '{0}' means".format(path))

    file_write.write("</tbody></table>")

    # Display the bottom of the index file
    with open("bottom.html", "r") as bottom_file:
      file_write.write(bottom_file.read())

if __name__ == '__main__':
  main()
